//
//  DebateNotification.swift
//  Tirade
//
//  Created by admin on 20/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class DebateNotification: Decodable {
    
    
    var sender_id : String?
    var sender_username : String?
    var created_date : String?
    var debate_reason : String?
    var sender_user_profile : String?

}
