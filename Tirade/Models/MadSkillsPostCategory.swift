//
//  MadSkillsPostCategory.swift
//  Tirade
//
//  Created by admin on 01/06/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class MadSkillsPostCategory: NSObject {
    var category : String!
    var selectedCategory : String!
    var post : String!
    var business_category : String!
    var business_name : String!
    var business_image : String!
    var experience : String!
    var youRepresent : String!
    var height : String!
    var weight : String!
    var otherSkills : String? = ""

    var fileInfoType = [FileTypeInfo]()
}
