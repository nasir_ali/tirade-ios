//
//  Likes.swift
//  Tirade
//
//  Created by admin on 20/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class Likes: Decodable {
      var user_id : String?
      var forum_id : String?
      var comment : String?
      var created_at : String?
      var user_name : String?
      var user_img : String?
      var agree : String?
    
}
