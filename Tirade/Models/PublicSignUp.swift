//
//  PublicSignUp.swift
//  Tirade
//
//  Created by admin on 18/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class PublicSignUp: NSObject {
    
    var firstName : String? = ""
    var lastName : String? = ""
    var phone : String? = ""
    var email : String? = ""
    var password : String? = ""
    var birthday : String? = ""
    var zipCode : String? = ""
    var gender = String()
    
}
