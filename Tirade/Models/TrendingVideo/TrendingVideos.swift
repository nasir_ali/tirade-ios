//
//  TrendingVideos.swift
//  Tirade
//
//  Created by admin on 02/06/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TrendingVideos: Decodable {
    var trending_videos : [Videos]?
    
}

class Videos: Decodable {
    var videos : [VideosData]?
}

class VideosData: Decodable {
    var likes : String? = ""
    var post_video : String? = ""
}
