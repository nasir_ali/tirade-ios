//
//  IncognitoForum.swift
//  Tirade
//
//  Created by admin on 27/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class IncognitoForum: Decodable {
    
    var category : String? = ""
    var fourmAudience : String? = ""
    var fourmGoal : String? = ""
    var followLike : String? = ""
    var fourmName : String? = ""
    var fourmsDescription : String? = ""
    var linkSite : String? = ""
    
    var customLink : String? = ""
    var titleLink : String? = ""
    var myWebsite : String? = ""
    var addUrl : String? = ""
    var socialLink : String? = ""
    var profileName : String? = ""
    var fourmImage : String? = ""
    var forumVideo : String! = ""
    
    var forumNotes : String? = ""
    var id : String? = ""
    var agree : [AgreeDisagree]?
    var comments : [Message]?
    var user_id : String? = ""
    
    
    
}

class MadSkillsData: Decodable {
    
    var business_image : String? = ""
    var business_name : String? = ""
    var created_date : String? = ""
    var experience : String? = ""
    var file_path : String? = ""
    var height : String? = ""
    var image_path : String? = ""
    
    var message : String? = ""
    var music_path : String? = ""
    var otherSkills : String? = ""
    var post_id : String? = ""
    var user_id : String? = ""
    var user_name : String? = ""
    var user_profile : String? = ""
    var video_path : String! = ""
    
    var weight : String? = ""
    var youRepresent : String? = ""
    
}
class MadTopSkill : Decodable{
    var id : String? = ""
    var name : String? = ""
    var image : String? = ""
}

class MadRateTopSkill : Decodable{
    var rate_top_skills : [MadRateSkills]?
}

class MadRateSkills: Decodable {
    var artists : [MadRateSkillsCat]?
    var sports : [MadRateSkillsCat]?
    var media : [MadRateSkillsCat]?
    var actor : [MadRateSkillsCat]?
    var dancer : [MadRateSkillsCat]?
    var singer : [MadRateSkillsCat]?
    var doctor : [MadRateSkillsCat]?
}

class MadRateSkillsCat: Decodable {
    var likes : String? = ""
    var dislikes : String? = ""
    var skill_id : String? = ""
    var category : String? = ""
    var user_id : String? = ""
    var business_image : String? = ""
}
