//
//  ForumDetailByUser.swift
//  Tirade
//
//  Created by admin on 19/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class ForumDetailByUser: Decodable {

    var name : String?
    var user_image : String?
    var images : [ForumDetailImages]?
    var videos : [ForumDetailVideos]?
    var notes  : [ForumDetailNotes]?
    
}

class ForumDetailImages: Decodable {
    var fourmImage : String?
    var forum_id : String?
    var total_agree : String?
    var total_comment : String?

}

class ForumDetailVideos: Decodable {
    var forumVideo : String?
    var forum_id : String?
    var total_agree : String?
    var total_comment : String?
    
}

class ForumDetailNotes: Decodable {
    var forumNotes : String?
    var forum_id : String?
    var total_agree : String?
    var total_comment : String?

}
