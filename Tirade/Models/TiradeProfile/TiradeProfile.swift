//
//  TiradeProfile.swift
//  Tirade
//
//  Created by admin on 28/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeProfileDict: Decodable {
    var getProfileData : [TiradeProfile]?
    
}
class TiradeProfile: Decodable {
    var profile_id : String?
    var status : String?
    var message : String?
    var editProfile : [EditProfile]?
    var addPeople : [AddPeople]?
    var Biography : [Biography]?
    var favouriteImg : [FavouriteImg]?
    var Hobbies :  [Hobbies]?
}

class EditProfile: Decodable {
    var s1_userImage : String?
    var s1_userName : String?
    var s1_userGender : String?
    var s1_userBirthdate : String?
}
class AddPeople: Decodable {
    var s2_userImage : String?
    var s2_userName : String?
}
class Biography: Decodable {
    var s3_education : String?
    var s3_born : String?
    var s3_live : String?
    var s3_work : String?
    var s3_liveIn : String?
    var s3_status : String?
}
class FavouriteImg: Decodable {
    var s4_imgUrl : String?
}
class Hobbies: Decodable {
    var s4_imgUrl : String?
    var s5_team : String?
    var s5_music : String?
    var s5_band : String?
    var s5_hobbies : String?
    var s5_heros : String?
    var s5_dreamjob : String?
    var s5_interested : String?
    var s5_animalLover : String?
    var s5_beliveInlife : String?
    var s5_favouriteColor : String?
    var s5_movies : String?
    var s5_dreamCar : String?
    var s5_dreamVacation : String?
    var s5_earning : String?
    var s5_food : String?
    var s5_sadOrHappy : String?
    var s5_makesYouHappy : String?
    var s5_makesYouAngry : String?
}

