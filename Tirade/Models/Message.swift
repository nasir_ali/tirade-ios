//
//  Message.swift
//  Tirade
//
//  Created by admin on 03/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class Message: Decodable {
    var name: String! = nil
    var content: String! = nil
    var isIncoming: Bool? = true
    var user_img: String! = nil
    var user_name: String! = nil

    
    var post_id_fk: String! = nil
    var author_id_fk: String! = nil
    var author_id: String! = nil
    var created_at: String! = nil
    var message: String! = nil
    var comment: String! = nil
    var forum_id: String! = nil

    
}
