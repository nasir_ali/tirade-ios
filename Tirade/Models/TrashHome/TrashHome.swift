//
//  TrashHome.swift
//  Tirade
//
//  Created by admin on 03/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TrashHome: Decodable {
    
    var name : String?
    var username : String?
    var category : String?
    var user_id : String?
    var post_title : String?
    var post_content : String?
    var post_id : String?
    var post_type : String?

    
//    var post_video : String?
//    var post_file : String?
    var agree : [AgreeDisagree]?
    var disagree: [AgreeDisagree]?
    var s_agree : [AgreeDisagree]?
    var s_disagree : [AgreeDisagree]?
    var boo : [AgreeDisagree]?
    var cheers : [AgreeDisagree]?
    var created_date : String?
    var user_profile : String?
   
    var image_path : String?
    var video_path : String?
    var audio_path : String?
    
    var pdf_path : String?
    var txtfile_path : String?
    
   // var post_image : String?
    var message : String?
    var comments : [Message]?
    var isInvite : Bool?
    
    
    var who : String?
    var what : String?
    var how : String?
    
    var selectedCatType : String?
    
      
}
