//
//  DataUtils.swift
//  Tirade
//
//  Created by admin on 09/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import AVKit

var globalPlayer = AVPlayerLayer()

class DataUtils: NSObject {
   private static var vSpinner : UIView?
    
    public static func showAlert(title:String,msg:String,selfObj:UIViewController,completionBlock : @escaping()-> Void)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            completionBlock()
        }))

        // alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        selfObj.present(alert, animated: true)
    }
    
    public static func player(view : UIView,url : String,playContinue:Bool){
        if globalPlayer.player != nil {
            globalPlayer.player?.pause()
        }
        let player = AVPlayer(url: URL(fileURLWithPath:url))
        print(player)
        globalPlayer.player = player
        globalPlayer.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width, height: view.frame.size.height)
        globalPlayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(globalPlayer)
        globalPlayer.player?.play()
        if playContinue{
            NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
                globalPlayer.player!.seek(to: CMTime.zero)
                globalPlayer.player!.play()
            }
        }
        else{
            NotificationCenter.default.removeObserver(self)
        }
        
    }
    
    public static func addPlayer(view : UIView,url : String)  {
        let bundle = Bundle.main
        let moviePath = bundle.path(forResource: url, ofType: "mp4")
        let width = view.frame.size.width
        let height: CGFloat = view.frame.size.height
        let player = AVPlayer(url: URL(fileURLWithPath: moviePath ?? ""))
        let playerLayer = AVPlayerLayer()
        playerLayer.player = player
        playerLayer.frame = CGRect(x: 0.0, y: 0, width: UIScreen.main.bounds.size.width, height: height + 30)
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.videoGravity = .resizeAspectFill
        
        view.layer.addSublayer(playerLayer)
        
        player.play()
    }
   public static func randomStringGenrator(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    public static func showLoader(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = .clear
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
        vSpinner?.tag = 112233
    }
    
    public static func showProgressView(onView : UIView,progress : Float) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = .clear
        let ai = UIProgressView.init(progressViewStyle: .bar)
        
        ai.setProgress(progress, animated: true)
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
    }
    
    public static func removeLoader() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
    public static func addBorder(view : UIView,cornerRadius : CGFloat,borderWidth : CGFloat,borderColor : UIColor)   {
          view.layer.cornerRadius = cornerRadius
          view.layer.borderWidth = borderWidth
        view.layer.borderColor = borderColor.cgColor
      }
    
   public static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
 
    public static func roundCorners(cornerRadius: Double,selfObj : UIView,topLeft : UIRectCorner,topRight : UIRectCorner,bottomLeft : UIRectCorner,bottomRight : UIRectCorner){
        let path = UIBezierPath(roundedRect: selfObj.bounds, byRoundingCorners: [topLeft,topRight,bottomLeft,bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        selfObj.layer.borderColor = UIColor.red.cgColor
        selfObj.layer.borderWidth = 5
        selfObj.layer.masksToBounds = true
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = selfObj.bounds
        maskLayer.path = path.cgPath
        maskLayer.fillColor = UIColor.red.cgColor
       // self.layer.borderWidth = 5
        selfObj.layer.mask = maskLayer
    }
    
   public static func makeLine(items : [UIView],onView : UIView)  {
        let path = UIBezierPath()
        path.move(to: items[0].center)
               //Appends a straight line to the receiver’s path
        for points in items {
            path.addLine(to: points.center)
        }
             

               //below code for draw line with use of path which is above code
               let progressLine = CAShapeLayer()
               progressLine.path = path.cgPath
               progressLine.strokeColor = UIColor.blue.cgColor
               progressLine.fillColor = UIColor.clear.cgColor
               progressLine.lineWidth = 30
             //  progressLine.lineCap = .round

               // add the curve to the screen
               onView.layer.addSublayer(progressLine)
    }
    
  
        
    public static func applyRadiusMaskFor(view :  UIView,topLeftRadius : CGFloat,topRightRadius : CGFloat,bottomLeftRadius : CGFloat,bottomRightRadius : CGFloat) {
        var topLeftRadius: CGFloat = topLeftRadius {
            didSet { view.setNeedsLayout() }
                }
        var topRightRadius: CGFloat = topRightRadius {
           didSet { view.setNeedsLayout() }
                  
           }
        var bottomLeftRadius: CGFloat = bottomLeftRadius {
            didSet { view.setNeedsLayout() }
                   }
        var bottomRightRadius: CGFloat = bottomRightRadius {
            didSet { view.setNeedsLayout() }
        }
        
        let path = UIBezierPath(shouldRoundRect: view.bounds, topLeftRadius: topLeftRadius, topRightRadius: topRightRadius, bottomLeftRadius: bottomLeftRadius, bottomRightRadius: bottomRightRadius)
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        view.layer.mask = shape
    }
    
  
}

class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
