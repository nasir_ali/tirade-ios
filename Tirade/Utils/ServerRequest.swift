//
//  ServerRequest.swift
//  Tirade
//
//  Created by admin on 18/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import Alamofire

class ServerRequest: NSObject {
    public static func doPostRequestToServer(_ url:String,data:Dictionary<String, AnyObject>,onController : UIViewController, completionBlock : @escaping()-> Void){
        //print(videoID)
        //print(userToken)
        //print("dopostrequl")
        
        let theJSONData = try? JSONSerialization.data(withJSONObject: data , options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!, encoding: String.Encoding.ascii.rawValue)
        let session = URLSession.shared
        let urlPath = URL(string: url)
        let request = NSMutableURLRequest(url: urlPath!)
        request.timeoutInterval = 20
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        //Bearer
        request.httpMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        DataUtils.showLoader(onView: onController.view)
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            guard error == nil else {
                
                return
            }
            // make sure we got data
            guard let responseData = data else {
                
                return
            }
            do {
                guard let jsonResult = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
                    
                    return
                }
                DispatchQueue.main.async {
                    DataUtils.removeLoader()
                }
                if let dictFromJSON = jsonResult as? [String:AnyObject] {
                    let locData = dictFromJSON["message"] as? String
                    let status = dictFromJSON["status"] as? Int
                    let userID = dictFromJSON["user"] as? [String:AnyObject]

                    if locData != nil{
                        DispatchQueue.main.async {
                            DataUtils.showAlert(title: "", msg: locData!, selfObj: onController) {
                                if status == 0 {
                                    
                                }else {
                                    if TiradeSingleTon.shared.isIncognitoUserSelection {
                                        if TiradeUserDefaults.getUser_ID(key: "user_id")  == "noid" {
                                            print("useriduseriduserid\(userID?["user_id"] as! String)")
                                            TiradeUserDefaults.setUser_ID(value: userID?["user_id"] as! String)
                                        }
                                    }
                                    completionBlock()
                                }
                            }
                        }
                    }
                }
            }catch  {
                
                return
            }
        }
        dataTask.resume()
    }
    
    public static func doGetRequestToServer(_ url:String,onController : UIViewController?, completionBlock : @escaping((Data)) -> Void){
        
        let url = URL(string: url)!
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if onController != nil {
                DispatchQueue.main.async {
                    DataUtils.showLoader(onView: onController!.view)
                }
            }
            guard let data = data, error == nil else {
                print("error=\(error)")
                return
            }
            
            completionBlock(data)
            // print("response = \(response)")
            
            let responseString = String(data: data, encoding: .utf8)
            // print("responseString = \(responseString)")
        }
        task.resume()

    }
    
    public static func doPostDataRequestToServer(_ url:String,data:Dictionary<String, AnyObject>,onController : UIViewController?, completionBlock : @escaping((Data))-> Void){
      
        let theJSONData = try? JSONSerialization.data(withJSONObject: data , options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString = NSString(data: theJSONData!, encoding: String.Encoding.ascii.rawValue)
        let session = URLSession.shared
        let urlPath = URL(string: url)
        let request = NSMutableURLRequest(url: urlPath!)
        request.timeoutInterval = 10
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        //Bearer
        request.httpMethod = "POST"
        let postLength = NSString(format:"%lu", jsonString!.length) as String
        request.setValue(postLength, forHTTPHeaderField:"Content-Length")
        request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        if onController != nil {
            if !(onController?.isKind(of: IncognitoHomeVC.self))! {
                DataUtils.showLoader(onView: onController!.view)
            }
        }
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            guard error == nil else {
                
                return
            }
            // make sure we got data
            guard let responseData = data else {
                
                return
            }
//            do {
//                guard let jsonResult = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject] else {
//
//                    return
//                }
//                DispatchQueue.main.async {
//                    DataUtils.removeLoader()
//                }
//                if let dictFromJSON = jsonResult as? [String:AnyObject] {
//                    let locData = dictFromJSON["message"] as? String
//                    let status = dictFromJSON["status"] as? Int
//
//                    if locData != nil{
//                        DispatchQueue.main.async {
//                          //  DataUtils.showAlert(title: "", msg: locData!, selfObj: onController!) {
//                             completionBlock(responseData)
//                         //   }
//                        }
//                    }
//                }
//            }catch  {
//
//                return
//            }
            DataUtils.removeLoader()
            let responseString = String(data: responseData, encoding: .utf8)
            print(responseString)
            completionBlock(responseData)
        }
        dataTask.resume()
    }
    public static func requestWithFormData(endUrl: String,fileTypeInfo : [FileTypeInfo], parameters: [String : Any], onCompletion: ((Data?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil,onController : UIViewController?){
        let progressView = UIProgressView.init(progressViewStyle: .bar)
        progressView.frame = CGRect(x: (onController?.view.frame.size.width)!/4, y: (onController?.view.frame.size.height)!/2 - 30, width: (onController?.view.frame.size.width)!/2, height: 20)
        progressView.backgroundColor = UIColor.white
        onController?.view.addSubview(progressView)
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        if onController != nil {
                   DataUtils.showLoader(onView: onController!.view)
               }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            for file in fileTypeInfo {
            if fileTypeInfo.count > 0 {
                if let data = fileTypeInfo[0].fileData{
                    multipartFormData.append(data, withName: fileTypeInfo[0].uploadFileKey, fileName: fileTypeInfo[0].fileName, mimeType: fileTypeInfo[0].mimeType)
                }
            }
        }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                   // print(result)

                    DataUtils.removeLoader()
                
                    print("Succesfully uploaded")
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    progressView.isHidden = true
                    onCompletion?(response.data)
                }
                upload.uploadProgress(closure: { (progress) in
                    progressView.setProgress(Float(progress.fractionCompleted), animated: true)
                     print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
}
