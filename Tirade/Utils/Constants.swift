//
//  Constants.swift
//  Tirade
//
//  Created by admin on 01/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

var INCOGNITOSHOUTOUT = "Shout-Out"
var INCOGNITOREPRESENT = "Represent"

var INCOGNITOTRASH = "TRASH TALK"
var INCOGNITODEBATE = "DEBATE"
var INCOGNITOCALLOUT = "Whistle Blower"
var INCOGNITOJOKES = "JOKES"
var INCOGNITOFORUMS = "FORUM"
var INCOGNITOSHAME = "SHAME"
var INCOGNITOAWARDS = "AWARDS"


var INCOGNITOTRASHIMG = "trashform"
var INCOGNITODEBATEIMG = "debateform"
var INCOGNITOCALLOUTIMG = "calloutform"
var INCOGNITOJOKESIMG = "jokesform"
var INCOGNITOFORUMSIMG = "forumsform"
var INCOGNITOSHAMEIMG = "wall_of_shameform"


var INCOGNITOTRASHIMGFORM = "trashform"
var INCOGNITODEBATEIMGFORM = "debateform"
var INCOGNITOCALLOUTIMGFORM = "calloutform"
var INCOGNITOJOKESIMGFORM = "jokesform"
var INCOGNITOFORUMSIMGFORM = "forumsform"
var INCOGNITOSHAMEIMGFORM = "wall_of_shameform"

var INCOGNITOBACKTRASHIMG = "trashBack"
var INCOGNITOBACKDEBATEIMG = "debateBack"
var INCOGNITOBACKCALLOUTIMG = "debateBack"
var INCOGNITOBACKJOKESIMG = "debateBack"
var INCOGNITOBACKFORUMSIMG = "fourmBack"
var INCOGNITOBACKSHAMEIMG = "shameBack"

var INCOGNITOTXTTRASHIMG = "trashTxt"
var INCOGNITOTXTDEBATEIMG = "debateTxt"
var INCOGNITOTXTCALLOUTIMG = "calloutTxt"
var INCOGNITOTXTJOKESIMG = "jokesTxt"
var INCOGNITOTXTFORUMSIMG = "fourmTxt"
var INCOGNITOTXTSHAMEIMG = "shameTxt"




//*******************************************PUBLICSIGNUP****************************************//
var ZIPCODEARR = ["64720","63730","64401","64830","64402","63430","64722","64723","63431","64831","64831","63620"]

//*******************************************SERVER URL'S****************************************//
var BASEURL = "https://vridhisoftech.in/tirade/"
var PUBlICSIGNUPURL = "public/user/publicSignup/"
var INCOGNITOSIGNUPURL = "incognito/user/incognitoSignup"
var POSTCATEGORYDATA = "incognito/postdata/postdata.php"
var POSTCATEGORYNEWSFEEDDATA = "incognito/postdata/news_feed.php"

var TRASHHOMEACTION = "incognito/action/action.php"
var TRASHGETHOMEDATA = "incognito/postdata/getdata.php"
var TRASHGETNEWSFEED = "incognito/postdata/get_news_feed.php"

var POSTCOMMENT = "incognito/comment/postcomment.php"
var TIRADEHOMEDATA = "public/postdata/getdata/"
var POSTTIRADHOMEDATA = "public/postdata/postdata/"

var POSTSENDDEBATE = "incognito/user/notification/"
var POSTGETNOTIFICATION = "incognito/user/get_notification/"
var TRASHTIRADEACTION = "public/action/action.php"
var POSTTIRADECOMMENT = "public/comment/postcomment.php"


var TIRADEPROFILEPOST = "public/postdata/public_profile.php"
var TIRADEPROFILEGET = "public/postdata/getprofile/"
var TIRADETRENDINGVIDEOS = "public/postdata/trending_videos/"


var INCOGNITOFORUMPOSTDATA = "incognito/forum/post_forum.php"
var INCOGNITOFORUMGETDATA = "incognito/forum/get_forum.php"


var FORUMVIDEOACTION = "incognito/forum/forum_like_action/"
var FORUMVIDEOACTIONCOMMENT = "incognito/forum/forum_comments/"
var FORUMDETAILALL = "/incognito/forum/forum_detail/"


var MADSKILLGETDATA = "madeskills/postdata/getdata/"
var MADSKILLPOSTDATA = "madeskills/postdata/postdata.php"
var MADTOPSKILL = "madeskills/postdata/get_made_skills.php"
var MADSKILLDETAIL = "madeskills/postdata/get_skills_detail/"
var RATETOPSKILL = "madeskills/postdata/rate_top_skills.php"

//***************************************INCOGNITOTIRASHCATEGORY***********************************//
var ARRTRASHCATEGORY = ["Politics","Race","Government","LBGT","Business","Legel","Religeon","Sex","Drugs","Music","Sports","Education", "Law enforecment","Medical","Love","Culture","Language","Behavior","Psyche","Beauty","Community","Exercise"  ,"Cooking","Cosmology","Dancing","Gender","Dream Interpretation","Eschatology","Courtship","Ethics","World Relations","Etiquette","Faith ","Food","Taboos","Funeral Rites","Games","Gift Giving","Healing","Hairstyle","Hospitality","Housing","Hygiene","Incest","Inheritance","Rules","Joking","Kinship","Relatives","Systems","Categorizing","sexual restrictions","Soul Concept","Luck","Superstition","Magic", "Marriage","Meal times","Medicine","Obstetrics","Pregnancy","Childbirth","Rituals","Penal Sanctions","Punishment of Crimes","Personal Names", "Population","Policy","Post Natal Care","Property Rights","Propitiation of Supernatural Beings" ,"Puberty","Customs","Politics","Other"]


//***************************************VISIONBOARD***********************************//

var ARRCATHEADER = ["Create Story","Choose Background","Choose Background","Create Group","PetsPage","Manage Page"]

var ARRCATHEADERSELECTION = ["Story","Theame","Music","Group","Create Page","Page"]
var ARRIMGEHADERSELECTION = ["logoWithBlack","paint","music","yellowGroup","whitePlus","whitePlus"]




//***************************************SETTINGS***********************************//
let TERMSEOFUSE = """
Last Updated: October 11, 2019

Tirade, (“Tirade” or “we”) provides the Tirade  mobile application, and other online products and services (we call these the "Services"). By using the Services, you agree to be bound by these Terms of Use (“Terms”). If you do not agree to these Terms, including the mandatory arbitration provisions in Section 0, do not access or use the Services. If Tirade makes changes to these Terms, we will provide notice by posting a notice on the Services or updating the "Last Updated" date above. Your continued use of the Services will confirm your acceptance of the changes.

AUDIENCE AND ACCOUNTS

You must be at least 18 years of age to use the Services or submit any information to Tirade. If you are between 18 and 20 years of age, you may only use the Services under the supervision of a parent or legal guardian who agrees to be bound by these Terms.

By using the Services, you agree to: (a) maintain the security of your account by not sharing your password (if you have an account with a password) or other access credentials with others and restricting access to your account and your computer or mobile device; (b) promptly notify Tirade if you discover or otherwise suspect any security breaches related to the Services; and (c) take responsibility for all activities that occur under your account and accept all risks of unauthorized access.

You understand that you are responsible for all data charges you incur by using the Services.

USER CONTENT

The Services allow you and other users to create, post, send and store content, including messages, text, photos, and other materials (we call these "User Content"). You agree not to post, store, create or otherwise publish or send through the Services any User Content that violates our Community Guidelines, which we may update from time to time. Tirade has the right to delete or remove any User Content that we, in our sole discretion, view as in violation of these Terms or our Community Guidelines or for any other reason.

If you submit or post User Content to the Services, you grant Tirade a nonexclusive, royalty-free, worldwide, and fully sub-licensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, perform and display your User Content in any media (now known or later developed), including in connection with Tirade’s marketing and promotional activities. You agree that this license includes the right for Tirade to make User Content available to other companies, organizations or individuals for the syndication, broadcast, distribution or publication on the Services or on other media and services. You further grant Tirade the right to publicly display your username in connection with User Content.


You understand that User Content may be displayed publicly. Tirade does not control, take responsibility for or assume liability for any User Content or any loss or damage related to User Content.

You may only post User Content that (a) is non-confidential; (b) you have all necessary rights to post to the Services; (c) is accurate and not misleading or harmful in any manner; and (d) does not and will not violate these Terms or any applicable law, rule or regulation.

In connection with User Submissions, you affirm, represent, and/or warrant that: (i) you own or have the necessary licenses, rights, consents, and permissions to use and authorize Tirade to use all patent, trademark, trade secret, copyright or other proprietary rights in and to any and all User Submissions to enable inclusion and use of the User Submissions in the manner contemplated by the mobile application and these Terms of Use; and (ii) you have the written consent, release, and/or permission of each and every identifiable individual person in the User Submission to use the name or likeness of each and every such identifiable individual person to enable inclusion and use of the User Submissions in the manner contemplated by the application and these Terms of Use.

In connection with User Submissions, you further agree that you will not: (i) submit material that is copyrighted, protected by trade secret or otherwise subject to third party proprietary rights, including privacy and publicity rights, unless you are the owner of such rights or have permission from their rightful owner to post the material and to grant Tirade all of the license rights granted herein; (ii) publish falsehoods or misrepresentations that could damage Tirade or any third party; (iii) submit material that is unlawful, obscene, defamatory, libelous, threatening, pornographic, harassing, hateful, racially or ethnically offensive, or encourages conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate; (iv) post advertisements or solicitations of business: (v) impersonate another person. Tirade does not endorse any User Submission or any opinion, recommendation, or advice expressed therein, and Tirade expressly disclaims any and all liability in connection with User Submissions. Tirade does not permit copyright infringing activities and infringement of intellectual property rights on its mobile application, and Tirade will remove all Content and User Submissions if properly notified that such Content or User Submission infringes on another's intellectual property rights. Tirade reserves the right to remove Content and User Submissions without prior notice. Tirade will also terminate a User's access to its mobile application, if they are determined to be a repeat infringer. A repeat infringer is a User who has been notified of infringing activity more than twice and/or has had a User Submission removed from the mobile application more than twice. Tirade may remove such User Submissions and/or terminate a User's access for uploading such material in violation of these Terms of Use at any time, without prior notice and at its sole discretion.

You understand that when using the Tirade mobile application, you will be exposed to User Submissions from a variety of sources, and that Tirade is not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such User Submissions. You further understand and acknowledge that you may be exposed to User Submissions that are inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against Tirade with respect thereto, and agree to indemnify and hold Tirade, its Owners/Operators, affiliates, and/or licensors, harmless to the fullest extent allowed by law regarding all matters related to your use of the site.

Tirade permits you to link to materials on the mobile application for personal, non-commercial purposes only. Tirade reserves the right to discontinue any aspect of the Tirade mobile application at any time.

Tirade is developed by Tirade, Inc. and does not have any affiliation with the content providers. We reserve the right to change the source of data without prior notice. Tirade does not claim to own or create any of the data, or their underlying content.

ACCEPTABLE USE OF THE SERVICES

You agree that your use of the Services will not violate any law, contract, intellectual property or other third-party right or constitute a tort, and that you are solely responsible for your conduct while on the Services and you use the Services at your own risk. You further agree not to:


Use the Services in any manner that could inhibit other users from fully enjoying the Services or that could damage or impair the functioning of the Services;


Engage in any discriminatory, defamatory, hateful, harassing, abusive, obscene, threatening, physically dangerous, or otherwise objectionable conduct;


Attempt to indicate that you have a relationship with Tirade or that Tirade has endorsed you or any products or services;


Send any unsolicited advertising or promotional materials or collect the email addresses or other contact information of other users from the Services for the purpose of sending commercial messages;


Attempt to reverse engineer any aspect of the Services or do anything that might circumvent measures employed to prevent or limit access to any area, content or code of the Services (except as otherwise expressly permitted by law);


Use or attempt to use another’s account without authorization from such user and Tirade;


Develop any third-party application that interacts with the Services without Tirade’s prior written consent;


Use any automated means or interface not provided by Tirade to access the Services;


Use the Services or the Service Materials (defined below) for anything other than their intended purpose or in a way that violates our Community Guidelines.


COPYRIGHT, TRADEMARK AND LIMITED LICENSE

ALL COPYRIGHTS AND TRADEMARKS NOT THE PROPERTY OF TIRADE, INCLUDING BUT NOT LIMITED TO DESIGN, CHARACTERS, TRANSLATIONS AND LOGOS THAT ARE USED OR REFERRED TO ON THE MOBILE APPLICATION ARE THE PROPERTY OF THEIR RESPECTIVE OWNERS.

The Tirade logos and any other product or service name or slogan contained in the Services are trademarks of Tirade and its suppliers or licensors. Unless otherwise indicated, the Services and all content and other materials on the Services, including, without limitation, the Tirade logo and all designs, text, graphics, pictures, videos, information, data, software, sound files, other files (collectively, the "Service Materials") as well as their selection and arrangement are the proprietary property of Tirade or its licensors or users and are protected by U.S. and international copyright, trademark and other laws. Except as explicitly stated in these Terms, Tirade does not grant any express or implied rights to use Service Materials.

You are granted a limited, non-transferable and revocable license to access and use the Services and Service Materials for your personal, non-commercial use. This license is revocable at any time.

THIRD-PARTY CONTENT

The Services may include links and other content owned or operated by third parties, including advertisements and social "widgets" (we call these "Third-Party Content"). You agree that Tirade is not responsible or liable for Third-Party Content and that you access and use Third-Party Content at your own risk. Tirade has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party. In addition, Tirade will not and cannot censor or edit the content of any third-party site. Your interactions with Third-Party Content are solely between you and the third party providing the content. When you leave the Services, you should understand that Tirade’s terms and policies no longer govern and that the terms and policies of those third party sites or services will then apply.

By using the mobile application, you expressly relieve Tirade and any third party provider, and their respective affiliates and the officers, directors, employees and agents of the foregoing, from any and all claims, causes of action, costs, damages and expenses of any type and nature whatsoever, arising out of or related to such content or from any and all liability arising from your use of any third-party website or mobile application and the content thereof. Accordingly, you must read the terms and conditions and privacy policy of each other mobile application or website that you visit from our application or from whom content is posted on our application. Where possible, we will post a citation or other reference to the source site and all User Submissions must site to third party materials.

FEEDBACK AND OTHER SUBMISSIONS

Separate from User Content, you may submit questions, comments, feedback, suggestions, and other information regarding the Services (we call these "Submissions"). You acknowledge and agree that Submissions are non-confidential and will become the sole property of Tirade. You agree to execute any documentation required by Tirade to confirm such assignment to Tirade.

COPYRIGHT COMPLAINTS

Tirade respects the intellectual property of others. If you believe that your work has been copied in a way that constitutes copyright infringement, you may notify Tirade’s Designated Agent.

E-Mail Address:


Please see 17 U.S.C. §512(c)(3) for the requirements of a proper notification. You should note that if you knowingly misrepresent in your notification that the material or activity is infringing, you may be liable for any damages, including costs and attorneys’ fees, incurred by Tirade or the alleged infringer as the result of Tirade’s relying upon such misrepresentation in removing or disabling access to the material or activity claimed to be infringing.

INTERACTIONS WITH OTHER USERS

Tirade provides a platform for users to communicate and interact with one another. You are solely responsible for your interactions with others and Tirade has no responsibility or liability with respect to any online or offline interactions. We reserve the right, but have no obligation, to monitor interactions between you and other users of our Services. Please use your best judgment and keep safety in mind when you use the Services and interact with others.

INDEMNIFICATION

You agree to defend, indemnify and hold harmless Tirade, its independent contractors, service providers and consultants, and their respective directors, officers, employees and agents (collectively, "Tirade Parties"), from and against any claims, damages, costs, liabilities and expenses (including, but not limited to, reasonable attorneys' fees) arising out of or related to (a) your use of the Services; (b) any User Content or Submission you provide; (c) your violation of these Terms; (d) your violation of any rights of another; or (e) your conduct in connection with the Services.



DISCLAIMERS

EVEN THOUGH THIS RIDE IS 100% INCOGNITO, IF YOU DO ANYTHING TO THREATEN HARM, OR DEATH TO ANYONE ’S LIFE, WE PERSONALLY PROMISE YOU THAT WE WILL COOPERATE WITH AUTHORITIES TO BRING YOU TO JUSTICE. THINK BEFORE YOU SPEAK PEOPLE.

TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, THE SERVICES AND SERVICE MATERIALS ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, EXCEPT AS EXPRESSLY PROVIDED TO THE CONTRARY IN A WRITING BY TIRADE. TIRADE DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT AS TO THE SERVICES AND SERVICE MATERIALS. TIRADE DOES NOT REPRESENT OR WARRANT THAT THE SERVICES OR SERVICE MATERIALS ARE ACCURATE, COMPLETE, RELIABLE, CURRENT OR ERROR-FREE. TIRADE IS NOT RESPONSIBLE FOR TYPOGRAPHICAL ERRORS OR OMISSIONS RELATING TO TEXT OR PHOTOGRAPHY. WHILE TIRADE ATTEMPTS TO MAKE YOUR ACCESS TO AND USE OF THE SERVICES SAFE, TIRADE CANNOT AND DOES NOT REPRESENT OR WARRANT THAT THE SERVICES OR ITS SERVERS ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; THEREFORE, YOU SHOULD USE INDUSTRY-RECOGNIZED SOFTWARE TO DETECT AND DISINFECT VIRUSES FROM ANY DOWNLOAD.

LIMITATION OF LIABILITY

You assume all risk arising from your use of the Services, including but not limited to all of the risks associated with any online or offline interactions with other users. You agree to take any and all necessary precautions when interacting with other users.


TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL TIRADE OR THE TIRADE PARTIES BE LIABLE FOR ANY DIRECT, SPECIAL, INDIRECT, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES, OR ANY OTHER DAMAGES OF ANY KIND, INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE) OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF OR INABILITY TO USE THE SERVICES OR THE SERVICE MATERIALS, INCLUDING WITHOUT LIMITATION ANY DAMAGES CAUSED BY OR RESULTING FROM RELIANCE ON ANY INFORMATION OBTAINED THROUGH THE SERVICES OR THAT RESULT FROM THE ONLINE OR OFFLINE CONDUCT OF YOU OR ANYONE ELSE IN CONNECTION WITH THE SERVICES (INCLUDING WITHOUT LIMITATION BODILY INJURY, DEATH OR PROPERTY DAMAGE), MISTAKES, OMISSIONS, INTERRUPTIONS, DELETION OF FILES OR USER CONTENT, ERRORS, DEFECTS, VIRUSES, DELAYS IN OPERATION OR TRANSMISSION OR ANY FAILURE OF PERFORMANCE, WHETHER OR NOT RESULTING FROM ACTS OF GOD, COMMUNICATIONS FAILURE, THEFT, DESTRUCTION OR UNAUTHORIZED ACCESS TO TIRADE’S RECORDS, PROGRAMS OR SERVICES.

MODIFICATIONS TO THE SERVICES

Tirade reserves the right to modify or discontinue, temporarily or permanently, the Services, or any features or portions of the Services, without prior notice. You agree that Tirade will not be liable for any modification, suspension or discontinuance of the Services, or any part of the Services.

ARBITRATION

PLEASE READ THIS SECTION CAREFULLY BECAUSE IT REQUIRES YOU TO ARBITRATE DISPUTES WITH TIRADE AND LIMITS THE MANNER IN WHICH YOU CAN SEEK RELIEF FROM TIRADE.


Except for any dispute in which either party seeks to bring an individual action in small claims court or seeks injunctive or other equitable relief for the alleged unlawful use of copyrights, trademarks, trade names, logos, trade secrets, or patents, you and Tirade waive your respective rights to a jury trial and to have any dispute arising out of or related to these Terms or the Services resolved in court. Instead, all disputes arising out of or relating to these Terms or the Services will be resolved through confidential binding arbitration held in Los Angeles, California (or, alternatively, via telephone or video conference) before and in accordance with the rules of JAMS, which are available on the JAMS website. You either acknowledge and agree that you have read and understand the rules of JAMS or waive your opportunity to read the rules of JAMS and any claim that the rules of JAMS are unfair or should not apply for any reason.


YOU AND TIRADE AGREE THAT ANY DISPUTE ARISING OUT OF OR RELATED TO THESE TERMS OR THE SERVICES IS PERSONAL TO YOU AND TIRADE AND THAT YOU AND TIRADE WILL NOT COMMENCE AGAINST THE OTHER A CLASS ACTION, CLASS ARBITRATION OR OTHER REPRESENTATIVE ACTION OR PROCEEDING.


As limited by the Federal Arbitration Act, these Terms and the JAMS rules, the arbitrator will have exclusive authority to make all procedural and substantive decisions regarding any dispute and to grant any remedy that would otherwise be available in court; provided, however, that the arbitrator does not have the authority to conduct a class arbitration or a representative action, which is prohibited by these Terms. The arbitrator may only conduct an individual arbitration and may not consolidate more than one individual’s claims, preside over any type of class or representative proceeding or preside over any proceeding involving more than one individual.


Any claim you may have arising out of or related to these Terms or our Services must be filed within one year after such claim arose; otherwise, your claim is permanently barred. You and Tirade agree that you will notify each other in writing of any dispute within thirty (30) days of when it arises. Notice to Tirade must be sent to ______________(email)

APPLICABLE LAW AND VENUE

These Terms and your use of the Services shall be governed by and construed in accordance with the laws of California without resort to its conflict of law provisions. To the extent the arbitration provision in Section 0 does not apply (if ever), you agree that any action at law or in equity arising out of or relating to these Terms shall be filed only in the state and federal courts located in Los Angeles County, California and you hereby irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of these Terms.

TERMINATION

Tirade reserves the right, without notice and in its sole discretion, to terminate your license to use the Services and to block or prevent future your future access to, and use of, the Services.

SEVERABILITY

If any provision of these Terms shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions.

QUESTIONS & CONTACT INFORMATION

Questions or comments about the Services may be directed to Tirade at __________(email)
"""
let TERMSEOFSERVICE = """
YOUR USE OF THIS WEB SITE CONSTITUTES YOUR AGREEMENT TO BE BOUND BY THESE TERMS AND CONDITIONS OF USE.
Acceptance of Terms of Use
This site (the “Site”) is operated by Tirade Inc. By accessing or using this Site (or any part thereof), you agree to be legally bound by the terms and conditions that follow (the “Terms of Use”) as we may modify them from time to time. These Terms of Use apply to your use of this Site, including the services offered via the Site. They constitute a legal contract between you and Tirade Inc. and by accessing or using any part of the Site you represent and warrant that you have the right, power and authority to agree to and be bound by these Terms of Use. If you do not agree to the Terms of Use, or if you do not have the right, power, and authority to agree to and be bound by these Terms of Use, you may not use the Site. Notwithstanding anything to the contrary herein, if you and Tirade Inc. have entered into a separate written agreement that covers your use of a Tirade Inc. service, the terms and conditions of such agreement shall control with respect to such service to the extent they are inconsistent with these Terms of Use.
Purpose of Site
The Site is intended to provide information about commercial real estate, services, and other property related services and Tirade Inc. The information and services offered on or through this Site are provided solely for general business information, do not constitute real estate, legal, tax, accounting, investment or other professional advice, or an offer to sell or lease real estate, and may not be used for or relied upon for these purposes. No lawyer-client, advisory, fiduciary or other relationship is created by your accessing or using this Site or communicating by way of email or through this Site. You shall not use information and services offered on or through this Site for personal, family or household purposes or to determine an individual’s eligibility for credit, insurance, employment, or government license or benefit.
Services
The information, data, text, software, photographs, images, graphics, organization, layout, design, and other content contained on or provided through this Site (collectively, the “Content”) are proprietary to Tirade Inc. and its licensors, and are protected by copyright and other international intellectual property rights, laws and treaties. The Content includes proprietary databases (the “Database”) of commercial real estate information, which, by way of example, include information, text, photographic and other images and data contained therein (collectively, the “Information”) and the proprietary organization and structures for categorizing, sorting and displaying such Information, and the related software (“Software”). The Site, Content, Database, Information, Software and any portion of the foregoing, including any derivatives, adaptations, successors, updates or modifications provided thereto and any information derived from the use of the Database, including as a result of the verification of any portion of the Information, are collectively referred to herein as the “Product”. Those portions of the Product that may be accessed by the general public and that do not require any use of Passcodes (as defined below) or facial recognition authentication are referred to as the “Non-Passcode Protected Product”. Those portions of the Product that require use of Passcodes and, if applicable, facial recognition authentication for access and are available only to individuals or entities (“Tirade Inc.  Clients”), or those acting through them, who enter into a License Agreement (as defined below) with Tirade Inc.  that authorizes access to such Tirade Inc.  service are referred to as the “Passcode Protected Product”. The Passcode Protected Product includes, registered and unregistered trademarks protected by United States and other laws. A “License Agreement” is defined as either (i) a written and signed contract between a Tirade Inc. Client and Tirade Inc. that authorizes access to a Tirade Inc.  service, or (ii) an online contract between the Tirade Inc. Client and Tirade Inc. that is formed by online registration and acceptance of these Terms of Use or other online contract established by Tirade Inc.  and that authorizes access to a Tirade Inc. service. Only Authorized Users (defined below) for a Passcode Protected Product may access such product and they may access it solely using the user name, password (collectively, the “Passcodes”) and, if required by Tirade Inc., facial recognition authentication..
Unauthorized attempts to
(i) defeat or circumvent Passcodes or other security features (e.g., facial recognition authentication), (ii) use the Site or the Product for other than intended purposes, or (iii) obtain, alter, damage or destroy information or otherwise to interfere with the system of its operation are not permitted and may result in a loss of access to the Passcode Protected Product. Evidence of such acts may also be disclosed to law enforcement authorities and result in criminal prosecution under the laws of the United States or such other jurisdictions as may apply.
An “Authorized User” is defined as an individual
(a) employed by a Tirade Inc. Client or an Exclusive Contractor (as defined below) of a Tirade Inc. Client at a site identified in the License Agreement, and (b) who is specified in the License Agreement as a user of a specific Passcode Protected Product and represented by the Client to be an employee or Exclusive Contractor of the Client. An “Exclusive Contractor” is defined as an individual person working solely for the Tirade Inc. Client and not another company with real estate information needs or for themselves and performing substantially the same services for such Tirade Inc. Client as an employee of such Tirade Inc. Client.
Permitted Uses
Subject to the provisions in these Terms of Use, you may use the Non-Passcode Protected Product (or, if you are an Authorized User, subject to the provisions in your License Agreement and these Terms of Use, you may use the applicable Passcode Protected Product), in the ordinary course of your business for: (1) Your internal research purposes; (2) Providing information regarding a limited number of particular properties and market trends to your clients and prospective clients; (3) Marketing properties; (4) Supporting your valuation, appraisal or counseling regarding a specific property; and (5) Creating periodic general market research reports for in-house use or for clients’ or prospective clients’ use, provided that such reports do not contain building-specific or tenant-specific Information and are not commercially or generally distributed.
Subject to the provisions in your License Agreement and these Terms of Use, you may print Information or copy Information into word processing, spreadsheet and presentation programs (or other software programs with the express written consent of Tirade Inc., so long as the level of Information being printed or copied is reasonably tailored for your purposes, insubstantial and used in compliance with these use and copying provisions. Notwithstanding the foregoing, the Information you access through the “Free Building Lookup” feature on this Site may only be viewed by you for your individual, non-commercial use while visiting this Site.
Prohibited Uses
You shall not, except (i) as may be expressly set forth above under “Permitted Uses” and (ii) to the extent necessary to integrate commercial property listings within Tirade Inc.  commercial real estate marketing website available through Tirade Inc. Showcase®, (a) distribute, disclose, copy, reproduce, communicate to the public by telecommunication, make available, display, publish, transmit, assign, sublicense, transfer, provide access to, use, rent or sell, directly or indirectly (including in electronic form) any portion of the Product, or (b) modify, adapt or create derivative works of any portion of the Product. Notwithstanding anything to the contrary herein, you shall not: (1) Access any portion of a Passcode Protected Product unless you are an Authorized User for such Passcode Protected Product using the Passcodes assigned to you by Tirade Inc.  to access the components and services of the Passcode Protected Product that your License Agreement authorizes you to access, subject to the terms contained therein and in these Terms of Use; (2) Access or use any portion of the Product if you are a direct or indirect competitor of Tirade Inc., nor shall you provide, disclose or transmit any portion of the Product to any direct or indirect competitor of Tirade Inc. (by way of example, a “direct or indirect competitor” of Tirade Inc. includes, but is not limited to, Internet listing services or other real estate information services and employees, independent contractors and agents of such services); (3) Provide your Passcode or otherwise provide access to a Passcode Protected Product to any individual other than yourself, including by providing the results of queries of or reports generated from a Passcode Protected Product to a person who is not a client or prospective client. (4) Use or distribute any Information from the Product, including Information that has been verified or confirmed by you or anyone else, to directly or indirectly create or contribute to the development of any database or product; (5) Modify, merge, decompile, disassemble, scrape, translate, decode or reverse engineer any portion of the Product, or use any data mining, gathering or extraction tool, or any robot, spider or other automatic device or manual process, to monitor or copy any portion of the Product or the data generated from it; (6) Use, reproduce, publish or compile any portion of the Product for the purpose of selling or licensing any portion of the Product or making any portion of the Product publicly available; (7) Store, copy or export any portion of the Product into any database or other software, except as expressly set forth in the Permitted Uses above; (8) Upload, post or otherwise publish any portion of the Product on, or provide access to any portion of the Product through, the Internet, any bulletin board system, any other electronic network, any data library, any listing service or any other data sharing arrangement, except that you may e-mail a report containing Information that complies with the Permitted Use provisions set forth above to a limited number of your clients and prospective clients; (9) Upload, post, e-mail, make available or otherwise transmit or communicate to the public by telecommunication any information, data, text, software, photographs, images, graphics, or other content to or through the Product, or use any portion of this Product in a manner, that: (a) is unlawful, threatening, abusive, harmful, libelous, tortious, defamatory, false, misleading, obscene, vulgar, racially or ethnically offensive, invasive of privacy or publicity rights, inclusive of hate speech, or would constitute or encourage a criminal offence, violate the rights of any party, give rise to liability or violate any local, provincial, federal or international law, intentionally or unintentionally, or is otherwise objectionable; (b) infringes any patent, copyright, trademark, trade secret, or other proprietary right of any party or violates the privacy or publicity rights of any party; (c) constitutes unlawful advertising or fraudulent, unfair or deceptive practices, “spam,” or any other form of unlawful solicitation in the United States, Canada or other county, including the Kansas non-solicitation law (K.S.A. 45-230), which, with limited exceptions, prohibits anyone from knowingly selling, giving or receiving, for the purpose of selling or offering for sale any property or service to persons listed therein, any list of names and addresses contained in or derived from Kansas public records; or (d) contains software viruses or any other computer code, files or programs that are designed to or have the capability to interrupt, modify, damage, improperly access, disable, destroy or limit the functionality of the Product or servers or networks connected thereto or the activities of other users of the Product or of any computer software or hardware or telecommunications equipment. (10) Except as set forth in a License Agreement, you acknowledge and agree that you do not have a right to make available, communicate to the public by telecommunication, transmit under any law, contractual obligation (i.e., nondisclosure agreement) or fiduciary duty any information about the Product. (11) Impersonate any person or entity, including but limited to an Authorized User, or falsely state or otherwise misrepresent any registration information, or otherwise disguise the origin of any information, data, text, software, photographs, images, graphics, or other content posted on or transmitted through the Product; and (12) Use any portion of the Product to encourage or engage in illegal activity, stalk or harass another person, or violate these Terms of Use or any applicable local, state, provincial, national or international law, rule, regulation or ordinance, including without limitation, state, provincial and local real estate practice, spam or privacy laws.
Fees
You are responsible for the timely payment of any fees incurred by your use of components and services available on the Site or via links to other web sites, and all taxes applicable to your use of the Product. SUBJECT TO THE TERMS OF ANY LICENSE AGREEMENT BETWEEN Tirade Inc.  AND YOUR COMPANY, WE RESERVE THE RIGHT, AT ANY TIME AND FROM TIME TO TIME, TO PROSPECTIVELY CHANGE THE NATURE AND AMOUNT OF FEES CHARGED FOR ACCESS TO THE PRODUCT OR ANY OF THE COMPONENTS OR SERVICES AVAILABLE ON THE PRODUCT, AND THE MANNER IN WHICH SUCH FEES ARE ASSESSED. If you are accessing the Passcode Protected Product, you agree: (1) to provide Tirade Inc.  with accurate and complete registration and billing information and to promptly update such information in the event it changes; and (2) to pay any applicable license fees or other fees incurred by your use of the Passcode Protected Product. With respect to the online registration for subscribing to market commercial real estate listings through Tirade Inc. Showcase® (Showcase.com), you agree to a recurring monthly charge on your credit card until canceled by you upon 30 days written notice to Tirade Inc.
Termination and Interruption of Access
You acknowledge and agree that Tirade Inc. may interrupt, terminate, discontinue, or block your access to the Product or portions thereof at any time, subject to the terms of any License Agreement in place between you or your company and Tirade Inc.
Tirade Inc. reserves the right to terminate or suspend your use of a Tirade Inc. service or to terminate your License Agreement upon a good faith determination of a violation of the terms of any material provision of any other agreement between the parties or their affiliates. In the event such suspension or termination occurs, you shall cease using any portion of the Product, permanently delete or destroy all portions of the Product within your possession, custody or control, and, upon written request from Tirade Inc., certify, in writing, your compliance with this provision.
If you are a Tirade Inc. Client, your License Agreement sets forth an initial term that expires on a specified date and that may automatically renew for a specified length. Following the effective date of termination or non-renewal of your License Agreement, you shall cease using any portion of the Product. In addition, you shall permanently delete or destroy all portions of the Product within your possession, custody or control and, upon written request from Tirade Inc., certify, in writing, your compliance with this provision.
Confidentiality Within Web Version of Tirade Inc. PropertyProfessional®
The documents stored in the “My Survey” section of the Tirade Inc. Property Professional® service are intended to be confidential. Except as otherwise provided herein, Tirade Inc. personnel shall not review or disclose to others the documents or other information stored there. Authorized access is designed to be restricted to the Authorized User(s) that store the information there and other users designated by such Authorized User(s) to have access to the information.
Consistent with this goal of confidentiality, in addition to any other right provided for herein, Tirade Inc. reserves the right to compile statistical information regarding use of various features of this Site and Tirade Inc. services, including the “My Survey” section of Tirade Inc. Property Professional. Tirade Inc. also reserves the right for Tirade Inc.  and its contractors to access any portion of its services to perform customer support, product or system development, routine security inspections, to protect against unauthorized use of our products or services, to respond to legal process, or if otherwise required to do so by law.
Submitted Content
This Product may include opportunities for users to submit information, data, text, photographs, images, graphics, messages, links, expressions of ideas and other content to the Product, for it to be publicly displayed on the Product, used by Tirade Inc. in connection with researching real estate activity, or for some other purpose (“Submitted Content”). Tirade Inc. acknowledges that if you provide Tirade Inc. with any information or images, then you retain any applicable ownership rights that you may have with respect to such information and images. However, you understand that all such Submitted Content, whether publicly posted or privately transmitted, is the sole responsibility of the person from which such content originated. This means that you, and not Tirade Inc., are entirely responsible for all such content that you upload, post, e-mail or otherwise transmit to or via the Product. Tirade Inc. is under no obligation to post or use any such Submitted Content you may provide and may remove any such content at any time in Tirade Inc. sole discretion.
You agree that Tirade Inc. may adjust portions of the Information contained within the Product. Any such adjustment will have no material impact on the meaning and interpretation of the Information, but will serve as a means of uniquely identifying the Information as having been supplied to you. You accept that this is a legitimate and lawful security precaution on the part of Tirade Inc., and accept further that in the event that any third party has access to Information that can be identified as having your unique adjustments a prima facie breach of security and of these Terms of Use on your part may be assumed by Tirade Inc.
With respect to all Submitted Content you elect to upload, post, e-mail or otherwise transmit to or via the Product, you grant Tirade Inc. and its Licensees a royalty-free, perpetual, irrevocable, non-exclusive and fully sub-licensable right and license (through multiple tiers) to use, reproduce, communicate to the public by telecommunications, make available, adapt, perform, display, publish, translate, prepare derivative works from, modify, distribute, sell, rent and take any other action with respect to such content (in whole or part) worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed. You further acknowledge and agree that Tirade Inc. may preserve any such content and may also disclose such content in its discretion. The foregoing license is without restrictions of any kind and without payment due from Tirade Inc. You also hereby forever waive and agree never to assert any and all Moral Rights you may have in or with respect to any Submitted Content. “Moral Rights” means any so-called “moral rights” or “droit moraux” or any similar right which you may have in any Submitted Content, existing under judicial or statutory law of any country in the world, or under any treaty. For greater certainty, these so-called “moral rights” or “droits moraux” shall not include the so-called “paternal right”.
You represent and warrant that
(a) you own or have the full right, power and authority to grant to Tirade Inc. use of and rights in and to all Submitted Content that you upload, post, e-mail or otherwise transmit to or via the Product; (b) your license of such content to Tirade Inc.  hereunder does not, and the use or license of such content by Tirade Inc. to third parties will not, infringe any right or interest owned or possessed by any third party; and (c) there are no claims, judgments or settlements to be paid by you, or pending claims or litigation, relating to such content. You acknowledge and agree that your submitting Submitted Content to the Site does not create any new or alter any existing relationships between you and Tirade Inc.. Tirade Inc. has no obligation to monitor or screen Submitted Content and is not responsible for Submitted Content. However, Tirade Inc. reserves the right, in its sole discretion, to monitor Submitted Content, edit Submitted Content or delete Submitted Content at any time for any reason or no reason.
Tirade Inc. Lease AnalysisTM
You own the lease information you submit to Lease Analysis. What’s yours stays yours, and you can do anything you want with your data. If you are a commercial real estate owner, investor, broker or property manager, as long as you are a client, Tirade Inc.  will always provide you access to your information in Lease Analysis at your request. And if you are a firm that owns, invests in, brokers, or manages commercial real estate, as long as you are a client, we will also always provide you with access to the lease information submitted by your users at your request.
Tirade Inc. will protect the lease information you submit and will treat it confidentially. This means that the researchers that collect the data in Tirade Inc. cannot see your information and that Tirade Inc. will secure it in a separate database.
Tirade Inc. will also never disclose or make identifiable the details of any specific lease that you submit, such as the property address or owner or tenant name. Instead, when you upload lease information to Lease Analysis, you grant Tirade Inc. permission to aggregate that information, identify trends and disclose those trends within analyses or reports we may provide to our customers. Any such analysis or report will not include the details of any submitted lease and will not otherwise disclose information sufficient to identify any lease you submit. We will apply a “Rule of 5”. This means that Tirade Inc.  will only disclose aggregated lease data based on information taken from 5 or more leases, involving at least 5 different buildings, owners and tenants. For example, a report provided by Tirade Inc. analyzing effective rents in Toronto, Ontario would include aggregated effective rent information taken from a minimum of 5 leases involving at least 5 different buildings, owners and tenants, and the report would never disclose the specific details of a lease you submitted. In the event that you submit any personal information to Lease Analysis, you hereby represent and warrant that you have all consents necessary to share such information.
Tirade Inc. Lease CompsTM
When you create your own lease comp in Tirade Inc. Lease CompsTM, we not only protect the lease comps you add in a secure and separate database and prevent anyone at Tirade Inc. from seeing or accessing them, but we also do not use the information for any purpose, including aggregating data from comps you enter.
Map Data
Certain map data, including public transportation information, is ©Urban Mapping, Inc. and/or other parties, and is used with permission.
Links; Framing
You may provide a hyperlink to the home page of this Site, which is www.tiradeapp.com, or to www.showcase.com provided that you must remove any such link upon request from Tirade Inc.. Except as set forth in the preceding sentence, or as otherwise authorized by Tirade Inc.  in writing, links to and the framing of this Product or any of its Content is prohibited.
For your convenience, the Product may include links to other sites, some of them owned and operated by Tirade Inc. and some of them owned and operated by third parties. Under no circumstances shall Tirade Inc. be deemed to be associated or affiliated with, or viewed as endorsing or sponsoring, any web site that links to this Site, or is linked to from this Site, or any service or product that may be offered through such web sites. Tirade Inc. has not necessarily reviewed any or all of the content of such other web sites, does not guarantee the accuracy or timeliness of such sites, and disclaims responsibility for the content and services available therein. Different terms and conditions may apply to your use of any linked sites. It is your responsibility to review any such terms and conditions in connection with your use of any such sites. Any issues or disputes that may arise with respect to any such sites shall solely be between you and the applicable third party.
Information Regarding Accuracy, Completeness and Timeliness of Information in the Product
The Product is provided for general information only and should not be relied upon or used as the basis for making significant decisions without consulting primary or more accurate, more complete or more timely sources of information. Any reliance upon the Product shall be at your own risk. Neither we, nor any third party involved in creating, producing or delivering the Product, is responsible if the Product is not accurate, complete or current. Neither we, nor any third party involved in creating, producing or delivering the Product, has any responsibility for any consequence relating directly or indirectly to any action or inaction that you take based on the Product.
No Warranties
ALTHOUGH Tirade Inc. ATTEMPTS TO PROVIDE AN ACCURATE PRODUCT, THE PRODUCT AND ALL PARTS THEREOF ARE PROVIDED “AS IS”, “WITH ALL FAULTS” AND “AS AVAILABLE”. Tirade Inc.  (INCLUDING ITS AFFILIATES), AND THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, AND THIRD PARTY SUPPLIERS OR THEIR RESPECTIVE SUCCESSORS AND ASSIGNS (COLLECTIVELY, THE “Tirade Inc. PARTIES”) CANNOT AND DO NOT REPRESENT, WARRANT OR COVENANT THAT (I) THE PRODUCT WILL ALWAYS BE ACCURATE, COMPLETE, CURRENT, OR TIMELY; (II) THE OPERATION OF, OR YOUR ACCESS TO, PRODUCT THROUGH THE SITE WILL ALWAYS BE UNINTERRUPTED OR ERROR-FREE; AND/OR (III) DEFECTS OR ERRORS IN THE SITE OR THE PRODUCT, BE THEY HUMAN OR COMPUTER ERRORS, WILL BE CORRECTED.
THE Tirade Inc. PARTIES DISCLAIM ANY AND ALL REPRESENTATIONS, WARRANTIES OR GUARANTEES OF ANY KIND, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING WITHOUT LIMITATION (1) AS TO TITLE, MERCHANTABILITY, FITNESS FOR ORDINARY PURPOSES AND FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, SYSTEM INTEGRATION, WORKMANLIKE EFFORT, QUIET ENJOYMENT AND NO ENCUMBRANCES OR LIENS, (2) THE QUALITY, ACCURACY, TIMELINESS OR COMPLETENESS OF THE LICENSED PRODUCT, (3) THOSE ARISING THROUGH COURSE OF DEALING, COURSE OF PERFORMANCE OR USAGE OF TRADE; (4) THE PRODUCT CONFORMING TO ANY FUNCTION, DEMONSTRATION OR PROMISE BY ANY Tirade Inc.  PARTY, AND (5) THAT ACCESS TO OR USE OF THE PRODUCT WILL BE UNINTERRUPTED, ERROR-FREE OR COMPLETELY SECURE.
ANY RELIANCE UPON THE PRODUCT IS AT YOUR OWN RISK AND THE Tirade Inc.  PARTIES MAKE NO WARRANTIES. Tirade Inc.  RESERVES THE RIGHT TO RESTRICT OR TERMINATE YOUR ACCESS TO THE PRODUCT OR ANY FEATURE OR PART THEREOF AT ANY TIME.
Limitation of Liability
TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, THE Tirade Inc.  PARTIES WILL NOT BE HELD LIABLE FOR ANY LOSS, COST OR DAMAGE SUFFERED OR INCURRED BY YOU OR ANY THIRD PARTY INCLUDING WITHOUT LIMITATION THOSE ARISING OUT OF OR RELATED TO ANY FAULTS, INTERRUPTIONS OR DELAYS IN THE PRODUCT, OUT OF ANY INACCURACIES, ERRORS OR OMISSIONS IN THE INFORMATION CONTAINED IN THE PRODUCT, REGARDLESS OF HOW SUCH FAULTS, INTERRUPTIONS, DELAYS, INACCURACIES, ERRORS OR OMISSIONS ARISE, OR FOR ANY UNAUTHORIZED USE OF THE PRODUCT. IN ADDITION, IF YOU HAVE NOT PAID ANY FEES TO Tirade Inc.  TO ACCESS THE PRODUCT, TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, IF YOU DOWNLOAD ANY CONTENT FROM THIS PRODUCT, YOU DO SO AT YOUR OWN DISCRETION AND RISK. NONE OF THE Tirade Inc. PARTIES SHALL BE LIABLE FOR ANY DAMAGES ARISING FROM, RELATING TO OR RESULTING FROM THE PRODUCT, THESE TERMS OF USE, OR YOUR USE OR INABILITY TO USE ANY OF THE FOREGOING. THESE LIMITATIONS OF LIABILITY INCLUDE DAMAGES FOR ERRORS, OMISSIONS, INTERRUPTIONS, DEFECTS, DELAYS, COMPUTER VIRUSES, LOSS OF PROFITS, LOSS OF DATA, UNAUTHORIZED ACCESS TO AND ALTERATION OF YOUR TRANSMISSIONS AND DATA, AND OTHER TANGIBLE AND INTANGIBLE LOSSES. NOTWITHSTANDING ANY PROVISION CONTAINED HEREIN TO THE CONTRARY, AND TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, IN NO EVENT WILL THE MAXIMUM AGGREGATE, CUMULATIVE LIABILITY OF THE Tirade Inc.  PARTIES FOR ANY AND ALL REASONS TO ANY PARTY FOR DAMAGES, DIRECT OR OTHERWISE, ARISING OUT OF OR IN CONNECTION WITH THE PRODUCT, THESE TERMS OF USE, OR A SEPARATE LICENSE AGREEMENT EXCEED THE TOTAL AMOUNT OF LICENSE FEES ACTUALLY PAID TO Tirade Inc.  UNDER THE RELEVANT LICENSE AGREEMENT BETWEEN THE RELEVANT Tirade Inc.  CLIENT AND Tirade Inc.  DURING THE TWELVE MONTH PERIOD IMMEDIATELY PRECEDING THE DATE SUCH CLAIM AROSE, REGARDLESS OF THE CAUSE OR FORM OF ACTION. RECOVERY OF THIS AMOUNT SHALL BE THE SOLE AND EXCLUSIVE REMEDY FOR THE Tirade Inc.  CLIENT OR ANY OTHER PARTY FOR ANY APPLICABLE DAMAGES.
TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, UNDER NO CIRCUMSTANCES WILL ANY OF THE Tirade Inc. PARTIES BE HELD LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, EXEMPLARY, PUNITIVE OR ANY OTHER DAMAGES, INCLUDING, WITHOUT LIMITATION, LOST PROFITS, ARISING OUT OF, BASED ON, OR RESULTING FROM, OR IN CONNECTION WITH THE PRODUCT, THESE TERMS OF USE, OR YOUR USE OR INABILITY TO USE ANY OF THE FOREGOING, EVEN IF Tirade Inc.  HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING EXCLUSION OF DAMAGES IS INDEPENDENT OF THE EXCLUSIVE REMEDY DESCRIBED ABOVE AND SURVIVES IN THE EVENT SUCH REMEDY FAILS.
NONE OF THE Tirade Inc.  PARTIES SHALL HAVE ANY LIABILITY FOR ANY DAMAGES RESULTING FROM ANY FAILURE TO PERFORM ANY OBLIGATION HEREUNDER OR FROM ANY DELAY IN THE PERFORMANCE THEREOF DUE TO CAUSES BEYOND Tirade Inc. CONTROL, INCLUDING, WITHOUT LIMITATION, INDUSTRIAL DISPUTES, ACTS OF GOD OR GOVERNMENT, PUBLIC ENEMY, WAR, FIRE, OTHER CASUALTY, FAILURE OF ANY LINK OR CONNECTION WHETHER BY COMPUTER OR OTHERWISE, OR FAILURE OF TECHNOLOGY OR TELECOMMUNICATIONS OR OTHER METHOD OR MEDIUM OF STORING OR TRANSMITTING THE PRODUCT.
IF YOU ARE A CALIFORNIA RESIDENT, YOU WAIVE CALIFORNIA CIVIL CODE SECTION 1542, WHICH SAYS: “A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM MUST HAVE MATERIALLY AFFECTED HIS SETTLEMENT WITH THE DEBTOR.” THESE FOREGOING LIMITATIONS APPLY WHETHER BASED ON BREACH OF CONTRACT, BREACH OF WARRANTY, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR OTHERWISE, EVEN IF THE Tirade Inc.  PARTIES WERE NEGLIGENT OR HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
THE NEGATION OF DAMAGES SET FORTH ABOVE IS A FUNDAMENTAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN Tirade Inc. AND YOU. THE PRODUCT WOULD NOT BE PROVIDED TO YOU WITHOUT SUCH LIMITATIONS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM Tirade Inc.  THROUGH THE PRODUCT CREATE ANY WARRANTY, REPRESENTATION AND/OR GUARANTEE NOT EXPRESSLY STATED IN THIS AGREEMENT.
NO ACTION ARISING OUT OF OR PERTAINING TO THESE TERMS OF USE MAY BE BROUGHT MORE THAN ONE (1) YEAR AFTER THE CAUSE OF ACTION HAS ARISEN.
User Data
If you create any settings, surveys, fields or functions in the Product or input, add or export any data into or from the Product (collectively, the “User Data”), none of the Tirade Inc.  Parties shall have any liability or responsibility for any of such User Data, including the loss, destruction or use by third parties of such User Data. It is your responsibility to make back-up copies of such User Data.
Your Liability
If you cause a technical disruption of the Product, you agree to be responsible for any and all liabilities, costs and expenses (including reasonable attorneys’ fees, fines, and costs of enforcement) arising from or related to that disruption. Upon your breach of any term of these Terms of Use or a separate License Agreement, Tirade Inc.  remedies shall include any damages and relief available at law or in equity as well as interruption and/or termination of your access to the Product or any portion thereof and permanent deletion or destruction of all portions of the Product within your possession, custody or control. If Tirade Inc.  retains any third party to obtain any remedy to which it is entitled under these Terms of Use or a separate License Agreement, Tirade Inc. shall be entitled to recover all costs, including attorney’s fees or collection agency commissions, Tirade Inc. incurs.
Indemnity
You agree to indemnify, defend, and hold harmless the Tirade Inc.  Parties from and against any third party action, suit, claim or demand and any associated losses, expenses, damages, costs and other liabilities (including reasonable attorneys’ fees), arising out of or relating to your (and your users’) Submitted Content, use or misuse of any portion of the Product, or your violation of these Terms of Use or a separate License Agreement. You shall cooperate as fully as reasonably required in the defense of any such claim or demand. Tirade Inc.  and any third party involved in creating, producing, or delivering the Product reserves the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, at your expense, and you shall not in any event settle any such matter without the written consent of Tirade Inc.  and any such third party.
Privacy
Tirade Inc.  is committed to respecting the privacy of your personal information in connection with your use of this Site. We take great care to keep your personal information confidential and secure. However, the Internet is not a totally secure medium of communication. For this reason, we cannot guarantee the privacy of any information you input on this site, send to us, or request be delivered to you on the Internet. Tirade Inc. will not be responsible for any damages you or others may suffer as a result of the loss of confidentiality of any such information.
Use of the Product is subject to Tirade Inc.  Privacy Policy, located at www.tiradeapp.com, which is hereby incorporated into, and made part of, these Terms of Use. Tirade Inc.  reserves the right, and you authorize us, to use the Product and any and all other personal information provided by you in any manner consistent with our Privacy Policy.
Tirade Inc.  conducts product research using professional researchers conducting phone interviews of commercial real estate market participants and their agents. By using Tirade Inc.  services and agreeing to these terms of use, you consent to the recording of your telephone or other communications with Tirade Inc.  representatives, including research, customer service and sales personnel, for training, quality assurance and archival purposes.
Trademarks
The Product employs various trademarks and service marks of Tirade Inc.  and of other third parties. All of these trademarks and service marks are the property of their respective owners. You agree not to use or display them in any manner without the prior written permission of the applicable trademark owner.
Procedure for Making Notification of Claims of Copyright Infringement
Tirade Inc.  respects the intellectual property of others, and we ask those posting or transmitting any content to or through this Site to respect copyright law. It is the policy of Tirade Inc. to restrict and/or terminate in appropriate circumstances the ability to submit content and/or use this Product by individuals or entities that repeatedly submit infringing content in violation of these Terms of Use. If you believe that your work has been copied and is available on this Site or our other online services in a way that constitutes copyright infringement, you may notify Tirade Inc. according to the notice requirements of the Digital Millennium Copyright Act and any other applicable law. Pursuant to 17 U.S.C. Section 512, Tirade Inc.  DMCA registered agent can be reached as follows: by mail to Legal Department (attn: General Counsel), Tirade Inc.  Realty Information, Inc., 1331 L Street, N.W., Washington, DC 20005; by e-mail to Copyright@worldbizmarket.com; and by telephone at 202-346-6500.
Ownership
You acknowledge that Tirade Inc.  and its licensors have and shall retain exclusive ownership of all proprietary rights in or to the Product, including all Canadian and international intellectual property and other rights such as patents, trademarks, copyrights and trade secrets. This is a license agreement and not an agreement for sale. You shall have no right or interest in any portion of the Product except the right to use the Product as set forth in these Terms of Use and, if you are a Tirade Inc.  Client, your License Agreement. You acknowledge that the Software, Database, Content, Information, Passcode Protected Product, Non-Passcode Protected Product, and Product constitute the valuable property and confidential copyrighted information of Tirade Inc.  and its licensors (collectively, the “Proprietary Information”). You agree to (a) comply with all copyright, trademark, trade secret, patent, contract and other laws necessary to protect all rights in the Proprietary Information, (b) not challenge Tirade Inc.  and its licensor’s ownership of (or the validity or enforceability of their rights in and to) the Proprietary Information, and (c) not remove, conceal, obliterate or circumvent any copyright or other notice or license, use or copying technological measure or rights management information included in the Product. You shall be liable for any violation of the provisions of these Terms of Use and, if applicable, the License Agreement by your employees, Independent Contractors, affiliates and agents and for any unauthorized use of the Product by such persons.
NOTICE — U.S. Government Rights/Commercial Technical Data and Software Unpublished, Rights Reserved Under the Copyright Laws of the United States This Site contains commercial technical data and computer software that have been privately developed and are normally vended commercially under a license or lease agreement restricting their use, disclosure and reproduction. In accordance with FAR 12.211, 12.212, 27.405(b)(2) and 52.227-19 and DFARS 227.7202, 227.7102 and 252.227-7015, as well as other applicable supplemental agency regulations, use, reproduction, disclosure and dissemination of this commercial technical data and computer software are governed strictly in accordance with Tirade Inc.  commercial license agreements, including these Terms of Use.
Export Restrictions.
This Site is controlled and operated by Tirade Inc.  from its offices within the United States. Tirade Inc.  makes no representation that any portion of the Product or other material accessed through this Site is appropriate or available for use in other locations, and access to them from other countries where their contents are illegal is prohibited. Those who choose to access the Product or the Site from other locations do so on their own volition and are responsible for compliance with applicable local laws. You may not export or re-export any portion of the Product except in full compliance with all applicable laws and regulations, this Terms of Use, and, if applicable, the associated License Agreement. In particular, no portion of the Product may be exported or re-exported in violation of the sanctions, export control laws and regulations of any applicable country, or exported or re-exported into (or to a national or resident of) any country to which the United States embargoes goods, or to anyone on the U.S. Treasury Department’s list of Specially Designated Nationals or the U.S. Commerce Department’s Table of Denial Orders.
Sweepstakes
From time to time, Tirade Inc.  conducts sweepstakes that entitle the winners to prizes. Each sweepstakes has its own terms and conditions, set forth in the “official rules” for that sweepstakes.
Jurisdiction
Tirade Inc.  is headquartered in the District of Columbia of the United States. These Terms of Use and your use of this Product shall be governed by the laws of the District of Columbia without regard to its conflicts of laws principles. Unless otherwise agreed to in writing, the federal and state courts located in the District of Columbia shall be the exclusive jurisdiction for any action brought against Tirade Inc. in connection with these Terms of Use or use of the Product. You irrevocably consent to the jurisdiction and venue of the federal and state courts located in the District of Columbia, and to the jurisdiction of the federal, provincial and/or state courts located in any State or Province where you are located, for any action brought against you in connection with these Terms of Use or use of the Product. All disputes arising outside of the United States shall be settled by arbitration held in London, England and in accordance with the Rules of Arbitration and Conciliation of the International Chamber of Commerce. All arbitrators shall be fluent in English and all documents submitted in connection with the arbitration shall be in English. Judgment upon an arbitration award may be entered in any court having jurisdiction, or application may be made to such court for a judicial acceptance of the award and an order of enforcement. If any material in this Product, or your use of the Product, is contrary to the laws of the place where you are when you access it, or if Tirade Inc.  is not Licensed as required by applicable laws or regulations in such locale, the Product is not intended for you, and we ask you not to use the Product. You are responsible for informing yourself of, and complying with, the laws of your jurisdiction.
Modifications to Product; Changes to these Terms
Tirade Inc. is continuously updating and changing the Product, and reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Product (or any part thereof) with or without notice. You agree that Tirade Inc. shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Product (or any part thereof). Additionally, we reserve the right, in our complete and sole discretion, to change these Terms of Use at any time by posting revised terms on the Product. It is your responsibility to check periodically for any changes we may make to the Product and these Terms of Use. Your continued use of this Product following the posting of changes to these terms or other policies means you accept the changes.
Termination
The restrictions imposed on you with respect to information or materials viewed and/or downloaded in respect of the Product and the disclaimers and the limitations of liabilities set forth above shall survive termination or suspension of these Terms of Use for any reason.
Miscellaneous
These Terms of Use contain the entire understanding of the parties with respect to the Product and supersede any prior oral or written statements and documents with respect to such subject matter, provided that these Terms of Use do not supersede any written License Agreement between the parties. Your obligations hereunder are binding on your successors, legal representatives and assigns. You may not assign or transfer (by operation of law or otherwise) these Terms of Use or any portion hereunder, in whole or in part, without the prior written consent of Tirade Inc. In the event any portion of these Terms of Use not being of a fundamental nature is held to be invalid, illegal or unenforceable, such part shall be deemed severed from these Terms of Use without invalidating the remaining provisions of these Terms of Use or affecting the enforceability of such remaining provisions. If a provision is held to be invalid, illegal or otherwise unenforceable, it shall be deemed to be replaced with an enforceable provision that retains the intent and benefits of the original provision. Any consent by Tirade Inc.  to, or waiver of, a breach of a right, term or provision of these Terms of Use, whether express or implied, shall not constitute a consent to, or waiver of, any other, different or subsequent breach. Headings are for reference only. The use of and access to the Product is available only to individuals who can enter into legally binding contracts under applicable law. All notices to Tirade Inc.  pertaining to these Terms of Use will be in writing, mailed by registered or certified mail, return receipt requested, or delivered by a well-recognized overnight United States or other international carrier, delivered to Tirade Inc., 13715 SW 139th Ct. Miami FL, 33186, attention: General Counsel.
Thank you for visiting tiradeapp.com Last revised Dec 01, 2018
"""

let PRIVACY = """
Last Updated: October 11, 2019
This Privacy Policy explains how we (Tirade) collect, use and share your information when you use our mobile application and other online products and services (we call all these the "Services") or when you otherwise interact with us.
Tirade App respects the privacy of its users and has developed this Privacy Policy to demonstrate its commitment to protecting your privacy. To keep the Services as anonymous as possible, We DO NOT collect personal information such as email address, first name, last name, phone number, address, state, province, zip/postal code, and city.

This Privacy Policy agreement (this “Privacy Policy”) constitutes an agreement between you (“You”) and the developers of this app, (“We” or “Us”) and governs your use of the Tirade application, as defined below. The Tirade application is referred to in this Privacy Policy and other agreements as “Tirade App” or “Licensed App” and all services and provided features as “Services”.

By downloading, installing, and using Tirade, you acknowledge that you have read, understood, and agree to be bound by and comply with this Privacy Policy. If you do not agree to the terms and conditions of this Privacy Policy, you are not entitled to use Tirade App.

These privacy policies (the "Privacy Policy") are intended to describe for you, as an individual who is a user of Tirade App or any other online services, the information we collect, how that information may be used, with whom it may be shared, and your choices about such uses and disclosures. By using Tirade App and its Services, you are accepting the practices described in this Privacy Policy.

HOW WE COLLECT AND USE INFORMATION

Information you provide us directly: We retain messages, photos, videos, invites, views, events, and reports you send. We use this information to operate, maintain, and provide to you the basic messaging features and functionality of the Services.

Server logs: Log file information is automatically reported by your mobile device each time you access the Services. When you use our Services, our servers automatically record certain log file information. These server logs may include anonymous information such as your web request, Internet Protocol address, device type, and other similar information.

Unique identifier: When you access the Services through a mobile device, we may access and store a unique identifier. Unique identifiers are randomly generated data structures stored on or associated with your mobile device, which uniquely identify your mobile device. A unique identifier may remain persistently on your device to help you log in to the Services.
  Use of certain service type information we collect about you: We use or may use log files and identifiers to: (a) remember information so that you will not have to re- enter it during your next session; (b) provide custom, personalized content and information; (c) to provide and monitor the effectiveness of our Services; (d) diagnose or fix technology problems; (e) help you efficiently access your information after you sign in; (f) to provide advertising to your device, and (g) automatically update the Tirade App on your mobile devices.

SHARING OF YOUR INFORMATION

We never share your personal data such as messages, photos or videos sent in private with third parties outside the Tirade App and its group companies (including any parent, subsidiaries and affiliates) without your consent. However, Tirade App may implement image content analysis for public pictures (such as profile pictures) to automatically filter objectionable content using third party services such as Google Vision API.

Who you may choose to share your information with: Any information or content that you voluntarily disclose for posting (messages, profile pictures, etc.) to the Services becomes available to other users.

What happens in the event of a change of control: We may buy or sell/divest/transfer the Tirade App, or any combination of its products, services, assets and/or businesses. Your user information related to the Services may be among the items sold or otherwise transferred in these types of transactions.

Instances where we are required to share your information: We will disclose your information where required to do so by law or subpoena or if we reasonably believe that such action is necessary to (a) comply with the law and the reasonable requests of law enforcement; (b) to enforce our Terms of Use or to protect the security or integrity of our Services ; and/or (c) to exercise or protect the rights, property, or personal safety of Tirade App, our Users or others.

HOW WE STORE AND PROTECT YOUR INFORMATION

Storage and Processing: Your information collected through the Services may be stored and processed in the United States or any other country in which Tirade App or its subsidiaries, affiliates or service providers maintain facilities. We may transfer information that we collect about you, to affiliated entities, or to other third parties across borders and from your country or jurisdiction to other countries or jurisdictions around the world. If you are located in the European Union or other regions with laws governing data collection and use that may differ from U.S. law, please note that we may transfer information, including personal information, to a country and jurisdiction that does not have the same data protection laws as your jurisdiction, and you consent to the transfer of information to the U.S. or any other country in which Tirade App or its parent, subsidiaries, affiliates or service providers maintain facilities and the use and disclosure of information about you as described in this Privacy Policy.

   Compromise of information: In the event that any information under our control is compromised as a result of a breach of security, we will take reasonable steps to investigate the situation and where appropriate, notify those individuals whose information may have been compromised and take other steps, in accordance with any applicable laws and regulations.

EXTERNAL LINKS AND SERVICES

We are not responsible for the practices employed by websites or services linked to or from the Services, including the information or content contained therein. Please remember that when you use a link to go from the Services to another website, our Privacy Policy does not apply to third-party websites or services. Your browsing and interaction on any third-party website or service, including those that have a link or advertisement on our website, are subject to that third party’s own rules and policies. This Privacy Policy does not apply to information we collect by other means (including offline) or from other sources other than through the Services.

CHANGES TO OUR PRIVACY POLICY

We may modify or update this Privacy Policy from time to time to reflect the changes in our business and practices, and so you should review this page periodically. When we change the policy in a material manner we will let you know and update the last modified date at the bottom of the file. For more information, you can contact us through the in-app support system.
"""

var AGREEDISAGREEDEC = """
I certify that I'm atleast 18 years old.
WARNINGS STORONG CONTENT AND LANGUAGE AHEAD
This ride is now open. You are to say whatevere you want
we humbly ask that you to respect people's race, nation
ality, gender, age, religion, and political status.

Anything said or displayed is not be taken seriously.
If you are offended by the profanity,strong opnions, and
insulting language, please do not enter Tirade.


"""

