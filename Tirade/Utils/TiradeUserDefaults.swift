//
//  UserDefaults.swift
//  Tirade
//
//  Created by admin on 04/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeUserDefaults: NSObject {
    static let defaults = UserDefaults.standard
    
    static func setIncognitCredential(key :  String)  {
        TiradeUserDefaults.defaults.set(true, forKey: key)
    }
    
    static func getIncognitoCredential(key :  String) -> Bool  {
        if TiradeUserDefaults.defaults.value(forKey: key) == nil {
            return false
        }
        return TiradeUserDefaults.defaults.value(forKey: key) as! Bool
    }
    
    
    static func setPublicCredential(key :  String)  {
        TiradeUserDefaults.defaults.set(true, forKey: key)
    }
    
    static func getPublicCredential(key :  String) -> Bool  {
        if TiradeUserDefaults.defaults.value(forKey: key) == nil {
            return false
        }
        return TiradeUserDefaults.defaults.value(forKey: key) as! Bool
    }
    
    static func setUser_ID(value :  String)  {
        TiradeUserDefaults.defaults.set(value, forKey: "user_id")
    }
    
    static func getUser_ID(key :  String) -> String  {
        if TiradeUserDefaults.defaults.value(forKey: key) == nil {
            return "noid"
        }
        return TiradeUserDefaults.defaults.value(forKey: key) as! String
    }
    
    static func setTiradeUser_ID(value :  String)  {
           TiradeUserDefaults.defaults.set(value, forKey: "userTirade_id")
       }
       
    static func getTiradeUser_ID(key :  String) -> String  {
           if TiradeUserDefaults.defaults.value(forKey: key) == nil {
               return "noid"
           }
           return TiradeUserDefaults.defaults.value(forKey: key) as! String
       }
    
    static func setUser_Name(value :  String)  {
        TiradeUserDefaults.defaults.set(value, forKey: "user_Name")
    }
    
    static func getUser_Name(key :  String) -> String  {
        if TiradeUserDefaults.defaults.value(forKey: key) == nil {
            return ""
        }
        return TiradeUserDefaults.defaults.value(forKey: key) as! String
    }
    
    static func setUser_Image(value :  String)  {
        TiradeUserDefaults.defaults.set(value, forKey: "user_Image")
    }
    
    static func getUser_Image(key :  String) -> String  {
        if TiradeUserDefaults.defaults.value(forKey: key) == nil {
            return ""
        }
        return TiradeUserDefaults.defaults.value(forKey: key) as! String
    }
    
    static func setTiradeUser_Name(value :  String)  {
          TiradeUserDefaults.defaults.set(value, forKey: "userTirade_Name")
      }
      
      static func getTiradeUser_Name(key :  String) -> String  {
          if TiradeUserDefaults.defaults.value(forKey: key) == nil {
              return ""
          }
          return TiradeUserDefaults.defaults.value(forKey: key) as! String
      }
      
      static func setTiradeUser_Image(value :  String)  {
          TiradeUserDefaults.defaults.set(value, forKey: "userTirade_Image")
      }
      
      static func getTiradeUser_Image(key :  String) -> String  {
          if TiradeUserDefaults.defaults.value(forKey: key) == nil {
              return ""
          }
          return TiradeUserDefaults.defaults.value(forKey: key) as! String
      }
}
