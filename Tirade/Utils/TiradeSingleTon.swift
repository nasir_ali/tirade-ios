//
//  TiradeSingleTon.swift
//  Tirade
//
//  Created by admin on 04/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeSingleTon {

    // MARK: - Properties

    static let shared = TiradeSingleTon()

    // MARK: -

    var tiradeProfileUpdation = TiradeProfileUpdate()
    
    var incognitoUserName = String()
    var incognitoUserImage = UIImage(named: "user")
    
    
    var isLoggedINUserIncognito = false
    var isLoggedINUserPublic = false
    var isIncognitoUserSelection = false
    
    var arrComments = [TrashHome]()
    var arrVideoComment = [IncognitoForum]()

    // Initialization

    private init() {
    }
    

  


}
