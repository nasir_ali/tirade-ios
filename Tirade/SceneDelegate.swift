//
//  SceneDelegate.swift
//  Tirade
//
//  Created by videotap ios on 12/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import IQKeyboardManager

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    //@available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
      //  let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IncognitoFormVC") as! IncognitoFormVC

      // let loginStoryBoard = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeHomeVC") as! TiradeHomeVC

//        let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GoSelectionVC") as! GoSelectionVC
//
//        let navigationController = UINavigationController(rootViewController: loginStoryBoard)
//        self.window?.rootViewController = navigationController
        
        
        
        
//        let homeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "IncognitoHomeVC") as! IncognitoHomeVC
//        let naviController = UINavigationController(rootViewController: homeViewController)
//        self.window?.rootViewController = naviController
        
        
        
        
  //for production
        
        let valuePub = TiradeUserDefaults.getPublicCredential(key : "PublicLoggedInUser")
        let valueInco = TiradeUserDefaults.getIncognitoCredential(key: "incognitoLoggedIn")

        if valuePub || valueInco {
            let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
            let navigationController = UINavigationController(rootViewController: loginStoryBoard)
                   self.window?.rootViewController = navigationController
        }else {
             let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GoSelectionVC") as! GoSelectionVC
                let navigationController = UINavigationController(rootViewController: loginStoryBoard)
                       self.window?.rootViewController = navigationController
        }
        
        
        
        
        IQKeyboardManager.shared().isEnabled = true

        guard let _ = (scene as? UIWindowScene) else { return }
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

