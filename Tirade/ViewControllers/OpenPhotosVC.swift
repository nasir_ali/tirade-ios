//
//  OpenPhotosVC.swift
//  Tirade
//
//  Created by admin on 14/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class OpenPhotosVC: TiradeBaseVC {

    @IBOutlet var imgOpen: UIImageView!
    var imageUrl = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        var url : NSString = imageUrl as NSString
             var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString

        imgOpen.kf.setImage(with: URL(string: urlStr as String)!)

    }
    
    @IBAction func btnDownPressed(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
       }
}
