//
//  PlayAudioVC.swift
//  Tirade
//
//  Created by admin on 14/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class PlayAudioVC: TiradeBaseVC {

    @IBOutlet var playButton: UIButton!
    @IBOutlet var imgGif: UIImageView!
    var playUrl = String()
    var audioPlayer: AVAudioPlayer!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.imgGif.image = UIImage.gif(name: "VoiceRecord")

    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
           if audioPlayer == nil {
               startPlayback()
           } else {
               finishPlayback()
           }
       }
    
    
     @IBAction func btnDownPressed(_ sender: UIButton) {
        finishPlayback()
        self.dismiss(animated: true, completion: nil)
     }
    
    // MARK: - Playback
    
    func startPlayback() {
        //var player = AVPlayer()
        var url : NSString = playUrl as NSString
               var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
               var searchURL : NSURL = NSURL(string: urlStr as String)!
        
//        let playerItem = AVPlayerItem(url: searchURL as URL)
//               player = AVPlayer(playerItem: playerItem)
//               player.play()
//        do {
//            var dataFromurl = try? Data(contentsOf: searchURL as URL)
//            if dataFromurl != nil {
//                audioPlayer = try AVAudioPlayer(data: dataFromurl!)
//                audioPlayer.delegate = self
//                audioPlayer.prepareToPlay()
//                audioPlayer.play()
//                playButton.setTitle("Stop", for: .normal)
//            }
//        } catch {
//           // playButton.isHidden = true
//            // unable to play recording!
//        }
      
        do {
                  //keep alive audio at background
                  try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
              } catch _ {
              }
              do {
                  try AVAudioSession.sharedInstance().setActive(true)
              } catch _ {
              }
              UIApplication.shared.beginReceivingRemoteControlEvents()
       // let audioFilename = getDocumentsDirectory().appendingPathComponent("\(fileNameAudio).m4a")
        do {
            let dataFromurl = try Data(contentsOf: searchURL as URL)

            audioPlayer = try AVAudioPlayer(data: dataFromurl)
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.play()
            playButton.setTitle("Stop", for: .normal)
        } catch let error {
            print(error.localizedDescription)
            //playButton.isHidden = true
            // unable to play recording!
        }
        
        
        
    }
    
  
    
    
    func finishPlayback() {
          audioPlayer = nil
          playButton.setTitle("Play", for: .normal)
      }
}

extension PlayAudioVC: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        finishPlayback()
    }
    
}
