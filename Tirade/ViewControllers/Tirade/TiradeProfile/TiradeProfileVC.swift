//
//  TiradeProfileVC.swift
//  Tirade
//
//  Created by admin on 01/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import JJFloatingActionButton

class TiradeProfileVC: TiradeBaseVC {
    
    
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMemberID: UILabel!
    @IBOutlet weak var lblUserName: UILabel!

    @IBOutlet weak var tblProfile : UITableView!
    var actionButton = JJFloatingActionButton()
    @IBOutlet weak var btnFloating: UIButton!
    var dataProfile : TiradeProfile?
    var txtFieldUpdatedValue = String()
    var txtFieldUpdatedKey = String()
    var fileInfoType = [FileTypeInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.imgMember.layer.cornerRadius = self.imgMember.frame.size.width/2
        let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getTiradeUser_Image(key: "userTirade_Image"), options: .ignoreUnknownCharacters)!
             if  dataDecoded.count > 0 {
                 let imageFromData: UIImage = UIImage(data:dataDecoded)!
                 self.imgMember.image = imageFromData
             }else {
                 self.imgMember.image = UIImage(named: "user")
             }
        self.lblMemberID.text = TiradeUserDefaults.getTiradeUser_Name(key: "userTirade_Name")
        lblUserName.text = self.lblMemberID.text
        self.navigationController?.isNavigationBarHidden = true
        self.makeUrlRequest()
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)

    }
    
    @IBAction func drawerIconTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        let viewController22 = LeftSideViewController(nibName: "LeftSideViewController", bundle: nil)
//        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: viewController22)
//        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
//        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func btnFloatingAction(_ sender: UIButton) {
   
    sender.isSelected = !sender.isSelected
           if sender.isSelected {
               actionButton.tintColor = UIColor.white
               setFloatingMenu(actionButton: actionButton)
               actionButton.isHidden = false
           }else {
               actionButton.items.removeAll()
               actionButton.removeFromSuperview()
               actionButton.isHidden = true
           }
    
    }
    
    @IBAction func btnBackction(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    
    func setFloatingMenu(actionButton : JJFloatingActionButton)  {
        actionButton.addItem(title: "", image: UIImage(named: "homeHut")?.withRenderingMode(.alwaysOriginal)) { item in
            self.navigationController?.popViewController(animated: false)
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: UIImage(named: "notification")?.withRenderingMode(.alwaysOriginal)) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: UIImage(named: "goLiveIcon")?.withRenderingMode(.alwaysOriginal)) { item in
            // do something
        }
        
        
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.itemSizeRatio = CGFloat(1)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .trailing
            if item.imageView.image == nil {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .clear
                item.buttonColor = .clear
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }else {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .white
                item.buttonColor = .white
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }
        }
        
        
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        //  actionButton.bottomAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        //actionButton.trailingAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        
        actionButton.handleSingleActionDirectly = true
        actionButton.buttonDiameter = btnFloating.frame.size.width
        
        actionButton.overlayView.backgroundColor = UIColor(white: 1, alpha: 0.1)
        actionButton.buttonImageSize = CGSize(width: 30, height: 30)
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 180)
        
        //        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "X")!)
        //       actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        
        //       actionButton.layer.shadowColor = UIColor.black.cgColor
        //       actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        //       actionButton.layer.shadowOpacity = Float(0.4)
        //       actionButton.layer.shadowRadius = CGFloat(2)
        
    }
    
    @IBAction func btnUserAction(_ sender: UIButton) {
        
    }
    @IBAction func btndigitalcampaignAction(_ sender: UIButton) {
        
    }
    @IBAction func btnDartsAction(_ sender: UIButton) {
        
    }
    @IBAction func btnCameralensAction(_ sender: UIButton) {
        AttachmentHandler.shared.showImageLibraryList(vc: self)
    }
    
        @objc func getFileData(notification : NSNotification)  {
            guard let dictFile  = notification.userInfo
                else {
                    return
            }
        
            
        //    if fileInfoType.count < 2 {
                let fileData = FileTypeInfo()
                fileData.fileData = dictFile["fileData"] as! Data
                fileData.fileName = dictFile["fileName"] as! String
            if dictFile["uploadFileKey"] as! String == "post_video" {
                fileData.uploadFileKey = "forumVideo"

            }else if dictFile["uploadFileKey"] as! String == "post_image" {
                fileData.uploadFileKey = "fourmImage"

            }else {
                fileData.uploadFileKey = "forumNotes"

            }
                fileData.mimeType = dictFile["mimeType"] as! String
                fileInfoType.append(fileData)
    //        }else {
    //          let fileData = FileTypeInfo()
    //            fileData.fileData = dictFile["fileData"] as! Data
    //            fileData.fileName = dictFile["fileName"] as! String
    //            fileData.uploadFileKey = "forumNotes"
    //            fileData.mimeType = dictFile["mimeType"] as! String
    //            fileInfoType.append(fileData)
     //       }
            
           
          
        }
    
    func makeUrlRequest()  {
        let url = BASEURL + TIRADEPROFILEGET
         let dataDict =
        [ "user_id" : "132"]//TiradeUserDefaults.getTiradeUser_ID(key: "user_id")]
        let fileInfoType = [FileTypeInfo]()

        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: fileInfoType, parameters: dataDict, onCompletion: { (responseData) in
            do {
                let dictProfile : TiradeProfileDict = try! JSONDecoder().decode(TiradeProfileDict.self, from: responseData!)
                self.dataProfile = dictProfile.getProfileData![0]
                self.tblProfile.delegate = self
                self.tblProfile.dataSource = self
                self.tblProfile.reloadData()
            }catch  {
                return
            }

        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)

    }
    
    func sendDataToServer()  {
        let updateUser = TiradeSingleTon.shared.tiradeProfileUpdation
        let dataDict = [ "user_id" : updateUser.user_id,
         "s1_userName" : updateUser.s1_userName,
           "s1_userGender" : updateUser.s1_userGender,
           "s1_userBirthdate" : updateUser.s1_userBirthdate,
           "s2_userName" : updateUser.s2_userName,
           "s3_education":updateUser.s3_education,
           "s3_born" : updateUser.s3_born,
           "s3_live" : updateUser.s3_live,
           "s3_work" : updateUser.s3_work,
           "s3_liveIn" : updateUser.s3_liveIn,
           "s3_status" : updateUser.s3_status,
           "s5_team" : updateUser.s5_team,
           "s5_music" : updateUser.s5_music,
           "s5_band" : updateUser.s5_band,
           "s5_hobbies" : updateUser.s5_hobbies,
           "s5_heros":updateUser.s5_heros,
           "s5_dreamjob" : updateUser.s5_dreamjob,
           "s5_interested" : updateUser.s5_interested,
           "s5_animalLover":updateUser.s5_animalLover,
           "s5_beliveInlife":updateUser.s5_beliveInlife,
           "s5_favouriteColor":updateUser.s5_favouriteColor,
           "s5_movies" : updateUser.s5_movies,
           "s5_dreamCar" : updateUser.s5_dreamCar,
           "s5_dreamVacation" : updateUser.s5_dreamVacation,
           "s5_earning" : updateUser.s5_earning,
           "s5_food" : updateUser.s5_food,
           "s5_sadOrHappy" : updateUser.s5_sadOrHappy,
           "s5_makesYouHappy" : updateUser.s5_makesYouHappy,
           "s5_makesYouAngry" : updateUser.s5_makesYouAngry] as [String : AnyObject]
        sendActionDataToServer(dataDict: dataDict as [String : AnyObject])

    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + TIRADEPROFILEPOST
        // let home = IncognitoHomeVC()
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: fileInfoType, parameters: dataDict, onCompletion: { (data) in
            do {
                guard let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject] else {
                    return
                }
                DispatchQueue.main.async {
                    DataUtils.removeLoader()
                }
                if let dictFromJSON = jsonResult as? [String:AnyObject] {
                    let locData = dictFromJSON["message"] as? String
                    let status = dictFromJSON["status"] as? Int
                    let userID = dictFromJSON["user"] as? [String:AnyObject]
                    
                    if locData != nil {
                        DataUtils.showAlert(title: "", msg: locData!, selfObj: self) {
                            if status == 0 {
                                
                            }else {

                            }
                        }
                    }
                }
            }catch  {
                
                return
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
        }
    }
    
extension TiradeProfileVC : UITableViewDelegate,UITableViewDataSource  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 130
        case 1:
            return 130
        case 2:
            return 300
        case 3:
            return 250
        case 4:
            return 300
        default:
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? AddPeopleCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        // tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TiradeVisionBoardTblCell else { return }
        
        // storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.row {
        case 0:
            let tblCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileCell", for: indexPath) as! EditProfileCell
            tblCell.lblUserName.text = dataProfile?.editProfile![0].s1_userName
            tblCell.lblGender.text = dataProfile?.editProfile![0].s1_userGender
            tblCell.lblBirthday.text = dataProfile?.editProfile![0].s1_userBirthdate
            if dataProfile?.editProfile![0].s1_userImage != "" {
                tblCell.btnProfileEdit.setImage(UIImage(named: dataProfile?.editProfile![0].s1_userImage ?? "user"), for: .normal)
            }
            tblCell.btnProfileEdit.addTarget(self, action: #selector(saveProfileImage), for: .touchUpInside)
            tblCell.btnSaveAction.addTarget(self, action: #selector(saveBtnAction), for: .touchUpInside)
            return tblCell
        case 1:
            let tblCell = tblProfile.dequeueReusableCell(withIdentifier: "AddPeopleCell", for: indexPath) as! AddPeopleCell
            tblCell.lblMainTitle.text = "Add People You Know"

            return tblCell
        case 2:
            let tblCell = tblProfile.dequeueReusableCell(withIdentifier: "BiographyCell", for: indexPath)
                as! BiographyCell
            tblCell.imgBack.backgroundColor = DataUtils.hexStringToUIColor(hex: "6FBE40")
            tblCell.lblMainTitle.text = "Biography"
            tblCell.datBioGraphy = dataProfile!.Biography!

            return tblCell
        case 3:
            let tblCell = tblProfile.dequeueReusableCell(withIdentifier: "AddPeopleCell", for: indexPath) as! AddPeopleCell
            tblCell.lblMainTitle.text = "Favourite Images"

            return tblCell
        case 4:
            let tblCell = tblProfile.dequeueReusableCell(withIdentifier: "BiographyCell", for: indexPath) as! BiographyCell
            tblCell.imgBack.backgroundColor = DataUtils.hexStringToUIColor(hex: "59BCD0")
            tblCell.lblMainTitle.text = "Hobbies"
            tblCell.datHobbies = dataProfile!.Hobbies!
            return tblCell
        default:
            let tblCell = tblProfile.dequeueReusableCell(withIdentifier: "BiographyCell", for: indexPath) as! BiographyCell
            return tblCell
            
        }        
    }
    
    @objc func saveProfileImage()  {
        AttachmentHandler.shared.showImageLibraryList(vc: self)
    }
    @objc func saveBtnAction()  {
       sendDataToServer()
    }
}

extension TiradeProfileVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 3 {
            return dataProfile?.favouriteImg?.count ?? 0
        }
        return dataProfile?.addPeople?.count ?? 0

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(collectionView.tag)
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPeopleCollCell", for: indexPath) as! AddPeopleCollCell
        if collectionView.tag == 3 {
            collectionCell.lblTitle.isHidden = true
            if dataProfile?.favouriteImg![0].s4_imgUrl != "" {
                collectionCell.btnCircle.setImage(UIImage(named: dataProfile?.favouriteImg![indexPath.item].s4_imgUrl ?? "user"), for: .normal)
            }
        }else {
            if dataProfile?.addPeople![0].s2_userImage != "" {
                collectionCell.btnCircle.setImage(UIImage(named: dataProfile?.addPeople![indexPath.item].s2_userImage ?? "user"), for: .normal)
            }
            collectionCell.lblTitle.text = dataProfile?.addPeople![indexPath.item].s2_userName
            collectionCell.lblTitle.isHidden = false
        }
        collectionCell.btnCircle.layer.cornerRadius =  20
        collectionCell.layoutIfNeeded()

        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 3 {
            return CGSize(width: 70, height: 100)

        }
        return CGSize(width: 70, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}


class TiradeProfileUpdate {
    var user_id : String? = "132"
    var s1_userName : String? = ""
    var s1_userGender : String? = ""
    var s1_userBirthdate : String? = ""
    var s2_userName : String? = ""
    var s3_education : String? = ""
    var s3_born : String? = ""
    var s3_live : String? = ""
    var s3_work : String? = ""
    var s3_liveIn : String? = ""
    var s3_status : String? = ""
    var s5_team : String? = ""
    var s5_music : String? = ""
    var s5_band : String? = ""
    var s5_hobbies : String? = ""
    var s5_heros : String? = ""
    var s5_dreamjob : String? = ""
    var s5_interested : String? = ""
    var s5_animalLover : String? = ""
    var s5_beliveInlife : String? = ""
    var s5_favouriteColor : String? = ""
    var s5_movies : String? = ""
    var s5_dreamCar : String? = ""
    var s5_dreamVacation : String? = ""
    var s5_earning : String? = ""
    var s5_food : String? = ""
    var s5_sadOrHappy : String? = ""
    var s5_makesYouHappy : String? = ""
    var s5_makesYouAngry : String? = ""
}
