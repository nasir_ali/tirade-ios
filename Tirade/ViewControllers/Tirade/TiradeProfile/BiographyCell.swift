//
//  BiographyCell.swift
//  Tirade
//
//  Created by admin on 01/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class BiographyCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
  
    
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var lblMainTitle : UILabel!
    @IBOutlet weak var imgBack : UIImageView!
    @IBOutlet weak var btnSave : UIButton!
    var datBioGraphy = [Biography]()
    var datHobbies = [Hobbies]()
    var txtFieldUpdatedValue = String()
    var txtFieldUpdatedKey = String()
    var arrPlaceBio = ["Education","Where are you born","Where do you live","Where do you work?","City that you live in","Relationship status"]
    var arrPlaceHobi = ["Favorite sports team(s)?","Favorite music?","Favorite singer/rapper/band?","What are your hobbies","Who is your hero(s)?","What is your dream job?","Are you an animal lover?","Do you believe in life beyond","Favorite Color(s)","Favorite movies?","What is your dream car?","What is your dream vacation?","Are you interested in earnings?","What is your Favorite food?","Are you happy or sad?","What makes you happy?","What makes you Angry?"]



    @IBOutlet weak var tblBiography : UITableView!

    override func awakeFromNib() {
        super.awakeFromNib()
        tblBiography.delegate = self
        tblBiography.dataSource = self
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lblMainTitle.text == "Hobbies" {
            return arrPlaceHobi.count

        }else {
            
        }
        return arrPlaceBio.count
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 50
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let tblCell = tblBiography.dequeueReusableCell(withIdentifier: "BiographyInsideTblCell", for: indexPath) as! BiographyInsideTblCell
            
            if lblMainTitle.text == "Hobbies" {
                tblCell.txtTitle.placeholder = arrPlaceHobi[indexPath.row]
                tblCell.txtTitle.delegate = self
                switch indexPath.row {
                case 0:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_team
                    tblCell.txtTitle.accessibilityValue = "s5_team"
                case 1:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_music
                    tblCell.txtTitle.accessibilityValue = "s5_music"
                case 2:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_band
                    tblCell.txtTitle.accessibilityValue = "s5_band"
                case 3:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_hobbies
                    tblCell.txtTitle.accessibilityValue = "s5_hobbies"
                case 4:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_heros
                    tblCell.txtTitle.accessibilityValue = "s5_heros"
                case 5:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_dreamjob
                    tblCell.txtTitle.accessibilityValue = "s5_dreamjob"
                case 6:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_interested
                    tblCell.txtTitle.accessibilityValue = "s5_interested"
                case 7:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_animalLover
                    tblCell.txtTitle.accessibilityValue = "s5_animalLover"
                case 8:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_beliveInlife
                    tblCell.txtTitle.accessibilityValue = "s5_beliveInlife"
                case 9:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_favouriteColor
                    tblCell.txtTitle.accessibilityValue = "s5_favouriteColor"
                case 10:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_movies
                    tblCell.txtTitle.accessibilityValue = "s5_movies"

                case 11:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_dreamCar
                    tblCell.txtTitle.accessibilityValue = "s5_dreamCar"

                case 12:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_dreamVacation
                    tblCell.txtTitle.accessibilityValue = "s5_dreamVacation"

                case 13:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_earning
                    tblCell.txtTitle.accessibilityValue = "s5_earning"

                case 14:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_food
                    tblCell.txtTitle.accessibilityValue = "s5_food"

                case 15:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_sadOrHappy
                    tblCell.txtTitle.accessibilityValue = "s5_sadOrHappy"

                case 16:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_makesYouHappy
                    tblCell.txtTitle.accessibilityValue = "s5_makesYouHappy"

                case 17:
                    tblCell.txtTitle.text = datHobbies[indexPath.row].s5_makesYouAngry
                    tblCell.txtTitle.accessibilityValue = "s5_makesYouAngry"

                default:
                    tblCell.txtTitle.text = ""
                }
            }else  {
                tblCell.txtTitle.placeholder = arrPlaceBio[indexPath.row]

                
                switch indexPath.row {
                case 0:
                    tblCell.txtTitle.text = datBioGraphy[indexPath.row].s3_education
                    tblCell.txtTitle.accessibilityValue = "s3_education"

                    case 1:
                    tblCell.txtTitle.text = datBioGraphy[indexPath.row].s3_born
                    tblCell.txtTitle.accessibilityValue = "s3_born"

                    case 2:
                    tblCell.txtTitle.text = datBioGraphy[indexPath.row].s3_live
                    tblCell.txtTitle.accessibilityValue = "s3_live"

                    case 3:
                    tblCell.txtTitle.text = datBioGraphy[indexPath.row].s3_work
                    tblCell.txtTitle.accessibilityValue = "s3_work"

                    case 4:
                    tblCell.txtTitle.text = datBioGraphy[indexPath.row].s3_liveIn
                    tblCell.txtTitle.accessibilityValue = "s3_liveIn"

                    case 5:
                    tblCell.txtTitle.text = datBioGraphy[indexPath.row].s3_status
                    tblCell.txtTitle.accessibilityValue = "s3_status"

                    default:
                    tblCell.txtTitle.text = ""
                }
            }
            tblCell.btnSave.addTarget(self, action: #selector(saveBtnAction), for: .touchUpInside)

            //tblCell.txtTitle.accessibilityValue = ""
            // tblCell.collectionVision.tag = indexPath.section
            
            //tableView.dequeueReusableCell(withIdentifier: "TiradeVisionBoardTblCell") as! TiradeVisionBoardTblCell
            return tblCell
            
        }
    
    @objc func saveBtnAction()  {
        self.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.accessibilityValue {
        case "s5_team":
            TiradeSingleTon.shared.tiradeProfileUpdation.s5_team = textField.text
        default:
            TiradeSingleTon.shared.tiradeProfileUpdation.s5_team =  textField.text
        }
//        var user_id : String? = "132"
//          var s1_userName : String?
//          var s1_userGender : String?
//          var s1_userBirthdate : String?
//          var s2_userName : String?
//          var s3_education : String?
//          var s3_born : String?
//          var s3_live : String?
//          var s3_work : String?
//          var s3_liveIn : String?
//          var s3_status : String?
//          var s5_team : String?
//          var s5_music : String?
//          var s5_band : String?
//          var s5_hobbies : String?
//          var s5_heros : String?
//          var s5_dreamjob : String?
//          var s5_interested : String?
//          var s5_animalLover : String?
//          var s5_beliveInlife : String?
//          var s5_favouriteColor : String?
//          var s5_movies : String?
//          var s5_dreamCar : String?
//          var s5_dreamVacation : String?
//          var s5_earning : String?
//          var s5_food : String?
//          var s5_sadOrHappy : String?
//          var s5_makesYouHappy : String?
//          var s5_makesYouAngry : String?
//       TiradeSingleTon.shared.tiradeProfileUpdation.s5_team = textField.text

    }

}
