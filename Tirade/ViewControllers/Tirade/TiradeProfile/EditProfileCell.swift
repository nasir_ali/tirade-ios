//
//  EditProfileCell.swift
//  Tirade
//
//  Created by admin on 01/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class EditProfileCell: UITableViewCell {

    
    @IBOutlet weak var btnProfileEdit : UIButton!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lblBirthday : UILabel!
    @IBOutlet weak var btnSaveAction : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
