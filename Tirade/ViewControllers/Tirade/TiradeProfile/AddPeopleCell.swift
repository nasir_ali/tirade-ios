//
//  AddPeopleCell.swift
//  Tirade
//
//  Created by admin on 01/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class AddPeopleCell: UITableViewCell {

    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var lblMainTitle : UILabel!
    @IBOutlet weak var collectionProfile : UICollectionView!
    @IBOutlet weak var imgBack : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

             collectionProfile.delegate = dataSourceDelegate
             collectionProfile.dataSource = dataSourceDelegate
             collectionProfile.tag = row
             collectionProfile.setContentOffset(collectionProfile.contentOffset, animated:false) // Stops collection view if it was scrolling.
             collectionProfile.reloadData()
         }

         var collectionViewOffset: CGFloat {
             set { collectionProfile.contentOffset.x = newValue }
             get { return collectionProfile.contentOffset.x }
         }

}
