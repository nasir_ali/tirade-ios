//
//  TiradeHomeCollCell.swift
//  Tirade
//
//  Created by admin on 24/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeHomeCollCell: UICollectionViewCell {
    @IBOutlet weak var vwBase: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    override func layoutSubviews() {
          super.layoutSubviews()
          DataUtils.applyRadiusMaskFor(view: vwBase, topLeftRadius: 30, topRightRadius: 0, bottomLeftRadius: 0, bottomRightRadius: 30)
      }
}
