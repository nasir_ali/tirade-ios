//
//  TiradeTblCell.swift
//  Tirade
//
//  Created by admin on 24/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeTblCell: UITableViewCell {

    @IBOutlet weak var vwBaseCell: UIView!
    @IBOutlet weak var vwBasePlusIcons: UIView!
    @IBOutlet weak var constraintHeightThreeDotVw: NSLayoutConstraint!
    @IBOutlet weak var btnThreeDots: UIButton!
    @IBOutlet weak var vwOnThreeDots: UIView!
    @IBOutlet weak var imgTiradeProfile : UIImageView!
    
    
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblLikeCount : UILabel!
    @IBOutlet weak var lblCommentCount : UILabel!
    @IBOutlet weak var btnLike : UIButton!
    @IBOutlet weak var btnComment : UIButton!
    @IBOutlet weak var btnShare : UIButton!
    @IBOutlet weak var constraintWidthvwImage: NSLayoutConstraint!
    @IBOutlet weak var vwImgVdo : UIView!
    @IBOutlet weak var imgUploaded : UIImageView!
    @IBOutlet weak var btnOpenImg : UIButton!
    
    @IBOutlet weak var bntFBAction: UIButton!
    @IBOutlet weak var bntInstaAction: UIButton!
    @IBOutlet weak var bntSnapChatAction: UIButton!
    @IBOutlet weak var bntTicTocAction: UIButton!
    @IBOutlet weak var bntTwtAction: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgTiradeProfile.layer.cornerRadius = imgTiradeProfile.frame.size.height/2
      //  DataUtils.roundCorners(cornerRadius: 40, selfObj: vwBaseCell, topLeft: UIRectCorner.topRight, topRight: UIRectCorner.bottomLeft, bottomLeft: UIRectCorner.bottomLeft, bottomRight: UIRectCorner.bottomLeft)
        vwOnThreeDots.backgroundColor = UIColor(white: 1, alpha: 0.4)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DataUtils.applyRadiusMaskFor(view: vwBaseCell, topLeftRadius: 20, topRightRadius: 40, bottomLeftRadius: 40, bottomRightRadius: 20)
        vwBaseCell.setNeedsLayout()
    }
}
