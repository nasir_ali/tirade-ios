//
//  PostVC.swift
//  Tirade
//
//  Created by admin on 27/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import IQKeyboardManager

class PostVC: TiradeBaseVC {

    @IBOutlet weak var txtVwPost : IQTextView!
    var fileInfoType = [FileTypeInfo]()
    @IBOutlet weak var btnUploadImg : UIButton!
    @IBOutlet weak var btnUploadVideo : UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        btnUploadImg.layer.cornerRadius = 10
        btnUploadImg.layer.borderWidth = 1
        btnUploadImg.layer.borderColor = UIColor.white.cgColor
        btnUploadVideo.layer.borderColor = UIColor.white.cgColor
        btnUploadVideo.layer.borderWidth = 1
        btnUploadVideo.layer.cornerRadius = 10

        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)
    }
    
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
        
        let fileData = FileTypeInfo()
        fileData.fileData = dictFile["fileData"] as! Data
        fileData.fileName = dictFile["fileName"] as! String
        fileData.uploadFileKey = dictFile["uploadFileKey"] as! String
        fileData.mimeType = dictFile["mimeType"] as! String
        fileInfoType.append(fileData)
    }
    
    @IBAction func uploadVideoBtnAction(_ sender: UIButton) {
        AttachmentHandler.shared.showVideoList(vc: self)

       }
    @IBAction func uploadImgBtnAction(_ sender: UIButton) {
        
        AttachmentHandler.shared.showImageLibraryList(vc: self)

       }
    
   @IBAction func crossBtnAction(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
    NotificationCenter.default.post(Notification(name: Notification.Name("dissmisspost")))

    }
    @IBAction func postBtnAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtVwPost.text != nil {
            let dataDict = ["user_id" :  TiradeUserDefaults.getTiradeUser_ID(key: "userTirade_id")
                ,"post_content":txtVwPost.text] as [String : Any]
            
            //   let dataDict = ["created_by_fk" : "","category":formData.selectedCategory,"post_content":formData.post,"post_type":formData.category,"post_image":formData.photosData,"post_imgname" : formData.imageName,"post_audioname" : "aa","post_videoname" : "vvvv"] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
            
        }else {
        }
       
    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + POSTTIRADHOMEDATA
        
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: fileInfoType, parameters: dataDict, onCompletion: { (data) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(Notification(name: Notification.Name("dissmisspost")))

                }
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
    }
}
