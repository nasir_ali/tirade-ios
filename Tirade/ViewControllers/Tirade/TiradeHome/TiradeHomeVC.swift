//
//  TiradeHomeVC.swift
//  Tirade
//
//  Created by admin on 24/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import IQKeyboardManager
import AVKit

class TiradeHomeVC:  TiradeBaseVC {
    
    @IBOutlet weak var collVwTirade: UICollectionView!
    
    @IBOutlet weak var tblTirade: UITableView!
    @IBOutlet weak var btnFloating: UIButton!
    @IBOutlet weak var btnBackVW: UIView!
    
    @IBOutlet weak var txtOnMyMind: IQTextView!
    @IBOutlet weak var imgBackOnMyMind: UIImageView!
    
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMemberID: UILabel!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    
    var actionButton = JJFloatingActionButton()
    var formData = TrashCategory()

    var trendingVds = [VideosData]()

    
    
    var collLbldata =  [INCOGNITOTRASH,INCOGNITODEBATE,INCOGNITOCALLOUT,INCOGNITOJOKES,INCOGNITOFORUMS,INCOGNITOSHAME]
    var collLblimg =  ["temp1","temp2","temp3","temp4","temp1","temp2","temp1","temp2","temp3","temp4","temp1","temp2"]
    
    var arrTiradeHome : [TrashHome]?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(dissmissPostAction),name: NSNotification.Name(rawValue: "dissmisspost"), object: nil)

//        let indexPath = IndexPath(item: 1, section: 0)
//        let scrollPosition = UITableView.ScrollPosition.top
//        tblTirade.scrollToRow(at: indexPath, at:scrollPosition, animated: false)
        
    }
    
    @IBAction func btnMad(_ sender: UIButton) {
          let loginStoryBoard = UIStoryboard(name: "MadSkills", bundle: nil).instantiateViewController(withIdentifier: "MadskillsIntroVC") as! MadskillsIntroVC
          self.navigationController?.pushViewController(loginStoryBoard, animated: true)
          
      }
      @IBAction func btnTrashTalk(_ sender: UIButton) {
          let value = TiradeUserDefaults.getIncognitoCredential(key: "incognitoLoggedIn")
          if value {
              let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IncognitoHomeVC") as! IncognitoHomeVC
              self.navigationController?.pushViewController(loginStoryBoard, animated: true)
          }else {
              let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewVC") as! SignUpViewVC
              self.navigationController?.pushViewController(loginStoryBoard, animated: true)
          }
          
      }

      @IBAction func btnBlack(_ sender: UIButton) {
          let loginStoryBoard = UIStoryboard(name: "BlackMarket", bundle: nil).instantiateViewController(withIdentifier: "BlackMarketIntroVC") as! BlackMarketIntroVC
          self.navigationController?.pushViewController(loginStoryBoard, animated: true)
          
      }
    
    @objc func dissmissPostAction()  {
        makeUrlRequest()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.imgMember.layer.cornerRadius = self.imgMember.frame.size.width/2
        let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getTiradeUser_Image(key: "userTirade_Image"), options: .ignoreUnknownCharacters)!
        if  dataDecoded.count > 0 {
            let imageFromData: UIImage = UIImage(data:dataDecoded)!
            self.imgMember.image = imageFromData
        }else {
            self.imgMember.image = UIImage(named: "user")
        }
        self.lblMemberID.text = TiradeUserDefaults.getTiradeUser_Name(key: "userTirade_Name")
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        imgBackOnMyMind.layer.cornerRadius = 10
        txtOnMyMind.placeholder = "What's on your mind?"
        self.navigationController?.navigationBar.isHidden = true
        
        
        NotificationCenter.default.addObserver(self,selector: #selector(tiradecirclebtnClicked(notification:)),name: NSNotification.Name(rawValue: "tiradecirclebtnClicked"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          makeUrlRequest()
        self.makeUrlTrendingVideosRequest()

      }
    
    
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
        
        let fileData = FileTypeInfo()
        fileData.fileData = dictFile["fileData"] as! Data
        fileData.fileName = dictFile["fileName"] as! String
        fileData.uploadFileKey = dictFile["uploadFileKey"] as! String
        fileData.mimeType = dictFile["mimeType"] as! String
        
        var arr = [FileTypeInfo]()
        arr.append(fileData)
        
        formData.fileInfoType.append(fileData)
        print(formData.fileInfoType)
        //        formData.fileInfoType[1] = fileData
        //        formData.fileInfoType[2] = fileData
        //        formData.fileInfoType[3] = fileData
        
    }
    
    @objc private func tiradecirclebtnClicked(notification: NSNotification){
        let goSelection = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
        self.navigationController?.pushViewController(goSelection, animated: false)
        
    }
    
    @IBAction func drawerIconTapped(_ sender: UIButton) {
        let viewController22 = LeftSideViewController(nibName: "LeftSideViewController", bundle: nil)
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: viewController22)
        viewController22.selectedPage = "TiradeHome"
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func btnPostOpen(_ sender: UIButton) {
        let postVc = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "PostVC") as! PostVC
        self.navigationController?.present(postVc, animated: false, completion: {
            
        })

    }
    
    @IBAction func btnFloatingAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            actionButton.tintColor = UIColor.white
            setFloatingMenu(actionButton: actionButton)
            actionButton.isHidden = false
        }else {
            actionButton.items.removeAll()
            actionButton.removeFromSuperview()
            actionButton.isHidden = true
        }
    }
    
    func setFloatingMenu(actionButton : JJFloatingActionButton)  {
        actionButton.addItem(title: "", image: nil) { item in
            self.navigationController?.popViewController(animated: false)
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        
        
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.itemSizeRatio = CGFloat(1)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .trailing
            if item.imageView.image == nil {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .clear
                item.buttonColor = .clear
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }else {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .white
                item.buttonColor = .white
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }
        }
        
        
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        //  actionButton.bottomAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        //actionButton.trailingAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        
        actionButton.handleSingleActionDirectly = true
        actionButton.buttonDiameter = btnFloating.frame.size.width
        
        actionButton.overlayView.backgroundColor = UIColor(white: 1, alpha: 0.1)
        actionButton.buttonImageSize = CGSize(width: 30, height: 30)
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 180)
        
        //        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "X")!)
        //       actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        
        //       actionButton.layer.shadowColor = UIColor.black.cgColor
        //       actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        //       actionButton.layer.shadowOpacity = Float(0.4)
        //       actionButton.layer.shadowRadius = CGFloat(2)
        
    }
    
    
    @IBAction func btnVissionActioon(_ sender: UIButton) {
        let visionBoard = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeVisionBoard") as! TiradeVisionBoard
        self.navigationController?.pushViewController(visionBoard, animated: false)
    }
    @IBAction func btnNotificationAction(_ sender: UIButton) {
        let visionBoard = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeProfileVC") as! TiradeProfileVC
        self.navigationController?.pushViewController(visionBoard, animated: false)
        
    }
    
    @IBAction func btnMoreAction(_ sender: UIButton) {
        
        
    }
    
    
    func commingSoon()  {
        let commingSoonVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommingSoonVC") as! CommingSoonVC
        self.navigationController?.present(commingSoonVC, animated: true, completion: {
        })
    }
    
    @IBAction func btnLogOutAction(_ sender: UIButton) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        TiradeUserDefaults.setTiradeUser_ID(value: "noid")

        let chooseAcc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
        self.navigationController?.pushViewController(chooseAcc, animated: false)
    }
    
    @IBAction func btnCameraAction(_ sender: UIButton) {
       // AttachmentHandler.shared.showImageLibraryList(vc: self)
        let postVc = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "PostVC") as! PostVC
              self.navigationController?.present(postVc, animated: false, completion: {
                  
              })
    }
    @IBAction func btnEmojiAction(_ sender: UIButton) {
        
    }
    func makeUrlRequest()  {
        let url = BASEURL + TIRADEHOMEDATA
        
        ServerRequest.doGetRequestToServer(url, onController: self) { (responseData) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
            }
            do {
                let trashArr : [String : [TrashHome]]  = try! JSONDecoder().decode([String: [TrashHome]].self, from: responseData)
                self.arrTiradeHome = trashArr["data"]
                if self.arrTiradeHome != nil {
                    DispatchQueue.main.async {
                        for image in self.arrTiradeHome! {
                            if image.user_id == TiradeUserDefaults.getUser_ID(key: "user_id") {
                                if image.user_profile != nil && image.user_profile != "" {
                                    self.imgMember.kf.setImage(with: URL(string: image.user_profile!)!)
                                }
                                break
                            }
                        }
                    }
                }
                self.arrTiradeHome = self.arrTiradeHome?.sorted(by: { (obj1, obj2) -> Bool in
                    Int(obj1.post_id!)! > Int(obj2.post_id!)!
                })
                TiradeSingleTon.shared.arrComments = self.arrTiradeHome!
//                self.orignalTrashData = trashArr
//                self.isEnableLeftFilter = false
                DispatchQueue.main.async {
                    DataUtils.removeLoader()

                    for view in self.view.subviews {
                        if view.tag == 112233 {
                            view.removeFromSuperview()
                        }
                    }
                    self.tblTirade.reloadData()
                    //self.getNotificationData()
                    
                }
            }catch  {
                return
            }
        }
    }
    
        func makeUrlTrendingVideosRequest()  {
            let url = BASEURL + TIRADETRENDINGVIDEOS
            
            ServerRequest.doGetRequestToServer(url, onController: self) { (responseData) in
                DispatchQueue.main.async {
                    DataUtils.removeLoader()
                }
                do {
                    //let trashArr : [String : [TrendingVideos]]  = try! JSONDecoder().decode([String: [TrendingVideos]].self, from: responseData)
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(TrendingVideos.self, from: responseData)

                    
                    self.trendingVds = (responseModel.trending_videos?[0].videos)!

                    DispatchQueue.main.async {
                        DataUtils.removeLoader()

                        for view in self.view.subviews {
                            if view.tag == 112233 {
                                view.removeFromSuperview()
                            }
                        }
                        self.collVwTirade.reloadData()
                        //self.getNotificationData()
                        
                    }
                }catch  {
                    return
                }
            }
        }
}



@available(iOS 11.0, *)
extension TiradeHomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrTiradeHome?.count == 0 {
            lblNoDataFound.isHidden = false
        }else {
            lblNoDataFound.isHidden = true
        }
        return arrTiradeHome?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tblCell = tableView.dequeueReusableCell(withIdentifier: "TiradeTblCell") as! TiradeTblCell
        tblCell.vwOnThreeDots.isHidden = true
        tblCell.btnThreeDots.tag = indexPath.row
        tblCell.btnThreeDots.isSelected = false
        tblCell.constraintHeightThreeDotVw.constant = 0
        tblCell.btnOpenImg.tag = indexPath.row
        tblCell.btnThreeDots.addTarget(self, action: #selector(btnThreeDotAction), for: .touchUpInside)
        tblCell.vwBaseCell.backgroundColor = UIColor.random.withAlphaComponent(0.7)
        tblCell.lblDescription.text = (arrTiradeHome?[indexPath.row].post_content)
        tblCell.lblDate.text = (arrTiradeHome?[indexPath.row].created_date)
        tblCell.lblUserName.text = (arrTiradeHome?[indexPath.row].name)
        tblCell.lblLikeCount.text = "\(arrTiradeHome?[indexPath.row].s_agree?.count ?? 0)"
        tblCell.lblCommentCount.text = "\(arrTiradeHome?[indexPath.row].comments?.count ?? 0)"
        tblCell.btnComment.tag = indexPath.row
        tblCell.btnComment.addTarget(self, action: #selector(openCommentController(sender:)), for: .touchUpInside)
        tblCell.btnLike.addTarget(self, action: #selector(likeAction(sender:)), for: .touchUpInside)
        tblCell.btnLike.accessibilityHint = "\(arrTiradeHome?[indexPath.row].post_id ?? "")"
        tblCell.btnOpenImg.addTarget(self, action: #selector(allActionOnUplodedFiles(sender:)), for: .touchUpInside)
        if (arrTiradeHome?[indexPath.row].user_profile) == "" {
            tblCell.imgTiradeProfile.image = UIImage(named: "user_profile_img")
        }else {
            var url : NSString = (self.arrTiradeHome?[indexPath.row].user_profile)! as NSString
            var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            
            tblCell.imgTiradeProfile.kf.setImage(with: URL(string: urlStr as String)!)
        }
        
        if (arrTiradeHome?[indexPath.row].image_path) != "" {
            tblCell.constraintWidthvwImage.constant = 60
            tblCell.imgUploaded.kf.setImage(with: URL(string: (self.arrTiradeHome?[indexPath.row].image_path)!)!)
        }else {
            tblCell.constraintWidthvwImage.constant = 0
        }
        tblCell.bntFBAction.tag = indexPath.row
        tblCell.bntInstaAction.tag = indexPath.row
        tblCell.bntSnapChatAction.tag = indexPath.row
        tblCell.bntTicTocAction.tag = indexPath.row
        tblCell.bntTwtAction.tag = indexPath.row
        
        tblCell.bntFBAction.addTarget(self, action: #selector(bntFBAction(sender:)), for: .touchUpInside)
        tblCell.bntInstaAction.addTarget(self, action: #selector(bntInstaAction(sender:)), for: .touchUpInside)
        tblCell.bntSnapChatAction.addTarget(self, action: #selector(bntSnapChatAction(sender:)), for: .touchUpInside)
        tblCell.bntTicTocAction.addTarget(self, action: #selector(bntTicTocAction(sender:)), for: .touchUpInside)
        tblCell.bntTwtAction.addTarget(self, action: #selector(bntTwtAction(sender:)), for: .touchUpInside)
        return tblCell
        
    }
    
      @objc func bntFBAction(sender : UIButton)  {
            shareOnSocial(sender: sender)
        }
        @objc func bntInstaAction(sender : UIButton)  {
            shareOnSocial(sender: sender)
            
        }
        @objc func bntSnapChatAction(sender : UIButton)  {
            shareOnSocial(sender: sender)
            
        }
        @objc func bntTicTocAction(sender : UIButton)  {
            shareOnSocial(sender: sender)
            
        }
        @objc func bntTwtAction(sender : UIButton)  {
            shareOnSocial(sender: sender)
        }
        
        func shareOnSocial(sender : UIButton) {
            var urlShare = String()
            if arrTiradeHome?[sender.tag].video_path != "" {
                urlShare = (arrTiradeHome?[sender.tag].video_path)!
                
            }else  if arrTiradeHome?[sender.tag].audio_path != "" {
                urlShare = (arrTiradeHome?[sender.tag].audio_path)!
                
            }else  if arrTiradeHome?[sender.tag].image_path != "" {
                urlShare = (arrTiradeHome?[sender.tag].image_path)!
                
            }else  if arrTiradeHome?[sender.tag].pdf_path != "" {
                urlShare = (arrTiradeHome?[sender.tag].pdf_path)!
                
            }else  if arrTiradeHome?[sender.tag].txtfile_path != "" {
                urlShare = (arrTiradeHome?[sender.tag].txtfile_path)!
                
            }else {
                print("nothing selected")
            }
            let shareAll = [urlShare,self.imgMember,self.lblMemberID] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.addToReadingList,UIActivity.ActivityType.mail,UIActivity.ActivityType.message,UIActivity.ActivityType.airDrop,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.print,UIActivity.ActivityType.markupAsPDF,UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
                                                            UIActivity.ActivityType(rawValue: "net.whatsapp.WhatsApp.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "pinterest.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.facebook.Messenger.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.fogcreek.trello.trelloshare"),
                                                            UIActivity.ActivityType(rawValue: "com.linkedin.LinkedIn.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.hammerandchisel.discord.Share"),
                                                            UIActivity.ActivityType(rawValue: "com.google.Gmail.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.google.inbox.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.riffsy.RiffsyKeyboard.RiffsyShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.google.hangouts.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.ifttt.ifttt.share"),    UIActivity.ActivityType(rawValue: "com.burbn.instagram.shareextension")
    ]
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    
    @objc func allActionOnUplodedFiles(sender : UIButton)  {
          if arrTiradeHome?[sender.tag].video_path != "" {
            //  playVideoUrl(url:(trashData?[sender.tag].video_path)!)
              
          }else  if arrTiradeHome?[sender.tag].audio_path != "" {
            //  playAudioUrl(url:(trashData?[sender.tag].audio_path)!)
              
          }else  if arrTiradeHome?[sender.tag].image_path != "" {
              openPhotos(url:(arrTiradeHome?[sender.tag].image_path)!)
              
          }else  if arrTiradeHome?[sender.tag].pdf_path != "" {
            //  openPDF(url:(trashData?[sender.tag].pdf_path)!)
              
          }else  if arrTiradeHome?[sender.tag].txtfile_path != "" {
           //   openTXTFile(url:(trashData?[sender.tag].txtfile_path)!)
              
          }else {
              print("nothing selected")
          }
      }
    
    @objc func openCommentController(sender : UIButton) {
        let chatVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.selectedCommentRow = sender.tag
        chatVC.selectedCommentSection = "tirade"
        self.navigationController?.present(chatVC, animated: true, completion: {
        })
    }
    
   @objc func openPhotos(url : String)  {
        let rtfVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OpenPhotosVC") as! OpenPhotosVC
        rtfVC.imageUrl = url
        self.navigationController?.present(rtfVC, animated: true, completion: nil)
        
    }
    
    @objc func likeAction(sender : UIButton)  {
        let dataDict = ["post_id" : sender.accessibilityHint ?? "","value":"s_agree"] as [String : Any]
        sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + TRASHTIRADEACTION
        // let home = IncognitoHomeVC()
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: self) { (data) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                DataUtils.showAlert(title: "", msg: "Successfully Liked", selfObj: self) {
                        self.makeUrlRequest()
                }
            }
        }
    }

    
    @objc func btnThreeDotAction(sender : UIButton)  {
        sender.isSelected = !sender.isSelected
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblTirade.cellForRow(at: myIndexPath as IndexPath) as! TiradeTblCell
        if sender.isSelected {
            UIView.animate(withDuration: 0.2) {
                cell.vwOnThreeDots.isHidden = false
                cell.constraintHeightThreeDotVw.constant = 135
                self.view.layoutIfNeeded()
            }
        }else {
            cell.vwOnThreeDots.isHidden = true
            cell.constraintHeightThreeDotVw.constant = 0
        }
        
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trendingVds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    @objc func playVideoByButton(sender : UIButton)  {
        if trendingVds[sender.tag].post_video != "" {
            playVideoUrl(url: trendingVds[sender.tag].post_video!)
        }
    }
    
    @objc func playVideoUrl(url : String) {
        var url : NSString = url as NSString
        var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        var searchURL : NSURL = NSURL(string: urlStr as String)!
        
        let videoURL = searchURL
        let player = AVPlayer(url: videoURL as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TiradeHomeCollCell", for: indexPath) as! TiradeHomeCollCell
        collectionCell.btnPlay.tag = indexPath.row
        collectionCell.btnPlay.addTarget(self, action: #selector(playVideoByButton(sender:)), for: .touchUpInside)
        collectionCell.imgVideo.image = UIImage(named: collLblimg[indexPath.row])
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 135, height: 85)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //playVideoUrl(url:(trendingVds?[indexPath.row].)!)
        if trendingVds[indexPath.row].post_video != "" {
            playVideoUrl(url: trendingVds[indexPath.row].post_video!)
        }
    }
}
