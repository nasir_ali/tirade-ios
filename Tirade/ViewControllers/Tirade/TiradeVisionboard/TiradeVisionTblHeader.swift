//
//  TiradeVisionTblHeader.swift
//  Tirade
//
//  Created by admin on 30/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeVisionTblHeader: UITableViewCell {

   @IBOutlet weak var lblLeftHeader : UILabel!
   @IBOutlet weak var txtRightHeader : UITextField!
   @IBOutlet weak var imgSelectionRight : UIImageView!


}
