//
//  TiradeVisionBoard.swift
//  Tirade
//
//  Created by admin on 30/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import JJFloatingActionButton

class TiradeVisionBoard: TiradeBaseVC {
    
    @IBOutlet weak var btnSave : UIButton!
    @IBOutlet weak var tblVision : UITableView!
    @IBOutlet weak var btnFloating: UIButton!
    
    var actionButton = JJFloatingActionButton()
    
    
    var arrMusic = ["music1","music2","music3","music4","music5","music6","music7","music8"]
    var arrGroup = ["Sports","Doctor","Designer","S-Media","Joke","Designer","S-Media","Joke"]
    
    var arrMusicImg = ["tempvision1"]
    var arrGroupImg = ["tempvision2"]
    var arrPetsImg = ["tempvision3"]
    var arrManageImg = ["tempvision4"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        btnSave.layer.cornerRadius = 5
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        actionButton.items.removeAll()
        actionButton.removeFromSuperview()
        actionButton.isHidden = true
        btnFloating.isSelected = false
    }
    
    @IBAction func btnVisionAction(_ sender: UIButton) {
        
    }
    @IBAction func btnFloatingAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            actionButton.tintColor = UIColor.white
            setFloatingMenu(actionButton: actionButton)
            actionButton.isHidden = false
        }else {
            actionButton.items.removeAll()
            actionButton.removeFromSuperview()
            actionButton.isHidden = true
        }
    }
    
    func setFloatingMenu(actionButton : JJFloatingActionButton)  {
        actionButton.addItem(title: "", image: UIImage(named: "homeHut")?.withRenderingMode(.alwaysOriginal)) { item in
            self.navigationController?.popViewController(animated: false)
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: UIImage(named: "notification")?.withRenderingMode(.alwaysOriginal)) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: UIImage(named: "goLiveIcon")?.withRenderingMode(.alwaysOriginal)) { item in
            // do something
        }
        
        
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.itemSizeRatio = CGFloat(1)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .trailing
            if item.imageView.image == nil {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .clear
                item.buttonColor = .clear
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }else {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .white
                item.buttonColor = .white
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }
        }
        
        
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        //  actionButton.bottomAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        //actionButton.trailingAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        
        actionButton.handleSingleActionDirectly = true
        actionButton.buttonDiameter = btnFloating.frame.size.width
        
        actionButton.overlayView.backgroundColor = UIColor(white: 1, alpha: 0.1)
        actionButton.buttonImageSize = CGSize(width: 30, height: 30)
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 180)
        
        //        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "X")!)
        //       actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        
        //       actionButton.layer.shadowColor = UIColor.black.cgColor
        //       actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        //       actionButton.layer.shadowOpacity = Float(0.4)
        //       actionButton.layer.shadowRadius = CGFloat(2)
        
    }
    @IBAction func btnProfileAction(_ sender: UIButton) {
                let forumVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeProfileVC") as! TiradeProfileVC
                self.navigationController?.pushViewController(forumVC, animated: false)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        
    }
    @IBAction func btnBackction(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
}
extension TiradeVisionBoard : UITableViewDelegate,UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ARRCATHEADER.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "TiradeVisionTblHeader") as! TiradeVisionTblHeader
        headerCell.lblLeftHeader.text = ARRCATHEADER[section]
        headerCell.txtRightHeader.text = ARRCATHEADERSELECTION[section]
        headerCell.imgSelectionRight.image = UIImage(named: ARRIMGEHADERSELECTION[section])
        if section == 1 || section == 2 {
            headerCell.txtRightHeader.setIconOnTextFieldRight(UIImage(named: "dropArrow")!)
        }
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 70
        case 1:
            return 60
        case 2:
            return 60
        case 3:
            return 70
        default:
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TiradeVisionBoardTblCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
        // tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TiradeVisionBoardTblCell else { return }
        
        // storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tblCell = tblVision.dequeueReusableCell(withIdentifier: "TiradeVisionBoardTblCell", for: indexPath) as! TiradeVisionBoardTblCell
        // tblCell.collectionVision.tag = indexPath.section
        
        //tableView.dequeueReusableCell(withIdentifier: "TiradeVisionBoardTblCell") as! TiradeVisionBoardTblCell
        return tblCell
        
    }
}

extension TiradeVisionBoard: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView.tag {
        case 1:
            return 6
        case 2:
            return arrMusic.count
        case 3:
            return arrGroup.count
        case 4:
            return 6
        case 5:
            return 6
        case 6:
            return 6
        default:
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(collectionView.tag)
        switch collectionView.tag {
        case 0:
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateStoryCollCell", for: indexPath) as! CreateStoryCollCell
            collectionCell.imgCircle.layer.cornerRadius = collectionCell.imgCircle.frame.size.width/2
            collectionCell.imgCircle.clipsToBounds = true
            
            return collectionCell
        case 1:
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseBackTheme", for: indexPath) as! ChooseBackTheme
            collectionCell.vwBack.backgroundColor = UIColor.random
            
            return collectionCell
        case 2:
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseBackMusic", for: indexPath) as! ChooseBackMusic
            collectionCell.vwBack.layer.cornerRadius = 8
            collectionCell.vwBack.backgroundColor = UIColor.blue
            
            collectionCell.lblTitle.text = arrMusic[indexPath.row]
            collectionCell.img.image = UIImage(named: arrMusicImg[0])
            
            return collectionCell
            
        case 3:
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseBackMusic", for: indexPath) as! ChooseBackMusic
            collectionCell.vwBack.layer.cornerRadius = 0
            collectionCell.img.layer.cornerRadius = 0
            collectionCell.vwBack.backgroundColor = UIColor.clear
            
            collectionCell.lblTitle.text = arrGroup[indexPath.row]
            collectionCell.img.image = UIImage(named: arrGroupImg[0])
            
            return collectionCell
            
        case 4:
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetsPageCollCell", for: indexPath) as! PetsPageCollCell
            collectionCell.img.image = UIImage(named: arrPetsImg[0])
            
            return collectionCell
            
        case 5:
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetsPageCollCell", for: indexPath) as! PetsPageCollCell
            collectionCell.img.image = UIImage(named: arrManageImg[0])
            
            return collectionCell
            
        default:
            print("uuu")
        }
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseBackMusic", for: indexPath) as! ChooseBackMusic
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 35, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}


