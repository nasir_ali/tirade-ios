//
//  TiradeVisionBoardTblCell.swift
//  Tirade
//
//  Created by admin on 30/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class TiradeVisionBoardTblCell: UITableViewCell {
    
    @IBOutlet weak var collectionVision : UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        collectionVision.delegate = self
//        collectionVision.dataSource = self
        // Initialization code
    }
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

           collectionVision.delegate = dataSourceDelegate
           collectionVision.dataSource = dataSourceDelegate
           collectionVision.tag = row
           collectionVision.setContentOffset(collectionVision.contentOffset, animated:false) // Stops collection view if it was scrolling.
           collectionVision.reloadData()
       }

       var collectionViewOffset: CGFloat {
           set { collectionVision.contentOffset.x = newValue }
           get { return collectionVision.contentOffset.x }
       }
    
   
    
    
}
