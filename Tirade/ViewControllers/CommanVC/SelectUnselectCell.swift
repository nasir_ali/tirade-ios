//
//  SelectUnselectCell.swift
//  Tirade
//
//  Created by admin on 18/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class SelectUnselectCell: UITableViewCell {

    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var lblUse : UILabel!
    @IBOutlet weak var btnCheckUncheck : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        // Configure the view for the selected state
    }

}
