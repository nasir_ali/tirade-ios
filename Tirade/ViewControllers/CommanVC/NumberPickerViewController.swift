//
//  NumberPickerViewController.swift
//  EzPopup_Example
//
//  Created by Huy Nguyen on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

protocol NumberPickerViewControllerDelegate: class {
    func numberPickerViewController(sender: NumberPickerViewController, didSelectNumber selectedIDs: [ String])
}

var arrSelectedId : [String]?


class NumberPickerViewController: TiradeBaseVC {
    
    var isSelectFromDetail = false
    var titleHere = ""
    var isSelectFromVideoDetail = false
    

    @IBOutlet weak var tableView: UITableView!
    var dataOfInvite : [TrashHome]?
    var uniqueInvite = [TrashHome]()
    var arrDetailsLike : [AgreeDisagree]?
    
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var arrComments : [Comments]?
    var arrLikes : [Likes]?


    weak var delegate: NumberPickerViewControllerDelegate?
    var seen = Set<String>()
    
    static func instantiate() -> NumberPickerViewController? {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(NumberPickerViewController.self)") as? NumberPickerViewController
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        NotificationCenter.default.addObserver(self,selector: #selector(updateArrayData(notification:)),name: NSNotification.Name(rawValue: "updateArrayData"), object: nil)

        lblTitle.text = titleHere
        if isSelectFromVideoDetail {
            btnInvite.isHidden = true
            //lblTitle.text = titleHere
        }else {
            if isSelectFromDetail {
                btnInvite.isHidden = true
                lblTitle.text = titleHere
            }else {
                btnInvite.isHidden = false
                lblTitle.text = "Choose to debate"
                arrSelectedId = [String]()
                for message in dataOfInvite! {
                    if !seen.contains(message.user_id!) {
                        if message.user_id != TiradeUserDefaults.getUser_ID(key: "user_id") {
                            uniqueInvite.append(message)
                            seen.insert(message.user_id!)
                        }
                    }
                }
            }
        }

        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "\(UITableViewCell.self)")
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    @objc func updateArrayData(notification :  NSNotification)  {
        guard let selectedTitle : String = notification.userInfo!["selectedTitle"] as! String else { return }
        if isSelectFromVideoDetail {
            guard let selectedTitleArr : [Comments] = notification.userInfo!["selectedTitleArr"] as! [Comments] else { return }
            arrComments = selectedTitleArr
            lblTitle.text = selectedTitle
        }else {
            guard let selectedTitleArr : [AgreeDisagree] = notification.userInfo!["selectedTitleArr"] as! [AgreeDisagree] else { return }
            arrDetailsLike = selectedTitleArr
            lblTitle.text = selectedTitle
        }
        tableView.reloadData()
    }
}

extension NumberPickerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSelectFromVideoDetail {
            if lblTitle.text == "Total Likes" {
                return arrLikes?.count ?? 0
            }
            return arrComments?.count ?? 0
        }
        if isSelectFromDetail {
            return arrDetailsLike?.count ?? 0
        }
        return uniqueInvite.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tblCell = tableView.dequeueReusableCell(withIdentifier: "SelectUnselectCell", for: indexPath) as! SelectUnselectCell
        if isSelectFromVideoDetail {
            if lblTitle.text == "Total Likes" {
                tblCell.lblUse.text = arrLikes?[indexPath.row].user_name
                if (arrLikes?[indexPath.row].user_img) == "" {
                    tblCell.imgUser.image = UIImage(named: "user_profile_img")
                }else {
                    if (self.arrLikes?[indexPath.row].user_img) != nil {
                        tblCell.imgUser.kf.setImage(with: URL(string: (self.arrLikes?[indexPath.row].user_img)!)!)
                    }
                }
            }else {
                tblCell.lblUse.text = arrComments?[indexPath.row].name
                if (arrComments?[indexPath.row].user_img) == "" {
                    tblCell.imgUser.image = UIImage(named: "user_profile_img")
                }else {
                    if (self.arrComments?[indexPath.row].user_img) != nil {
                        tblCell.imgUser.kf.setImage(with: URL(string: (self.arrComments?[indexPath.row].user_img)!)!)
                    }
                }
            }
            
            
        }else if isSelectFromDetail {
            tblCell.lblUse.text = arrDetailsLike?[indexPath.row].name
            if (arrDetailsLike?[indexPath.row].user_img) == "" {
                tblCell.imgUser.image = UIImage(named: "user_profile_img")
            }else {
                if (self.arrDetailsLike?[indexPath.row].user_img) != nil {
                    tblCell.imgUser.kf.setImage(with: URL(string: (self.arrDetailsLike?[indexPath.row].user_img)!)!)
                }
            }

        }else {
            tblCell.lblUse.text = uniqueInvite[indexPath.row].username
            if (uniqueInvite[indexPath.row].user_profile) == "" {
                tblCell.imgUser.image = UIImage(named: "user_profile_img")
            }else {
                tblCell.imgUser.kf.setImage(with: URL(string: (self.uniqueInvite[indexPath.row].user_profile)!)!)
            }
            if (uniqueInvite[indexPath.row].isInvite) == nil || (uniqueInvite[indexPath.row].isInvite) == false {
                tblCell.btnCheckUncheck.image = UIImage(named: "questionBack")
            }else {
                tblCell.btnCheckUncheck.image = UIImage(named: "checkTik")
            }
        }
      
        return tblCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isSelectFromDetail {
            
        }else {
            if (uniqueInvite[indexPath.row].isInvite) == nil || (uniqueInvite[indexPath.row].isInvite) == false {
                uniqueInvite[indexPath.row].isInvite = true
                arrSelectedId!.append(uniqueInvite[indexPath.row].user_id!)
            }else {
                if (arrSelectedId?.contains(uniqueInvite[indexPath.row].user_id!))! {
                    if let index = arrSelectedId!.firstIndex(of: uniqueInvite[indexPath.row].user_id!) {
                                   arrSelectedId?.remove(at: index)
                               }
                }
                uniqueInvite[indexPath.row].isInvite = false
            }
            print(arrSelectedId!)
            tableView.reloadData()

        }
        //
    }
    
    
    @IBAction func btnInvite(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        delegate?.numberPickerViewController(sender: self, didSelectNumber: arrSelectedId!)
    }
}

extension Array {
    
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        var results = [Element]()
        
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        
        return results
    }
}
