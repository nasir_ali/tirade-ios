//
//  SettingCell.swift
//  Tirade
//
//  Created by admin on 18/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

   @IBOutlet weak var lblTxt : UILabel!
   @IBOutlet weak var vwBack : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        vwBack.layer.shadowOffset = CGSize(width: 0, height: 0)
        vwBack.layer.shadowColor = UIColor.white.cgColor
        vwBack.layer.shadowOpacity = 0.23
        vwBack.layer.shadowRadius = 4
    }
}
