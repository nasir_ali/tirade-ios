//
//  SettingVC.swift
//  Tirade
//
//  Created by admin on 18/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class SettingVC: TiradeBaseVC,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblSetting : UITableView!
    
    let arrCount = 3
    var arrValues = ["TERMS OF USE","TERMS OF SERVICE","PRIVACY POLICY"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let settingCell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        settingCell.lblTxt.text = arrValues[indexPath.row]
        return settingCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let descripVC = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "DescriptionVC") as! DescriptionVC
        descripVC.descritionTitle = arrValues[indexPath.row]
        if indexPath.row == 0 {
            descripVC.descritionValue = TERMSEOFUSE
        }else if indexPath.row == 1{
            descripVC.descritionValue = TERMSEOFSERVICE
        }else {
            descripVC.descritionValue = PRIVACY
            
        }
        
        self.navigationController?.pushViewController(descripVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    @IBAction func btnBackction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
