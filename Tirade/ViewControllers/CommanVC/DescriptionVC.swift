//
//  DescriptionVC.swift
//  Tirade
//
//  Created by admin on 18/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class DescriptionVC: TiradeBaseVC {

    var descritionTitle = String()
    var descritionValue = String()

    @IBOutlet weak var lblTitleOfPage : UILabel!
    @IBOutlet weak var lblDescription : UITextView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        lblTitleOfPage.text = descritionTitle
        lblDescription.text = descritionValue
    }
    
    @IBAction func btnBackction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
}
