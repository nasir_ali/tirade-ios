//
//  ViewController.swift
//  CometChat
//
//  Created by Marin Benčević on 01/08/2019.
//  Copyright © 2019 marinbenc. All rights reserved.
//

import UIKit

final class ChatViewController: TiradeBaseVC,UITableViewDelegate,UITableViewDataSource {
    
    
    private enum Constants {
        static let incomingMessageCell = "incomingMessageCell"
        static let outgoingMessageCell = "outgoingMessageCell"
        static let contentInset: CGFloat = 24
        static let placeholderMessage = "Write a comment..."
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textAreaBackground: UIView!
    @IBOutlet weak var textAreaBottom: NSLayoutConstraint!
    @IBOutlet weak var emptyChatView: UIView!
    var selectedCommentRow = 0
    var selectedCommentSection = "Incognito"
    var isVideoPLayerComments = false
    
    //new
    var commentsID : String?
    var arrComments : [Comments]?
    
    
    
    // MARK: - Actions
    
    @IBAction func onSendButtonTapped(_ sender: Any) {
        sendMessage()
    }
    
    
    // MARK: - Interaction
    
    private func sendMessage() {
        if textView.text == Constants.placeholderMessage {
            return
        }
        let message: String = textView.text
        
        let msg = Message()
        msg.comment = textView.text
        msg.isIncoming = nil
        
        TiradeSingleTon.shared.arrComments[selectedCommentRow].comments?.append(msg)
        if isVideoPLayerComments {
           // TiradeSingleTon.shared.arrVideoComment[selectedCommentRow].comments?.append(msg)
        }
        
        guard !message.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else {
            return
        }
        
        if isVideoPLayerComments {
            let forumData = ["forum_id" : commentsID ?? "","comment":textView.text,"user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendVideoActionDataToServer(dataDict: forumData as [String : AnyObject])
            
        }else {
            let dataDict = ["post_id_fk" : TiradeSingleTon.shared.arrComments[selectedCommentRow].post_id,"author_id_fk":TiradeUserDefaults.getUser_ID(key: "user_id"),"comment":textView.text] as [String : Any]
            
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
            
        }
        textView.endEditing(true)
        addTextViewPlaceholer()
        scrollToLastCell()
    }
    
    
    
    func getVideoActionDataToServer(dataDict : [String : AnyObject])  {
        let url = "https://vridhisoftech.in/tirade/incognito/forum/get_comments.php"
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: self) { (responseData) in
            DispatchQueue.main.async {
             let trashArr   = try! JSONDecoder().decode(CommentsDict.self, from: responseData)
                self.arrComments = trashArr.comments
                self.tableView.reloadData()
            }
        }
    }
    
    func sendVideoActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + FORUMVIDEOACTIONCOMMENT
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: self) { (responseData) in
            DispatchQueue.main.async {
             let trashArr   = try! JSONDecoder().decode(CommentsDict.self, from: responseData)
                self.arrComments = trashArr.comments
                self.tableView.reloadData()
            }
        }
    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        if selectedCommentSection == "Incognito" {
            let url = BASEURL + POSTCOMMENT
            // let home = IncognitoHomeVC()
            ServerRequest.doPostRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) {
                self.tableView.reloadData()
            }
        }else {
            let url = BASEURL + POSTTIRADECOMMENT
            // let home = IncognitoHomeVC()
            ServerRequest.doPostRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) {
                self.tableView.reloadData()
            }
        }
        
    }
    @IBAction func btnDownPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isVideoPLayerComments {
            let forumData = ["forum_id" : commentsID ?? "","comment":textView.text,"user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            getVideoActionDataToServer(dataDict: forumData as [String : AnyObject])

        }
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")
        
        title = "#General"
        
        emptyChatView.isHidden = true
        
        setUpTableView()
        setUpTextView()
        tableView.reloadData()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addTextViewPlaceholer()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Add default shadow to navigation bar
        let navigationBar = navigationController?.navigationBar
        navigationBar?.shadowImage = nil
    }
    
    
    // MARK: - Set up
    
    private func setUpTextView() {
        //    textView.isScrollEnabled = false
        //    textView.textContainer.heightTracksTextView = true
        textView.delegate = self
        
        textAreaBackground.layer.addShadow(
            color: UIColor(red: 189 / 255, green: 204 / 255, blue: 215 / 255, alpha: 54 / 100),
            offset: CGSize(width: 2, height: -2),
            radius: 4)
    }
    
    private func setUpTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: Constants.contentInset, left: 0, bottom: 0, right: 0)
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isVideoPLayerComments {
            return arrComments?.count ?? 0
            
        }
        return TiradeSingleTon.shared.arrComments[selectedCommentRow].comments!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isVideoPLayerComments {
            let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncomingMessageTableViewCell") as! IncomingMessageTableViewCell
            tblCell.contentLabel.text = arrComments![indexPath.row].comment
            tblCell.userName.text = arrComments![indexPath.row].name
            let input = arrComments![indexPath.row].created_at
            let split = input?.components(separatedBy: " ")
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: split?[0] ?? "2020/09/09")
            formatter.dateFormat = "MMM dd, yyyy"
            let resultString = formatter.string(from: date!)
            print(resultString)
            tblCell.timeLabel.setTitle("\(resultString) \(split?[1] ?? "00:00")", for: .normal)
            

            if (self.arrComments?[indexPath.row].user_img) == "" || (self.arrComments?[indexPath.row].user_img) == nil {
                tblCell.userImage.image = UIImage(named: "user_profile_img")
            }else {
                tblCell.userImage.kf.setImage(with: URL(string: (self.arrComments?[indexPath.row].user_img)!)!)
                
            }
            
            
            return tblCell
        }else {
            if TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].isIncoming == nil {
                let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncomingMessageTableViewCell") as! IncomingMessageTableViewCell
                
                tblCell.contentLabel.text = TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].comment
                //  tblCell.contentLabel.userImage =  TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_img
                tblCell.userName.text = TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_name
                let input = TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].created_at
                let split = input?.components(separatedBy: " ")
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let date = formatter.date(from: split?[0] ?? "2020/09/09")
                formatter.dateFormat = "MMM dd, yyyy"
                let resultString = formatter.string(from: date!)
                print(resultString)
                tblCell.timeLabel.setTitle("\(resultString) \(split?[1] ?? "00:00")", for: .normal)
                if TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_img == "" {
                    tblCell.userImage.image = UIImage(named: "user_profile_img")
                }else {
                    if TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_img == nil {
                        if selectedCommentSection == "Incognito" {
                            let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getUser_Image(key: "user_Image"), options: .ignoreUnknownCharacters)!
                            if  dataDecoded.count > 0 {
                                let imageFromData: UIImage = UIImage(data:dataDecoded)!
                                tblCell.userImage.image = imageFromData
                            }else {
                                tblCell.userImage.image = UIImage(named: "user")
                            }
                            
                            tblCell.userName.text = TiradeUserDefaults.getUser_Name(key: "user_Name")
                        }else {
                            let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getTiradeUser_Image(key: "userTirade_Image"), options: .ignoreUnknownCharacters)!
                            if  dataDecoded.count > 0 {
                                let imageFromData: UIImage = UIImage(data:dataDecoded)!
                                tblCell.userImage.image = imageFromData
                            }else {
                                tblCell.userImage.image = UIImage(named: "user")
                            }
                            tblCell.userName.text = TiradeUserDefaults.getTiradeUser_Name(key: "userTirade_Name")
                        }
                        
                    }else {
                        tblCell.userImage.kf.setImage(with: URL(string: TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_img))
                    }
                }
                
                return tblCell
            }
            let tblCell = tableView.dequeueReusableCell(withIdentifier: "OutgoingMessageTableViewCell") as! OutgoingMessageTableViewCell
            
            tblCell.contentLabel.text =  TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].comment
            tblCell.userName.text = TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_name
            let input = TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].created_at
            let split = input?.components(separatedBy: " ")
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: split?[0] ?? "2020/09/09")
            formatter.dateFormat = "MMM dd, yyyy"
            let resultString = formatter.string(from: date!)
            tblCell.timeLabel.setTitle("\(resultString) \(split?[1] ?? "00:00")", for: .normal)
            if TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_img == "" {
                tblCell.userImage.image = UIImage(named: "user_profile_img")
            }else {
                let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getUser_Image(key: "user_Image"), options: .ignoreUnknownCharacters)!
                if  dataDecoded.count > 0 {
                    let imageFromData: UIImage = UIImage(data:dataDecoded)!
                    tblCell.userImage.image = imageFromData
                }else {
                    tblCell.userImage.image = UIImage(named: "user")
                    
                }
                // tblCell.userImage.kf.setImage(with: URL(string: TiradeSingleTon.shared.arrComments[selectedCommentRow].comments![indexPath.row].user_img))
            }
            return tblCell
            
        }
        
    }
    
    
    private func scrollToLastCell() {
        //    let lastRow = tableView.numberOfRows(inSection: 0) - 1
        //    guard lastRow > 0 else {
        //      return
        //    }
        //
        //    let lastIndexPath = IndexPath(row: lastRow, section: 0)
        //    tableView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
        tableView.reloadData()
    }
}

// MARK: - UITextViewDelegate
extension ChatViewController: UITextViewDelegate {
    private func addTextViewPlaceholer() {
        textView.text = Constants.placeholderMessage
        textView.textColor = .placeholderBody
    }
    
    private func removeTextViewPlaceholder() {
        textView.text = ""
        textView.textColor =  .darkBody
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        removeTextViewPlaceholder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            addTextViewPlaceholer()
        }
    }
}

