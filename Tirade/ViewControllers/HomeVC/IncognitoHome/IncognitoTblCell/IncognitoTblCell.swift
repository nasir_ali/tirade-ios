//
//  IncognitoTblCell.swift
//  Tirade
//
//  Created by admin on 27/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class IncognitoTblCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var vwBaseCell: UIView!
    @IBOutlet weak var vwBasePlusIcons: UIView!
    @IBOutlet weak var collVwPlusAction: UICollectionView!
    @IBOutlet weak var constraintWidthMenu: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightThreeDotVw: NSLayoutConstraint!
    @IBOutlet weak var btnThreeDots: UIButton!
    @IBOutlet weak var btnDropArrow: UIButton!

    @IBOutlet weak var vwOnThreeDots: UIView!
    @IBOutlet weak var vwOnDropArrow: UIView!
    @IBOutlet weak var constraintHeightDropArrow: NSLayoutConstraint!

    @IBOutlet weak var btnPlusIcon: UIButton!
    
    
    @IBOutlet weak var lblUserTitle: UILabel!
    @IBOutlet weak var ImgUser: UIImageView!
    @IBOutlet weak var postImg: UIImageView!

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var vwVideo: UIView!
    @IBOutlet weak var lblContent: UILabel!
    var selfObj : IncognitoHomeVC?

    @IBOutlet weak var btnPlayActions: UIButton!

      @IBOutlet weak var bntFBAction: UIButton!
      @IBOutlet weak var bntInstaAction: UIButton!
      @IBOutlet weak var bntSnapChatAction: UIButton!
      @IBOutlet weak var bntTicTocAction: UIButton!
      @IBOutlet weak var bntTwtAction: UIButton!
    
    
     @IBOutlet weak var lblFileName: UILabel!
     @IBOutlet weak var lblFileTitle: UILabel!
     @IBOutlet weak var imgFile: UIImageView!
     @IBOutlet weak var vwCenterLine: UIView!

    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblSubCategoryTitle: UILabel!

    
    @IBOutlet weak var imgHeading: UIImageView!
    @IBOutlet weak var constraintHeightVwPost: NSLayoutConstraint!
    @IBOutlet weak var imgPostOnCell: UIImageView!

    
    let lblDecArray = ["Like","Dislike","Boo","Comment","Agree","Cheer","Disagree"]
    let lblIconArray = ["agreeThumb","disagreeThumb","boo","comment.png","agree.png","Cheer.png","dis_agree"]
    
    var trashAction : TrashHome?
    
    override func prepareForReuse() {
               //set your cell's state to default here

               self.ImgUser.image = nil
               self.postImg.image = nil

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblContent.textAlignment = .left
        lblContent.numberOfLines = 0
        lblContent.sizeToFit()
        self.lblFileName.isHidden = true
        self.lblFileTitle.isHidden = true
        self.imgFile.isHidden = true
        self.vwCenterLine.isHidden = true
        
        
        ImgUser.layer.cornerRadius = ImgUser.frame.size.height/2
        
        vwOnThreeDots.backgroundColor = UIColor(white: 1, alpha: 0.4)
        vwOnDropArrow.backgroundColor = UIColor(white: 1, alpha: 0.4)

        vwBasePlusIcons.backgroundColor = UIColor(white: 0.1, alpha: 0.4)
        vwBasePlusIcons.layer.cornerRadius = 10
        vwBasePlusIcons.layer.masksToBounds = true
        constraintWidthMenu.constant = 0
        collVwPlusAction.delegate = self
        collVwPlusAction.dataSource = self
        vwBaseCell.layer.cornerRadius = 20
        vwBaseCell.layer.masksToBounds = true
        
        vwVideo.layer.cornerRadius = 10
        vwVideo.layer.masksToBounds = true
        vwVideo.layer.borderWidth = 0
        vwVideo.layer.borderColor = UIColor.white.cgColor
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lblDecArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "IncognitoCollPlusCell", for: indexPath) as! IncognitoCollPlusCell
        collectionCell.lblDescription.text = lblDecArray[indexPath.row]
        collectionCell.imgCircle.image = UIImage(named: lblIconArray[indexPath.row])
        switch indexPath.item {
        case 0:
            collectionCell.lblCount.text = "\(trashAction?.s_agree?.count ?? 0)"
        case 1:
            collectionCell.lblCount.text = "\(trashAction?.s_disagree?.count ?? 0)"
        case 2:
            collectionCell.lblCount.text = "\(trashAction?.boo?.count ?? 0)"
        case 3:
            collectionCell.lblCount.text = "\(trashAction?.comments?.count ?? 0)"
        case 4:
            collectionCell.lblCount.text = "\(trashAction?.agree?.count ?? 0)"
        case 5:
            collectionCell.lblCount.text = "\(trashAction?.cheers?.count ?? 0)"
        case 6:
            collectionCell.lblCount.text = "\(trashAction?.disagree?.count ?? 0)"
            
        default:
            collectionCell.lblCount.text = ""
        }
        
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 35, height: 35)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
        
        switch indexPath.item {
        case 0:
            let dataDict = ["post_id" : trashAction?.post_id ?? "","value":"s_agree","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
        case 1:
            let dataDict = ["post_id" : trashAction?.post_id ?? "","value":"s_disagree","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
        case 2:
            let dataDict = ["post_id" : trashAction?.post_id ?? "","value":"boo","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
        case 3:
            openCommentController(selectedRow: collectionView.tag)
        case 4:
            let dataDict = ["post_id" : trashAction?.post_id ?? "","value":"agree","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
        case 5:
            let dataDict = ["post_id" : trashAction?.post_id ?? "","value":"cheers","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
        case 6:
            let dataDict = ["post_id" : trashAction?.post_id ?? "","value":"disagree","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
            
        default:
            var dataDict = ["post_id" : trashAction?.post_id ?? "","value":"s_disagree","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
        }
        //        if indexPath.item == 3 {
        //            openCommentController(selectedRow: collectionView.tag)
        //        }
    }
    
    func openCommentController(selectedRow : Int) {
        let chatVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.selectedCommentRow = selectedRow
        selfObj?.navigationController?.present(chatVC, animated: true, completion: {
        })
    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + TRASHHOMEACTION
        // let home = IncognitoHomeVC()
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: selfObj) { (data) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    NotificationCenter.default.post(name: Notification.Name("tiradeTableReloded"), object: nil)
                }
            }
        }
    }
}
