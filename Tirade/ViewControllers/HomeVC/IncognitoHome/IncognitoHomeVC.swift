//
//  HomeVC.swift
//  Tirade
//
//  Created by videotap ios on 13/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import Alamofire
import  Kingfisher
import AVKit
import JPVideoPlayer
import Social

class IncognitoHomeVC: TiradeBaseVC {
    
    @IBOutlet weak var collVwIncognito: UICollectionView!
    @IBOutlet weak var tblIncognito: UITableView!
    @IBOutlet weak var btnFloating: UIButton!
    @IBOutlet weak var btnBackVW: UIView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMemberID: UILabel!
    var lastContentOffset: CGFloat = 0


    
    @IBOutlet weak var btnSearch: UILabel!
    var arrVideoImg = [UIImage]()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarHeightCons: NSLayoutConstraint!
    @IBOutlet weak var newFeedHeightCons: NSLayoutConstraint!

    var isLocalSearch = false
    
    var actionButton = JJFloatingActionButton()
    var collLbldata =  [INCOGNITOSHOUTOUT,INCOGNITOREPRESENT,INCOGNITOTRASH,INCOGNITODEBATE,INCOGNITOCALLOUT,INCOGNITOJOKES,INCOGNITOFORUMS,INCOGNITOSHAME]
    var collLblimg =  [INCOGNITOTRASHIMG,INCOGNITODEBATEIMG,INCOGNITOTRASHIMG,INCOGNITODEBATEIMG,INCOGNITOCALLOUTIMG,INCOGNITOJOKESIMG,INCOGNITOFORUMSIMG,INCOGNITOSHAMEIMG]
    
    var trashData : [TrashHome]?
    var newsFeed : [TrashHome]?

    var arrNotification : [DebateNotification]?
    
    var orignalTrashData : [TrashHome]?
    var config = R5Configuration()
    var stream : R5Stream?
    var isEnableLeftFilter = false
    
    
    @IBOutlet weak var collVWNewsFeed: UICollectionView!
    var scrollingTimer = Timer()
    var rowIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollingTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerNewsFeed(theTimer:)), userInfo: rowIndex, repeats: true)
        
        
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        searchBar.tintColor = UIColor.blue
        searchBarHeightCons.constant = 0
        searchBar.placeholder = "Search Post"
        let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getUser_Image(key: "user_Image"), options: .ignoreUnknownCharacters)!
        if  dataDecoded.count > 0 {
            let imageFromData: UIImage = UIImage(data:dataDecoded)!
            self.imgMember.image = imageFromData
        }else {
            self.imgMember.image = UIImage(named: "user")
        }
        self.lblMemberID.text = TiradeUserDefaults.getUser_Name(key: "user_Name")
        self.lblNotification.isHidden = true
       // getLocalNotificationOnDebate()
        
        
        imgMember.layer.cornerRadius =  imgMember.frame.size.height/2
        imgMember.layer.masksToBounds = true
        
        lblNotification.layer.cornerRadius =  lblNotification.frame.size.height/2
        lblNotification.layer.masksToBounds = true
        
        lblNoDataFound.isHidden = true
        let config = R5Configuration()
        config.host = "http://localhost:5080/"
        config.port = 5080
        config.contextName = "live"
        config.licenseKey = "3YAK-CNFT-0YJK-ZHKB"
        
        
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self,selector: #selector(reloadTableView(notification:)),name: NSNotification.Name(rawValue: "tiradeTableReloded"), object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(tiradecirclebtnClicked(notification:)),name: NSNotification.Name(rawValue: "tiradecirclebtnClicked"), object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(leftCellClicked(notification:)),name: NSNotification.Name(rawValue: "leftCellClicked"), object: nil)
        
        // makeUrlRequest()
        
    }
    
    func getNotificationData()  {
        //  if formData.category != nil {
        let dataDict = ["user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
        let url = BASEURL + POSTGETNOTIFICATION
        ServerRequest.doPostDataRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) { (responseData) in
            
            print(responseData)
            do {
                let debateNoti : [DebateNotification] = try JSONDecoder().decode([DebateNotification].self, from: responseData)
                let arr = debateNoti
                print(arr)
                
                DispatchQueue.main.async {
                    if arr.count > 0 {
                        self.lblNotification.isHidden = false
                        self.lblNotification.text = "\(arr.count)"
                        self.arrNotification = arr
                    }
                }
            }catch  {
                return
            }
        }
        
        //        }else {
        //         //   DataUtils.showAlert(title: nil, msg: "", selfObj: , completionBlock: )
        //        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        makeUrlRequestForNewsFeed()
        makeUrlRequest()
    }
    
    private func createVideoThumbnail(from url: URL,imageWidth:CGFloat,imageHeight : CGFloat) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.maximumSize = CGSize(width: imageWidth, height: imageHeight)
        
        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 1)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func makeUrlRequest()  {
        let url = BASEURL + TRASHGETHOMEDATA
        
        ServerRequest.doGetRequestToServer(url, onController: self) { (responseData) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
            }
            do {
                let trashArr : [String : [TrashHome]]  = try! JSONDecoder().decode([String: [TrashHome]].self, from: responseData)
                self.trashData = trashArr["data"]
                if self.trashData != nil {
                    DispatchQueue.main.async {
                        for loopForCat in 0..<self.trashData!.count {
                            self.trashData?[loopForCat].selectedCatType = "Random"
                        }
                        for image in self.trashData! {
                            if image.user_id == TiradeUserDefaults.getUser_ID(key: "user_id") {
                                if image.user_profile != nil && image.user_profile != "" {
                                    self.imgMember.kf.setImage(with: URL(string: image.user_profile!)!)
                                }
                                break
                            }
                        }
                    }
                }
                self.trashData = self.trashData?.sorted(by: { (obj1, obj2) -> Bool in
                    Int(obj1.post_id!)! > Int(obj2.post_id!)!
                })
                TiradeSingleTon.shared.arrComments = self.trashData!
                self.orignalTrashData = trashArr["data"]
                self.isEnableLeftFilter = false
                DispatchQueue.main.async {
                    self.tblIncognito.reloadData()
                    self.getNotificationData()
                    
                }
            }catch  {
                return
            }
        }
    }
    
    func makeUrlRequestForNewsFeed()  {
        let url = BASEURL + TRASHGETNEWSFEED
        
        ServerRequest.doGetRequestToServer(url, onController: self) { (responseData) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
            }
            do {
                let trashArr : [String : [TrashHome]]  = try! JSONDecoder().decode([String: [TrashHome]].self, from: responseData)
                self.newsFeed = trashArr["data"]
                DispatchQueue.main.async {
                    self.collVWNewsFeed.reloadData()
                }
            }catch  {
                return
            }
        }
    }
    
    @objc func leftCellClicked(notification :  NSNotification) {
        guard let selectedTitle : String = notification.userInfo!["selectedTitle"] as! String else { return }
        
        if selectedTitle == "All Posts" {
            makeUrlRequest()
            isEnableLeftFilter = false
        }else {
            if isLocalSearch{
                self.trashData = self.trashData?.filter({ (data) -> Bool in
                    
                    data.name?.lowercased() == selectedTitle.lowercased() ||  data.category?.lowercased() == selectedTitle.lowercased() ||  data.post_type?.lowercased() == selectedTitle.lowercased() || data.selectedCatType?.lowercased() == selectedTitle.lowercased()
 
                })
             
            }else {
                isEnableLeftFilter = true

                self.trashData = self.trashData?.filter({ (data) -> Bool in
                    
                    data.category == selectedTitle
                    
                })
            }
        }
        TiradeSingleTon.shared.arrComments = self.trashData!
        tblIncognito.reloadData()
        if selectedTitle == "" {
          self.trashData = self.orignalTrashData
            tblIncognito.reloadData()
        }
       // self.trashData = self.orignalTrashData
    }
        
    @objc private func reloadTableView(notification: NSNotification){
        makeUrlRequest()
    }
    
    @objc private func tiradecirclebtnClicked(notification: NSNotification){
        let goSelection = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
        self.navigationController?.pushViewController(goSelection, animated: false)
    }
    
    @IBAction func drawerIconTapped(_ sender: UIButton) {
        let viewController22 = LeftSideViewController(nibName: "LeftSideViewController", bundle: nil)
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: viewController22)
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        actionButton.items.removeAll()
        actionButton.removeFromSuperview()
        actionButton.isHidden = true
        btnFloating.isSelected = false
        searchBarHeightCons.constant = 0
        searchBar.text = ""
        isLocalSearch = false
        self.trashData = self.orignalTrashData
        tblIncognito.reloadData()
        self.view.endEditing(true)
    }
    
    @IBAction func btnFloatingAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            actionButton.tintColor = UIColor.white
            setFloatingMenu(actionButton: actionButton)
            actionButton.isHidden = false
            
            
        }else {
            actionButton.items.removeAll()
            actionButton.removeFromSuperview()
            actionButton.isHidden = true
            
        }
    }
    
    func setFloatingMenu(actionButton : JJFloatingActionButton)  {
        actionButton.addItem(title: "", image: nil) { item in
            self.navigationController?.popViewController(animated: false)
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: UIImage(named: "goLiveIcon")?.withRenderingMode(.alwaysOriginal)) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        actionButton.addItem(title: "", image: nil) { item in
            // do something
        }
        
        
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.itemSizeRatio = CGFloat(1)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .trailing
            if item.imageView.image == nil {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .clear
                item.buttonColor = .clear
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }else {
                item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
                item.titleLabel.textColor = .white
                item.buttonColor = .white
                item.buttonImageColor = .red
                
                item.layer.shadowColor = UIColor.black.cgColor
                item.layer.shadowOffset = CGSize(width: 0, height: 1)
                item.layer.shadowOpacity = Float(0.4)
                item.layer.shadowRadius = CGFloat(2)
            }
        }
        
        
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        //  actionButton.bottomAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        //actionButton.trailingAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        
        actionButton.handleSingleActionDirectly = true
        actionButton.buttonDiameter = btnFloating.frame.size.width
        
        actionButton.overlayView.backgroundColor = UIColor(white: 1, alpha: 0.1)
        actionButton.buttonImageSize = CGSize(width: 30, height: 30)
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 180)
        
        //        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "X")!)
        //       actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        
        //       actionButton.layer.shadowColor = UIColor.black.cgColor
        //       actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        //       actionButton.layer.shadowOpacity = Float(0.4)
        //       actionButton.layer.shadowRadius = CGFloat(2)
        
    }
    
    
    @IBAction func btnHomeAction(_ sender: UIButton) {
        
    }
    
    @IBAction func btnLogOutAction(_ sender: UIButton) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        TiradeUserDefaults.setUser_ID(value: "noid")
        
        let chooseAcc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
        self.navigationController?.pushViewController(chooseAcc, animated: false)
        
        
    }
    
    @IBAction func btnSettingAction(_ sender: UIButton) {
        let settingVC = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(settingVC, animated: false)
        
        
    }
    @IBAction func btnNotificationAction(_ sender: UIButton) {
        let notification = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        notification.debateNoti = self.arrNotification
        self.navigationController?.pushViewController(notification, animated: false)
    }
    @IBAction func btnMoreAction(_ sender: UIButton) {
        commingSoon()
    }
    func commingSoon()  {
        let commingSoonVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommingSoonVC") as! CommingSoonVC
        self.navigationController?.present(commingSoonVC, animated: true, completion: {
        })
    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {
       searchBarHeightCons.constant = 56
        searchBar.delegate = self
    }
    
    
    
    func getLocalNotificationOnDebate()  {
        sendNotification(title: "Hello", subtitle: "Hi", body: "Hello Notification", badge: 1, delayInterval: 5)
    }
    
    func sendNotification(title: String, subtitle: String, body: String, badge: Int?, delayInterval: Int?) {
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = title
        notificationContent.subtitle = subtitle
        notificationContent.body = body
        
        var delayTimeTrigger: UNTimeIntervalNotificationTrigger?
        
        if let delayInterval = delayInterval {
            delayTimeTrigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(delayInterval), repeats: false)
        }
        
        if let badge = badge {
            var currentBadgeCount = UIApplication.shared.applicationIconBadgeNumber
            currentBadgeCount += badge
            notificationContent.badge = NSNumber(integerLiteral: currentBadgeCount)
        }
        
        notificationContent.sound = UNNotificationSound.default
        
        UNUserNotificationCenter.current().delegate = self
        
        let request = UNNotificationRequest(identifier: "TestLocalNotificationnS", content: notificationContent, trigger: delayTimeTrigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
        
    }
}

@available(iOS 11.0, *)
extension IncognitoHomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if trashData?.count == 0 {
            lblNoDataFound.isHidden = false
        }else {
            lblNoDataFound.isHidden = true
        }
        return trashData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tblCell = tblIncognito.dequeueReusableCell(withIdentifier: "IncognitoTblCell", for: indexPath) as! IncognitoTblCell
        if ((self.trashData?[indexPath.row].s_agree!.count)! > 10) {
            tblCell.btnDropArrow.setTitle("Top Spot", for: .normal)
            tblCell.btnDropArrow.titleLabel?.textColor = UIColor.green
            self.trashData?[indexPath.row].selectedCatType = "Top Spot"
        }else if ((self.trashData?[indexPath.row].s_agree!.count)! < 10) {
            tblCell.btnDropArrow.titleLabel?.textColor = UIColor.yellow

            tblCell.btnDropArrow.setTitle("Random", for: .normal)

            self.trashData?[indexPath.row].selectedCatType = "Random"

        }else if ((self.trashData?[indexPath.row].s_disagree!.count)! > 10) {
            tblCell.btnDropArrow.setTitle("Most Hated", for: .normal)
            tblCell.btnDropArrow.titleLabel?.textColor = UIColor.red

            self.trashData?[indexPath.row].selectedCatType = "Most Hated"

        }else if ((self.trashData?[indexPath.row].user_id! ?? "0")! == TiradeUserDefaults.getUser_ID(key: "user_id")) {
            tblCell.btnDropArrow.setTitle("Local", for: .normal)
            tblCell.btnDropArrow.titleLabel?.textColor = UIColor.blue
            self.trashData?[indexPath.row].selectedCatType = "Local"

        }else if ((self.trashData?[indexPath.row].cheers!.count)! > 10) {
            tblCell.btnDropArrow.titleLabel?.textColor = UIColor.darkGray

            self.trashData?[indexPath.row].selectedCatType = "Much Props"

            tblCell.btnDropArrow.setTitle("Much Props", for: .normal)
        }
        tblCell.btnPlayActions.tag = indexPath.row
        tblCell.lblUserTitle.text = trashData?[indexPath.row].name
        tblCell.lblCategoryTitle.text = trashData?[indexPath.row].category
        tblCell.lblSubCategoryTitle.text = trashData?[indexPath.row].post_type
        
        if trashData?[indexPath.row].post_type?.lowercased() == "trash".lowercased() {
            tblCell.imgHeading.image = UIImage(named: "trashform")
        }else if trashData?[indexPath.row].post_type?.lowercased() == "debate".lowercased() {
            tblCell.imgHeading.image = UIImage(named: "debateform")
        }else if trashData?[indexPath.row].post_type?.lowercased() == "Whistle Blower".lowercased() {
            tblCell.imgHeading.image = UIImage(named: INCOGNITOCALLOUTIMGFORM)
        }else if trashData?[indexPath.row].post_type?.lowercased() == "jokes".lowercased() {
            tblCell.imgHeading.image = UIImage(named: "jokesform")
        }
        if (trashData?[indexPath.row].user_profile) == "" {
            tblCell.ImgUser.image = UIImage(named: "user_profile_img")
        }else {
            tblCell.ImgUser.kf.setImage(with: URL(string: (self.trashData?[indexPath.row].user_profile)!)!)
            tblCell.ImgUser.image?.imageResize(sizeChange: tblCell.ImgUser!.frame.size)
        }
        
        if (trashData?[indexPath.row].image_path) != "" {
            tblCell.postImg.kf.setImage(with: URL(string: (self.trashData?[indexPath.row].image_path)!)!)
            tblCell.postImg.image?.imageResize(sizeChange: tblCell.postImg!.frame.size)
            showHideFilesIcon(vwBorder: tblCell.vwVideo, vwCenterLine: tblCell.vwCenterLine, imgFile: tblCell.imgFile, lblFTitleile: tblCell.lblFileName, lblFileName: tblCell.lblFileTitle, isHidden: true)
            tblCell.constraintHeightVwPost.constant = 180
        }else {
            tblCell.constraintHeightVwPost.constant = 100

        }
        
        if (trashData?[indexPath.row].video_path) != "" {
            showHideFilesIcon(vwBorder: tblCell.vwVideo, vwCenterLine: tblCell.vwCenterLine, imgFile: tblCell.imgFile, lblFTitleile: tblCell.lblFileName, lblFileName: tblCell.lblFileTitle, isHidden: false)
            tblCell.imgFile.image = UIImage(named: "video.png")
            let theFileName = ((self.trashData?[indexPath.row].video_path)! as NSString).lastPathComponent
            tblCell.lblFileName.text = theFileName
            tblCell.lblFileTitle.text = "Video"
        }
        
        if (trashData?[indexPath.row].audio_path) != "" {
            showHideFilesIcon(vwBorder: tblCell.vwVideo, vwCenterLine: tblCell.vwCenterLine, imgFile: tblCell.imgFile, lblFTitleile: tblCell.lblFileName, lblFileName: tblCell.lblFileTitle, isHidden: false)
            tblCell.imgFile.image = UIImage(named: "voice.png")
            let theFileName = ((self.trashData?[indexPath.row].audio_path)! as NSString).lastPathComponent
            tblCell.lblFileName.text = theFileName
            tblCell.lblFileTitle.text = "Voice"
            
        }
        if (trashData?[indexPath.row].pdf_path) != "" {
            showHideFilesIcon(vwBorder: tblCell.vwVideo, vwCenterLine: tblCell.vwCenterLine, imgFile: tblCell.imgFile, lblFTitleile: tblCell.lblFileName, lblFileName: tblCell.lblFileTitle, isHidden: false)
            
            tblCell.imgFile.image = UIImage(named: "pdf.png")
            let theFileName = ((self.trashData?[indexPath.row].pdf_path)! as NSString).lastPathComponent
            tblCell.lblFileName.text = theFileName
            tblCell.lblFileTitle.text = "PDF"
        }
        if (trashData?[indexPath.row].txtfile_path) != "" {
            showHideFilesIcon(vwBorder: tblCell.vwVideo, vwCenterLine: tblCell.vwCenterLine, imgFile: tblCell.imgFile, lblFTitleile: tblCell.lblFileName, lblFileName: tblCell.lblFileTitle, isHidden: false)
            
            tblCell.imgFile.image = UIImage(named: "text.png")
            let theFileName = ((self.trashData?[indexPath.row].txtfile_path)! as NSString).lastPathComponent
            tblCell.lblFileName.text = theFileName
            tblCell.lblFileTitle.text = "TXT"
        }
        let input = trashData?[indexPath.row].created_date
        let split = input?.components(separatedBy: " ")
        
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: split?[0] ?? "2020/09/09")
        formatter.dateFormat = "MMM dd, yyyy"
        let resultString = formatter.string(from: date!)
        print(resultString)
        tblCell.lblDate.text = "\(resultString) \(split?[1] ?? "00:00")"

        
        tblCell.lblContent.text = trashData?[indexPath.row].post_content
        if isEnableLeftFilter {
            tblCell.trashAction = TiradeSingleTon.shared.arrComments[indexPath.row]
        }else {
            tblCell.trashAction = trashData?[indexPath.row]
        }
        
        tblCell.selfObj = self
        tblCell.collVwPlusAction.tag = indexPath.row
        
        tblCell.vwOnThreeDots.isHidden = true
        tblCell.vwOnDropArrow.isHidden = true
        
        tblCell.vwBasePlusIcons.isHidden = true
        tblCell.constraintWidthMenu.constant = 0
        
        tblCell.btnPlusIcon.tag = indexPath.row
        tblCell.btnThreeDots.tag = indexPath.row
        tblCell.btnDropArrow.tag = indexPath.row
        
        tblCell.btnPlusIcon.isSelected = false
        tblCell.btnThreeDots.isSelected = false
        tblCell.btnDropArrow.isSelected = false
        
        
        tblCell.constraintHeightThreeDotVw.constant = 0
        tblCell.btnPlusIcon.addTarget(self, action: #selector(btnPlusAction), for: .touchUpInside)
        tblCell.btnThreeDots.addTarget(self, action: #selector(btnThreeDotAction), for: .touchUpInside)
        tblCell.btnDropArrow.addTarget(self, action: #selector(btnDropArrowAction(sender:)), for: .touchUpInside)
        tblCell.collVwPlusAction.reloadData()
        tblCell.btnPlayActions.addTarget(self, action: #selector(allActionOnUplodedFiles(sender:)), for: .touchUpInside)
        tblCell.bntFBAction.tag = indexPath.row
        tblCell.bntInstaAction.tag = indexPath.row
        tblCell.bntSnapChatAction.tag = indexPath.row
        tblCell.bntTicTocAction.tag = indexPath.row
        tblCell.bntTwtAction.tag = indexPath.row
        
        tblCell.bntFBAction.addTarget(self, action: #selector(bntFBAction(sender:)), for: .touchUpInside)
        tblCell.bntInstaAction.addTarget(self, action: #selector(bntInstaAction(sender:)), for: .touchUpInside)
        tblCell.bntSnapChatAction.addTarget(self, action: #selector(bntSnapChatAction(sender:)), for: .touchUpInside)
        tblCell.bntTicTocAction.addTarget(self, action: #selector(bntTicTocAction(sender:)), for: .touchUpInside)
        tblCell.bntTwtAction.addTarget(self, action: #selector(bntTwtAction(sender:)), for: .touchUpInside)
        
        return tblCell
        
    }
    
    func showHideFilesIcon(vwBorder:UIView,vwCenterLine : UIView,imgFile : UIImageView,lblFTitleile : UILabel,lblFileName : UILabel,isHidden : Bool)  {
        vwCenterLine.isHidden = isHidden
        imgFile.isHidden = isHidden
        lblFTitleile.isHidden = isHidden
        lblFileName.isHidden = isHidden
        if lblFileName.isHidden {
            vwBorder.layer.borderWidth = 0.0
        }else{
            vwBorder.layer.borderWidth = 0.5
        }
        
    }
    
    @objc func bntFBAction(sender : UIButton)  {
        shareOnSocial(sender: sender)
    }
    @objc func bntInstaAction(sender : UIButton)  {
        shareOnSocial(sender: sender)
        
    }
    @objc func bntSnapChatAction(sender : UIButton)  {
        shareOnSocial(sender: sender)
        
    }
    @objc func bntTicTocAction(sender : UIButton)  {
        shareOnSocial(sender: sender)
        
    }
    @objc func bntTwtAction(sender : UIButton)  {
        shareOnSocial(sender: sender)
    }
    
    func shareOnSocial(sender : UIButton) {
        var urlShare = String()
        if trashData?[sender.tag].video_path != "" {
            urlShare = (trashData?[sender.tag].video_path)!
            
        }else  if trashData?[sender.tag].audio_path != "" {
            urlShare = (trashData?[sender.tag].audio_path)!
            
        }else  if trashData?[sender.tag].image_path != "" {
            urlShare = (trashData?[sender.tag].image_path)!
            
        }else  if trashData?[sender.tag].pdf_path != "" {
            urlShare = (trashData?[sender.tag].pdf_path)!
            
        }else  if trashData?[sender.tag].txtfile_path != "" {
            urlShare = (trashData?[sender.tag].txtfile_path)!
            
        }else {
            print("nothing selected")
        }
        let shareAll = [urlShare,self.imgMember,self.lblMemberID] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.addToReadingList,UIActivity.ActivityType.mail,UIActivity.ActivityType.message,UIActivity.ActivityType.airDrop,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.print,UIActivity.ActivityType.markupAsPDF,UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
                                                        UIActivity.ActivityType(rawValue: "net.whatsapp.WhatsApp.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "pinterest.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.facebook.Messenger.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.fogcreek.trello.trelloshare"),
                                                        UIActivity.ActivityType(rawValue: "com.linkedin.LinkedIn.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.hammerandchisel.discord.Share"),
                                                        UIActivity.ActivityType(rawValue: "com.google.Gmail.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.google.inbox.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.riffsy.RiffsyKeyboard.RiffsyShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.google.hangouts.ShareExtension"),
                                                        UIActivity.ActivityType(rawValue: "com.ifttt.ifttt.share"),    UIActivity.ActivityType(rawValue: "com.burbn.instagram.shareextension")
]
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func uploadVideoOnFacebook() {
        
    }
    
    func calculateHeight(inString:String) -> CGFloat
    {
        let messageString = inString
        
        //        let lineattribute : [NSAttributedString.Key : Any] = [
        //            ..UIFont : UIColor(named: "#0f88b7ff")!,
        //            .underlineStyle : NSUnderlineStyle.single.rawValue
        //        ]
        
        
        let attributes : [NSAttributedString.Key : Any] = [.font : UIFont.systemFont(ofSize: 15.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: 222.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        return requredSize.height
    }
    
    @objc func allActionOnUplodedFiles(sender : UIButton)  {
        if trashData?[sender.tag].video_path != "" {
            playVideoUrl(url:(trashData?[sender.tag].video_path)!)
            
        }else  if trashData?[sender.tag].audio_path != "" {
            playAudioUrl(url:(trashData?[sender.tag].audio_path)!)
            
        }else  if trashData?[sender.tag].image_path != "" {
            openPhotos(url:(trashData?[sender.tag].image_path)!)
            
        }else  if trashData?[sender.tag].pdf_path != "" {
            openPDF(url:(trashData?[sender.tag].pdf_path)!)
            
        }else  if trashData?[sender.tag].txtfile_path != "" {
            openTXTFile(url:(trashData?[sender.tag].txtfile_path)!)
            
        }else {
            print("nothing selected")
        }
    }
    
    func allActionNewsFeedOnUplodedFiles(indexPath : Int)  {
           if newsFeed?[indexPath].video_path != "" {
               playVideoUrl(url:(newsFeed?[indexPath].video_path)!)
               
           }else  if newsFeed?[indexPath].audio_path != "" {
               playAudioUrl(url:(newsFeed?[indexPath].audio_path)!)
               
           }else  if newsFeed?[indexPath].image_path != "" {
               openPhotos(url:(newsFeed?[indexPath].image_path)!)
               
           }else  if newsFeed?[indexPath].pdf_path != "" {
               openPDF(url:(newsFeed?[indexPath].pdf_path)!)
               
           }else  if newsFeed?[indexPath].txtfile_path != "" {
               openTXTFile(url:(newsFeed?[indexPath].txtfile_path)!)
               
           }else {
               print("nothing selected")
           }
       }
    
    
    func playVideoUrl(url : String) {
        var url : NSString = url as NSString
        var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        var searchURL : NSURL = NSURL(string: urlStr as String)!
        
        let videoURL = searchURL
        let player = AVPlayer(url: videoURL as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    
    
    func playAudioUrl(url : String)  {
        
        let audioVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "PlayAudioVC") as! PlayAudioVC
        audioVC.playUrl = url
        self.navigationController?.present(audioVC, animated: true, completion: nil)
        
        
        
        
        
        //        let videoURL = searchURL
        //        let player = AVPlayer(url: videoURL as URL)
        //        let playerViewController = AVPlayerViewController()
        //        playerViewController.player = player
        //        self.present(playerViewController, animated: true) {
        //            playerViewController.player!.play()
        //        }
        
    }
    
    func openPDF(url : String)  {
        let pdfvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PDFOpenVC") as! PDFOpenVC
        pdfvc.pdfURL = url
        self.navigationController?.present(pdfvc, animated: true, completion: nil)
        
    }
    
    func openTXTFile(url : String)  {
        let rtfVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OpenRTFfileVC") as! OpenRTFfileVC
        rtfVC.rtfURL = url
        self.navigationController?.present(rtfVC, animated: true, completion: nil)
        
        
    }
    
    func openPhotos(url : String)  {
        let rtfVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OpenPhotosVC") as! OpenPhotosVC
        rtfVC.imageUrl = url
        self.navigationController?.present(rtfVC, animated: true, completion: nil)
        
    }
    
    
    
    @objc func btnPlusAction(sender : UIButton)  {
        sender.isSelected = !sender.isSelected
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblIncognito.cellForRow(at: myIndexPath as IndexPath) as! IncognitoTblCell
        if sender.isSelected {
            UIView.animate(withDuration: 0.2) {
                cell.vwBasePlusIcons.isHidden = false
                cell.constraintWidthMenu.constant =  cell.vwBaseCell.frame.size.width - 10
                self.view.layoutIfNeeded()
            }
        }else {
            cell.constraintWidthMenu.constant =  0
        }
    }
    
    @objc func btnThreeDotAction(sender : UIButton)  {
        sender.isSelected = !sender.isSelected
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblIncognito.cellForRow(at: myIndexPath as IndexPath) as! IncognitoTblCell
        if sender.isSelected {
            UIView.animate(withDuration: 0.2) {
                cell.vwOnThreeDots.isHidden = false
                cell.constraintHeightThreeDotVw.constant = 135
                self.view.layoutIfNeeded()
            }
        }else {
            cell.constraintHeightThreeDotVw.constant = 0
            cell.vwOnThreeDots.isHidden = true
        }
    }
    
    @objc func btnDropArrowAction(sender : UIButton)  {
        sender.isSelected = !sender.isSelected
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tblIncognito.cellForRow(at: myIndexPath as IndexPath) as! IncognitoTblCell
        if sender.isSelected {
            UIView.animate(withDuration: 0.2) {
                cell.vwOnDropArrow.isHidden = false
                cell.constraintHeightDropArrow.constant = 135
                self.view.layoutIfNeeded()
            }
        }else {
            cell.constraintHeightDropArrow.constant = 0
        }
        
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        let forumVC = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "IncognitoHomeDetailVC") as! IncognitoHomeDetailVC
            forumVC.tiradeDetails = trashData![indexPath.row]
        self.navigationController?.pushViewController(forumVC, animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let msgArr = trashData?[indexPath.row].post_content
        let height:CGFloat = self.calculateHeight(inString: msgArr!)
        return 255 + height
        // return 200
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 101 {
            return self.newsFeed?.count ?? 0
        }else {
            return collLbldata.count
        }
    }
    
    @objc func timerNewsFeed(theTimer : Timer)  {
        let numberOfNewsFeed : Int = self.newsFeed?.count ?? 0
        if rowIndex < numberOfNewsFeed - 1 {
            rowIndex = (rowIndex + 1)
        }else {
            rowIndex = 0
        }
        print(self.rowIndex)
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            
            self.collVWNewsFeed.scrollToItem(at: IndexPath(item: self.rowIndex, section: 0), at: .centeredHorizontally, animated: true)
            
        }) { (boolValue) in
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 101 {
            
            let newsFeedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsFeedCollCell", for: indexPath) as! NewsFeedCollCell
            newsFeedCell.imgVideoFile.isHidden = true

            if (newsFeed?[indexPath.row].user_profile) == "" {
                newsFeedCell.imgUser.image = UIImage(named: "user_profile_img")
            }else {
                newsFeedCell.imgUser.kf.setImage(with: URL(string: (self.newsFeed?[indexPath.row].user_profile)!)!)
            }
            let input = newsFeed?[indexPath.row].created_date
            let split = input?.components(separatedBy: " ")
            
            var formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: split?[0] ?? "2020/09/09")
            formatter.dateFormat = "MMM dd, yyyy"
            let resultString = formatter.string(from: date!)
            print(resultString)
            newsFeedCell.lblDate.text = "\(resultString) \(split?[1] ?? "00:00")"
            newsFeedCell.lblContent.text = newsFeed?[indexPath.row].post_content
            newsFeedCell.lblUserTitle.text = newsFeed?[indexPath.row].name
            if (newsFeed?[indexPath.row].image_path) != "" {
                newsFeedCell.postImg.kf.setImage(with: URL(string: (self.newsFeed?[indexPath.row].image_path)!)!)
            }else {
                
            }

            if (newsFeed?[indexPath.row].video_path) != "" {
                newsFeedCell.imgVideoFile.isHidden = false
                newsFeedCell.imgVideoFile.image = UIImage(named: "video-icon")
            }
            
            if (newsFeed?[indexPath.row].audio_path) != "" {
                
                newsFeedCell.postImg.image = UIImage(named: "newsFeedMusic")

            }
            if (newsFeed?[indexPath.row].pdf_path) != "" {
                newsFeedCell.imgVideoFile.isHidden = false
                newsFeedCell.imgVideoFile.image = UIImage(named: "fileIcon")
            }
            
            return newsFeedCell
        }
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "IncognitoHomeCollCell", for: indexPath) as! IncognitoHomeCollCell
        collectionCell.lblDescription.text = collLbldata[indexPath.row]
        collectionCell.imgCircle.image = UIImage(named: collLblimg[indexPath.row])
        if indexPath.row == 0 {
            collectionCell.btnAwardsCount.isHidden = false
            
        }else {
            collectionCell.btnAwardsCount.isHidden = true
        }
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 101 {
            return CGSize(width: collectionView.frame.size.width - 8, height: 200)
        }
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 101 {
            self.allActionNewsFeedOnUplodedFiles(indexPath: indexPath.row)
        }else {
            if collLbldata[indexPath.row] == INCOGNITOFORUMS {
                let forumVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FourmFormQuestionVC") as! FourmFormQuestionVC
                self.navigationController?.pushViewController(forumVC, animated: false)
            }else if collLbldata[indexPath.row] == INCOGNITOSHAME {
                commingSoon()
            }else if collLbldata[indexPath.row] == INCOGNITOSHOUTOUT || collLbldata[indexPath.row] == INCOGNITOREPRESENT {
                if indexPath.row == 0 || indexPath.row == 1 {
                    let newpost = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "PostPageVC") as! PostPageVC
                    newpost.selectedIndexPath = indexPath.row
                    self.navigationController?.present(newpost, animated: true, completion: {
                    })
                }
            } else {
                let incognitoFormVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IncognitoFormVC") as! IncognitoFormVC
                incognitoFormVC.selectedPage = collLbldata[indexPath.row]
                incognitoFormVC.selectedImage = collLblimg[indexPath.row]
                incognitoFormVC.dataOfInvite = orignalTrashData
                if incognitoFormVC.selectedPage != INCOGNITOSHAME {
                    self.navigationController?.pushViewController(incognitoFormVC, animated: true)
                    //  self.navigationController?.present(incognitoFormVC, animated: false, completion: nil)
                }
            }
        }
    }
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }

    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: UITableView.self) {
            if self.lastContentOffset < scrollView.contentOffset.y {
                // did move up
                UIView.animate(withDuration: 0.3) {
                    self.newFeedHeightCons.constant = 10
                    self.view.layoutIfNeeded()
                }
            } else if self.lastContentOffset > scrollView.contentOffset.y {
                // did move down
                UIView.animate(withDuration: 0.8) {
                    self.newFeedHeightCons.constant = 240
                    self.view.layoutIfNeeded()
                }
            } else {
                // didn't move
            }
        }
    }
}

extension IncognitoHomeVC: UISearchBarDelegate,UITextFieldDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      isLocalSearch = true
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        textFieldInsideSearchBar!.becomeFirstResponder()

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.trashData = self.orignalTrashData
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leftCellClicked"), object: nil, userInfo: ["selectedTitle":searchBar.text!])
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        isLocalSearch = false
        searchBarHeightCons.constant = 0
    }
   
}


