//
//  IncognitoCollPlusCell.swift
//  Tirade
//
//  Created by admin on 01/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class IncognitoCollPlusCell: UICollectionViewCell {
    
    @IBOutlet weak var vwBase: UIView!
    @IBOutlet weak var imgCircle: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCount: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCircle.layer.cornerRadius = imgCircle.frame.size.height/2
        imgCircle.layer.masksToBounds = true
    }
}
