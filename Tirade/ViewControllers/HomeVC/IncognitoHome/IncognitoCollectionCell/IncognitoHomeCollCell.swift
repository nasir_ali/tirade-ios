//
//  IncognitoHomeCollCell.swift
//  Tirade
//
//  Created by admin on 27/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class IncognitoHomeCollCell: UICollectionViewCell {
    @IBOutlet weak var vwBase: UIView!
    @IBOutlet weak var imgCircle: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnAwardsCount: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnAwardsCount.layer.cornerRadius = btnAwardsCount.frame.size.height/2
        lblDescription.layer.cornerRadius = lblDescription.frame.size.height/2
    }
}
