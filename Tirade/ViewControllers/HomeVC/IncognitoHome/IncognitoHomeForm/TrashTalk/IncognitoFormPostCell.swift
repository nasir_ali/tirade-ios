//
//  IncognitoFormPostCell.swift
//  Tirade
//
//  Created by admin on 20/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import IQKeyboardManager

class IncognitoFormPostCell: UITableViewCell {

    @IBOutlet weak var txtVwPost : IQTextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        txtVwPost.placeholder = " Typehere.."
        txtVwPost.placeholderColor = UIColor.white
        DataUtils.addBorder(view: txtVwPost, cornerRadius: 14, borderWidth: 2, borderColor: DataUtils.hexStringToUIColor(hex: "#D4AF37"))

    }

}
