//
//  IncognitoFormCell.swift
//  Tirade
//
//  Created by admin on 29/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class IncognitoFormCell: UITableViewCell {

    @IBOutlet weak var txtFldForm: TextField!
    @IBOutlet weak var lblHeader: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        DataUtils.addBorder(view: txtFldForm, cornerRadius: 14, borderWidth: 2, borderColor: DataUtils.hexStringToUIColor(hex: "#D4AF37"))
        txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Typehere...",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}
