//
//  IncognitoFormVC.swift
//  Tirade
//
//  Created by admin on 29/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import UITextView_Placeholder
import EzPopup
import IQKeyboardManager

class IncognitoFormVC: TiradeBaseVC,NumberPickerViewControllerDelegate {
    
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var vwSocial: UIView!
    @IBOutlet weak var tblForm: UITableView!
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var imgTopBack: UIImageView!
    @IBOutlet weak var imgTopImg: UIImageView!
    @IBOutlet weak var imgTopTxt: UIImageView!
    @IBOutlet weak var imgTopImgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgTopImgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgTopImgTopConstraint: NSLayoutConstraint!
    var imagePicker: ImagePicker!
    var formData = TrashCategory()
    var dataOfInvite : [TrashHome]?
    
    
    let pickerVC = NumberPickerViewController.instantiate()
    
    
    var selectedPage = INCOGNITOTRASH
    var selectedImage = INCOGNITOTRASHIMG
    
    var arrTrashHeader = ["Category ?","Who are you Trashing ?","remove","How are you Trashing ?","Post"]
    var arrDebateHeader = ["Category ?","Who are you Debating ?","What are you Debating ?","How are you Debating ?","Post"]
    var arrCalloutHeader = ["Category ?","Who are you blowing the whistle on ?","What are you blowing the whistle on ?","How are you whistle blowing ?", "Post"]
    var arrJokeHeader = ["Category ?","How are you Joking ?","Post"]
    var arrForumsHeader = ["How are you trashing ?","What are you trashing ?","Where are you trashing ?","When are you trashing ?","Why are you trashing ?"]
    var arrShameHeader = ["How are you trashing ?","What are you trashing ?","Where are you trashing ?","When are you trashing ?","Why are you trashing ?"]
    
    
    //  let v = UIView(frame: window.bounds)
    //            window.addSubview(v);
    //            v.backgroundColor = UIColor.clear
    var categoryVw : UIView!
    
    var textViewDebate : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")
        
        textViewDebate = UITextView(frame: CGRect.zero)
        self.navigationController?.isNavigationBarHidden = true
        
        
        switch selectedPage {
        case INCOGNITOTRASH:
            btnInvite.isHidden = true
            setUI(imgBack: INCOGNITOBACKTRASHIMG, imgTxt: INCOGNITOTXTTRASHIMG, imgTop: INCOGNITOTRASHIMGFORM)
        case INCOGNITODEBATE:
            btnInvite.isHidden = false
            setUI(imgBack: INCOGNITOBACKDEBATEIMG, imgTxt: INCOGNITOTXTDEBATEIMG, imgTop: INCOGNITODEBATEIMGFORM)
        case INCOGNITOCALLOUT:
            btnInvite.isHidden = true
            setUI(imgBack: INCOGNITOBACKCALLOUTIMG, imgTxt: INCOGNITOTXTCALLOUTIMG, imgTop: INCOGNITOCALLOUTIMGFORM)
        case INCOGNITOJOKES:
            btnInvite.isHidden = true
            setUI(imgBack: INCOGNITOBACKJOKESIMG, imgTxt: INCOGNITOTXTJOKESIMG, imgTop: INCOGNITOJOKESIMGFORM)
        case INCOGNITOFORUMS:
            btnInvite.isHidden = true
            setUI(imgBack: INCOGNITOBACKFORUMSIMG, imgTxt: INCOGNITOTXTFORUMSIMG, imgTop: INCOGNITOFORUMSIMGFORM)
        case INCOGNITOSHAME:
            btnInvite.isHidden = true
            setUI(imgBack: INCOGNITOBACKSHAMEIMG, imgTxt: INCOGNITOTXTSHAMEIMG, imgTop: INCOGNITOSHAMEIMGFORM)
        default:
            btnInvite.isHidden = true
            setUI(imgBack: INCOGNITOBACKTRASHIMG, imgTxt: INCOGNITOTXTTRASHIMG, imgTop: INCOGNITOTRASHIMGFORM)
        }
        
        DataUtils.addBorder(view: txtCategory, cornerRadius: 14, borderWidth: 2, borderColor: DataUtils.hexStringToUIColor(hex: "#D4AF37"))
        tblForm.register(UINib(nibName: "CategorySelectionCell", bundle: nil), forCellReuseIdentifier: "CategorySelectionCell")
        vwSocial?.backgroundColor = UIColor(white: 1, alpha: 0.3)
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                if topController.isKind(of: UIAlertController.self){
                    topController.view.frame.origin.y -= 50
                }
                
                // topController should now be your topmost view controller
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            if topController.isKind(of: UIAlertController.self){
                topController.view.frame.origin.y = 0
            }
            
            // topController should now be your topmost view controller
        }         }
    
    
    func numberPickerViewController(sender: NumberPickerViewController, didSelectNumber selectedIDs: [String]) {
        dismiss(animated: true) {
            self.showAlert(arrOfSelectID : selectedIDs)
        }
    }
    
    func sendInviteForDebate(arrOfInvite : [String])  {
        //  if formData.category != nil {
        let dataDict = ["sender_id" : TiradeUserDefaults.getUser_ID(key: "user_id")
            ,"invited_id":arrOfInvite,"debate_reason":textViewDebate.text] as [String : Any]
        let url = BASEURL + POSTSENDDEBATE
        ServerRequest.doPostDataRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) { (data) in
            print(data)
            
            
            DispatchQueue.main.async {
                self.showAlertWithDistructiveButton()
                print(data)
            }
        }
        
        //        }else {
        //         //   DataUtils.showAlert(title: nil, msg: "", selfObj: , completionBlock: )
        //        }
    }
    
    func showAlert(arrOfSelectID : [String]) {
        let alertController = UIAlertController(title: "Reason, why your debate should be accepted \n\n\n\n\n", message: "", preferredStyle: .alert)
        
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            self.dismiss(animated: true) {
                
            }           }))
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            
            self.dismiss(animated: true) {
                self.sendInviteForDebate(arrOfInvite: arrOfSelectID)
            }
            self.sendInviteForDebate(arrOfInvite: arrOfSelectID)
            
        }))
        
        
        
        //        let saveAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        //        saveAction.isEnabled = false
        //        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.view.addObserver(self, forKeyPath: "bounds", options: NSKeyValueObservingOptions.new, context: nil)
        NotificationCenter.default.addObserver(forName: UITextView.textDidChangeNotification, object: textViewDebate, queue: OperationQueue.main) { (notification) in
            // saveAction.isEnabled = self.textViewDebate.text != ""
        }
        textViewDebate.backgroundColor = UIColor.white
        textViewDebate.layer.cornerRadius = 10
        alertController.view.addSubview(self.textViewDebate)
        
        //        alertController.addAction(saveAction)
        //        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertWithDistructiveButton() {
        let alert = UIAlertController(title: "", message: "Invited Succesfully", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.dismiss(animated: true) {
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "bounds"{
            if let rect = (change?[NSKeyValueChangeKey.newKey] as? NSValue)?.cgRectValue{
                let margin:CGFloat = 60.0
                textViewDebate.frame = CGRect(x: rect.origin.x + 8.0, y: rect.origin.y + margin, width: rect.width - 2*8.0, height: rect.height / 2)
                
                //    textViewDebate.frame = CGRect(x: rect.origin.x + margin, y: rect.origin.y + margin, width: CGRect.width(rect) - 2*margin, height: CGRectGetHeight(rect) / 2)
                
            }
        }
    }
    
    @IBAction func showTopRightButtonTapped(_ sender: Any) {
        guard let pickerVC = pickerVC else { return }
        pickerVC.dataOfInvite = self.dataOfInvite
        pickerVC.delegate = self
        
        let popupVC = PopupViewController(contentController: pickerVC, position: .topRight(CGPoint(x: btnInvite.frame.origin.x - 220, y: btnInvite.frame.origin.y + 20)), popupWidth: 200, popupHeight: 300)
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 5
        present(popupVC, animated: true, completion: nil)
    }
    
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
        
        let fileData = FileTypeInfo()
        fileData.fileData = dictFile["fileData"] as! Data
        fileData.fileName = dictFile["fileName"] as! String
        fileData.uploadFileKey = dictFile["uploadFileKey"] as! String
        fileData.mimeType = dictFile["mimeType"] as! String
        
        var arr = [FileTypeInfo]()
        arr.append(fileData)
        
        formData.fileInfoType.append(fileData)
        print(formData.fileInfoType)
        //        formData.fileInfoType[1] = fileData
        //        formData.fileInfoType[2] = fileData
        //        formData.fileInfoType[3] = fileData
        
    }
    
    func setUI(imgBack : String,imgTxt : String, imgTop : String)  {
        
        imgTopBack.image = UIImage(named: imgBack)
        imgTopBack.image = UIImage(named: imgBack)
        imgTopBack.image = UIImage(named: imgBack)
        //  if selectedPage != INCOGNITOJOKES {
        imgTopTxt.image = UIImage(named: imgTxt)
        imgTopTxt.image = UIImage(named: imgTxt)
        imgTopTxt.image = UIImage(named: imgTxt)
        imgTopImgWidthConstraint.constant = 80
        imgTopImgHeightConstraint.constant = 80
        imgTopImgTopConstraint.constant = 0
        //        }else {
        //            imgTopImgWidthConstraint.constant = 110
        //            imgTopImgHeightConstraint.constant = 110
        //            imgTopImgTopConstraint.constant = 15
        //        }
        imgTopImg.image = UIImage(named: imgTop)
        imgTopImg.image = UIImage(named: imgTop)
        imgTopImg.image = UIImage(named: imgTop)
    }
    
    @IBAction func btnFaceBookAction(_ sender: UIButton) {
        
        
    }
    @IBAction func btnGmailBookAction(_ sender: UIButton) {
        
        
    }
    @IBAction func btnTwitterAction(_ sender: UIButton) {
    }
    @IBAction func btnWatsUpAction(_ sender: UIButton) {
        
    }
    @IBAction func btnContinueAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if formData.category != nil {
            if formData.post == "" {
                DataUtils.showAlert(title: "", msg: "please write a post", selfObj: self) {
                }
            }else {
                    let dataDict = ["user_id" :  TiradeUserDefaults.getUser_ID(key: "user_id"),
                                    "who":formData.text1,"what":formData.text2,"category":formData.selectedCategory,"post_content":formData.post,"post_type":formData.category] as [String : Any]
                    sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
                }
            }
        else {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + POSTCATEGORYDATA
        
        
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: formData.fileInfoType, parameters: dataDict, onCompletion: { (data) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    DataUtils.showAlert(title: "", msg: "Post Successfully Uploaded.", selfObj: self) {
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        if categoryVw != nil {
            categoryVw.removeFromSuperview()
            categoryVw = nil
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if categoryVw != nil {
            categoryVw.removeFromSuperview()
            categoryVw = nil
        }
    }
    
    @objc func btnVideoAction() {
        AttachmentHandler.shared.showVideoList(vc: self)
        
    }
    @objc func btnPdfAction() {
        AttachmentHandler.shared.showFileLibraryList(vc: self, fileType: "pdf")
        
    }
    @objc func btnVoiceAction() {
        let voiceVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
        voiceVC.fileNameAudio = randomString(length: 8)
        self.navigationController?.present(voiceVC, animated: true, completion: {
            
        })
        //     AttachmentHandler.shared.showAudioList(vc: self)
        
        
        
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    @objc func btnPhotosAction(sender : UIView) {
        AttachmentHandler.shared.showImageLibraryList(vc: self)
        
        //        imagePicker = ImagePicker(presentationController: self, delegate: self)
        //        imagePicker.present(from: sender)
        
        
    }
    @objc func btnTextAction() {
        AttachmentHandler.shared.showFileLibraryList(vc: self, fileType: "txt")
    }
    
    
}

extension IncognitoFormVC : UITableViewDelegate,UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 101 {
            return ARRTRASHCATEGORY.count
        }
        switch selectedPage {
        case INCOGNITOTRASH:
            return arrTrashHeader.count
        case INCOGNITODEBATE:
            return arrDebateHeader.count
        case INCOGNITOCALLOUT:
            return arrCalloutHeader.count
        case INCOGNITOJOKES:
            return arrJokeHeader.count
        case INCOGNITOFORUMS:
            return arrForumsHeader.count
        case INCOGNITOSHAME:
            return arrShameHeader.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 101 {
            let tblCell = tableView.dequeueReusableCell(withIdentifier: "CategoryTblCell", for: indexPath) as! CategoryTblCell
            
            tblCell.lblTopic.text = ARRTRASHCATEGORY[indexPath.row]
            
            return tblCell
        }else {
            
            if selectedPage == INCOGNITOJOKES{
                if indexPath.row == 1 {
                    let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncognitoFormTrashCell", for: indexPath) as! IncognitoFormTrashCell
                    tblCell.lblTxtHeader.text = arrJokeHeader[indexPath.row]
                    tblCell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                    tblCell.btnPdf.addTarget(self, action: #selector(btnPdfAction), for: .touchUpInside)
                    tblCell.btnVoice.addTarget(self, action: #selector(btnVoiceAction), for: .touchUpInside)
                    tblCell.btnPhoto.addTarget(self, action: #selector(btnPhotosAction(sender:)), for: .touchUpInside)
                    tblCell.btnTxt.addTarget(self, action: #selector(btnTextAction), for: .touchUpInside)
                    return tblCell
                }
                if indexPath.row == 2 {
                    let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncognitoFormPostCell", for: indexPath) as! IncognitoFormPostCell
                    // tblCell.txtVwPost.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                    formData.post = tblCell.txtVwPost.text
                    tblCell.txtVwPost.delegate = self
                    return tblCell
                }
            }
            
            if indexPath.row == 3 {
                let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncognitoFormTrashCell", for: indexPath) as! IncognitoFormTrashCell
                if selectedPage == INCOGNITODEBATE {
                    tblCell.lblTxtHeader.text = arrDebateHeader[indexPath.row]
                }else if selectedPage == INCOGNITOTRASH {
                    tblCell.lblTxtHeader.text = arrTrashHeader[indexPath.row]
                }else if selectedPage == INCOGNITOCALLOUT {
                    tblCell.lblTxtHeader.text = arrCalloutHeader[indexPath.row]
                }
                tblCell.btnVideo.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                tblCell.btnPdf.addTarget(self, action: #selector(btnPdfAction), for: .touchUpInside)
                tblCell.btnVoice.addTarget(self, action: #selector(btnVoiceAction), for: .touchUpInside)
                tblCell.btnPhoto.addTarget(self, action: #selector(btnPhotosAction(sender:)), for: .touchUpInside)
                tblCell.btnTxt.addTarget(self, action: #selector(btnTextAction), for: .touchUpInside)
                return tblCell
                
            }
            if indexPath.row == 4 {
                let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncognitoFormPostCell", for: indexPath) as! IncognitoFormPostCell
                // tblCell.txtVwPost.addTarget(self, action: #selector(btnVideoAction), for: .touchUpInside)
                formData.post = tblCell.txtVwPost.text
                tblCell.txtVwPost.delegate = self
                
                return tblCell
                
            }
            
            let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncognitoFormCell", for: indexPath) as! IncognitoFormCell
            tblCell.txtFldForm.tag = indexPath.row
            tblCell.txtFldForm.delegate = self
            switch selectedPage {
            case INCOGNITOTRASH:
                if arrTrashHeader[indexPath.row] == "Category ?" {
                    tblCell.txtFldForm.tag = indexPath.row
                    tblCell.txtFldForm.delegate = self
                    tblCell.lblHeader.text = ""
                    tblCell.txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Category ?",
                                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                    tblCell.txtFldForm.setIconOnTextFieldRight(UIImage(named: "dropDownArrow")!)
                    tblCell.txtFldForm.inputView = UIView()
                    
                    
                    //tblCell.txtFldForm.inputView = datePicker
                }else {
                    tblCell.lblHeader.text = arrTrashHeader[indexPath.row]
                    formData.text1 = tblCell.txtFldForm.text
                }
                
            case INCOGNITODEBATE:
                if arrDebateHeader[indexPath.row] == "Category ?" {
                    tblCell.txtFldForm.tag = indexPath.row
                    tblCell.txtFldForm.delegate = self
                    tblCell.lblHeader.text = ""
                    tblCell.txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Category ?",
                                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                    tblCell.txtFldForm.setIconOnTextFieldRight(UIImage(named: "dropDownArrow")!)
                    tblCell.txtFldForm.inputView = UIView()
                    
                    
                    //tblCell.txtFldForm.inputView = datePicker
                }else {
                    tblCell.lblHeader.text = arrDebateHeader[indexPath.row]
                    formData.text1 = tblCell.txtFldForm.text
                }
                
            case INCOGNITOCALLOUT:
                if arrCalloutHeader[indexPath.row] == "Category ?" {
                    tblCell.txtFldForm.tag = indexPath.row
                    tblCell.txtFldForm.delegate = self
                    tblCell.lblHeader.text = ""
                    tblCell.txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Category ?",
                                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                    tblCell.txtFldForm.setIconOnTextFieldRight(UIImage(named: "dropDownArrow")!)
                    tblCell.txtFldForm.inputView = UIView()
                    
                    
                    //tblCell.txtFldForm.inputView = datePicker
                }else {
                    tblCell.lblHeader.text = arrCalloutHeader[indexPath.row]
                    formData.text1 = tblCell.txtFldForm.text
                }
            case INCOGNITOJOKES:
                if arrCalloutHeader[indexPath.row] == "Category ?" {
                    tblCell.txtFldForm.tag = indexPath.row
                    tblCell.txtFldForm.delegate = self
                    tblCell.lblHeader.text = ""
                    tblCell.txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Category ?",
                                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                    tblCell.txtFldForm.setIconOnTextFieldRight(UIImage(named: "dropDownArrow")!)
                    tblCell.txtFldForm.inputView = UIView()
                    
                    
                    //tblCell.txtFldForm.inputView = datePicker
                }else {
                    tblCell.lblHeader.text = arrJokeHeader[indexPath.row]
                    formData.text1 = tblCell.txtFldForm.text
                }
            case INCOGNITOFORUMS:
                tblCell.lblHeader.text = arrForumsHeader[indexPath.row]
                
            case INCOGNITOSHAME:
                tblCell.lblHeader.text = arrShameHeader[indexPath.row]
            default:
                tblCell.lblHeader.text = ""
            }
            return tblCell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView.tag == 101 {
            return 40
        }
        if arrTrashHeader[indexPath.row] == "Post" {
            return 170
        }
        if selectedPage == INCOGNITOJOKES {
            if indexPath.row == 2 {
                return 150
            }
        }
        if arrTrashHeader[indexPath.row] ==  "remove" {
            return 0
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 101 {
            let path = IndexPath(item: 0, section: 0)
            let cell = self.tblForm.cellForRow(at: path) as! IncognitoFormCell
            cell.txtFldForm.text = ARRTRASHCATEGORY[indexPath.row]
            formData.selectedCategory = ARRTRASHCATEGORY[indexPath.row]
            formData.category = selectedPage
            if categoryVw != nil {
                categoryVw.removeFromSuperview()
                categoryVw = nil
            }
        }
    }
    @IBAction func btnBackction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension IncognitoFormVC : UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            if categoryVw == nil {
                self.view.endEditing(true)
                var window = UIApplication.shared.keyWindow!
                //  let v = UIView(frame: window.bounds)
                //            window.addSubview(v);
                //            v.backgroundColor = UIColor.clear
                categoryVw = UIView(frame: CGRect(x: textField.frame.origin.x + textField.frame.size.width/2, y: tblForm.frame.origin.y + textField.frame.origin.y + textField.frame.size.height , width: 150, height: 240))
                categoryVw.backgroundColor = UIColor.clear
                categoryVw.layer.borderWidth = 1
                categoryVw.layer.borderColor = DataUtils.hexStringToUIColor(hex: "#2F6A69").cgColor
                window.addSubview(categoryVw)
                
                let tableView: UITableView = UITableView()
                tableView.register(UINib(nibName: "CategoryTblCell", bundle: nil), forCellReuseIdentifier: "CategoryTblCell")
                
                tableView.frame = CGRect(x: 0, y: 0, width: categoryVw.frame.size.width, height: 240)
                tableView.tag = 101
                tableView.dataSource = self
                tableView.delegate = self
                tableView.backgroundColor = UIColor.clear
                categoryVw.addSubview(tableView)
            }else {
                categoryVw.removeFromSuperview()
                categoryVw = nil
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if selectedPage != INCOGNITOJOKES {
            if textField.tag == 1 {
                formData.text1 = textField.text
            }
            if textField.tag == 2 {
                formData.text2 = textField.text
            }
        }else {
            
        }
        
        formData.category = selectedPage
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        formData.post = textView.text
        formData.category = selectedPage
    }
}


extension IncognitoFormVC: ImagePickerDelegate {
    func uploadVideoToServer(videoUrl: String, videoName: String) {
        
    }
    
    
    func selectedImgFormat(imageFormat : String) {
        formData.fileInfoType[0].fileName = imageFormat
    }
    
    
    func didSelect(image: UIImage?) {
        
        if image != nil {
            let imageData:Data = image!.pngData()!
            // formData.fileInfoType[0].fileData = imageData
            
            //            let imageStr = imageData.base64EncodedString()
            //            formData.photosData = imageStr
        }
    }
}

//import UIKit
//
//class ViewController: UIViewController {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        let showAlertButton = UIButton(type: UIButtonType.Custom)
//        showAlertButton.frame = CGRectMake(0, 0, 100, 40)
//        showAlertButton.setTitle("Show Alert", forState: .Normal)
//        showAlertButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
//        self.view.addSubview(showAlertButton)
//        showAlertButton.center = self.view.center
//
//        showAlertButton.addTarget(self, action: "showAlert", forControlEvents: UIControlEvents.TouchUpInside)
//    }
//
//
//}
