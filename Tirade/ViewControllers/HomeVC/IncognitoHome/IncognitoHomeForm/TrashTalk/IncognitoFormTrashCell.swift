//
//  IncognitoFormTrashCell.swift
//  Tirade
//
//  Created by admin on 19/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class IncognitoFormTrashCell: UITableViewCell {

    @IBOutlet weak var btnVideo : UIButton!
    @IBOutlet weak var btnPdf : UIButton!
    @IBOutlet weak var btnVoice : UIButton!
    @IBOutlet weak var btnPhoto : UIButton!
    @IBOutlet weak var btnTxt : UIButton!
    @IBOutlet weak var lblTxtHeader : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        addBorder(button: btnVideo)
        addBorder(button: btnPdf)
        addBorder(button: btnVoice)
        addBorder(button: btnPhoto)
        addBorder(button: btnTxt)
    }
    
    func addBorder(button : UIButton)   {
        button.layer.cornerRadius = 14
        button.layer.borderWidth = 2
        button.layer.borderColor = DataUtils.hexStringToUIColor(hex: "#D4AF37").cgColor
    }
}
