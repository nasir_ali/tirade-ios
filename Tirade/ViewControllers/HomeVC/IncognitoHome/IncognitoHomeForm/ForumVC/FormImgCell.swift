//
//  FormImgCell.swift
//  Tirade
//
//  Created by admin on 07/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import PDFKit

class FormImgCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgHeightCons: NSLayoutConstraint!
    @IBOutlet weak var imgWidthCons: NSLayoutConstraint!
    @IBOutlet weak var forumName: UILabel!
    @IBOutlet weak var pdfView: PDFView!
}
