//
//  ForumVC.swift
//  Tirade
//
//  Created by admin on 04/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import PDFKit

class ForumVC: TiradeBaseVC {
    
    @IBOutlet weak var collVwForum: UICollectionView!
    @IBOutlet weak var btnFloating: UIButton!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!

    var arrForum : [IncognitoForum]?
    var videoUrlArr = [URL]()
    var forumID = [String]()

    var isLocalSearch = false
    var copySearchArr : [IncognitoForum]?
    var arrOriginalForum : [IncognitoForum]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        makeUrlRequest()
        
    }
    func makeUrlRequest()  {
        let url = BASEURL + INCOGNITOFORUMGETDATA
        
        ServerRequest.doGetRequestToServer(url, onController: self) { (responseData) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
            }
            do {
                let trashArr : [String : [IncognitoForum]]  = try! JSONDecoder().decode([String: [IncognitoForum]].self, from: responseData)
                print(trashArr)
                self.arrForum = trashArr["data"]
                self.arrOriginalForum = trashArr["data"]
                DispatchQueue.main.async {
                    self.collVwForum.reloadData()
                }
            }catch  {
                return
            }
        }
    }
    @IBAction func btnFloatingAction(_ sender: UIButton) {
        
        
    }
    
    @IBAction func btnHomeAction(_ sender: UIButton) {
        let forumVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IncognitoHomeVC") as! IncognitoHomeVC
        self.navigationController?.pushViewController(forumVC, animated: false)
        
    }
    @IBAction func btnSettingAction(_ sender: UIButton) {
    }
    @IBAction func btnNotificationAction(_ sender: UIButton) {
    }
    @IBAction func btnMoreAction(_ sender: UIButton) {
    }
    
    @IBAction func drawerIconTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //        let viewController22 = LeftSideViewController(nibName: "LeftSideViewController", bundle: nil)
        //        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: viewController22)
        //        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        //        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
}

extension ForumVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isLocalSearch {
            if copySearchArr?.count == 0 {
                       lblNoDataFound.isHidden = false
                   }else {
                       lblNoDataFound.isHidden = true
                   }
                   return copySearchArr?.count ?? 0
        }else {
            if arrForum?.count == 0 {
                       lblNoDataFound.isHidden = false
                   }else {
                       lblNoDataFound.isHidden = true
                   }
                   return arrForum?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isLocalSearch {
            let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForumCollectionCell", for: indexPath) as! ForumCollectionCell
                  collectionCell.forumName.text = self.copySearchArr![indexPath.row].fourmName

                  
                  if self.copySearchArr![indexPath.row].fourmImage! == "" {
                  }else {
                      let collectionCellImg = collectionView.dequeueReusableCell(withReuseIdentifier: "FormImgCell", for: indexPath) as! FormImgCell
                      collectionCellImg.imgWidthCons.constant = collVwForum.frame.size.width/2 - 15
                      collectionCellImg.imgHeightCons.constant =   collVwForum.frame.size.height/2 - 10
                      if !self.copySearchArr![indexPath.row].fourmImage!.hasSuffix("pdf") {
                          collectionCellImg.img.kf.setImage(with: URL(string: self.copySearchArr![indexPath.row].fourmImage!)!)
                      }
                      collectionCellImg.forumName.text = self.copySearchArr![indexPath.row].fourmName
                    if self.copySearchArr![indexPath.row].forumNotes! == "" {

                    }else {
                                  var url : NSString = self.copySearchArr![indexPath.row].forumNotes as! NSString
                                  var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                                  var searchURL : NSURL = NSURL(string: urlStr as String)!
                                  var pdfUrlll = searchURL
                                  if let pdfDocument = PDFDocument(url: pdfUrlll as URL) {
                                      collectionCellImg.pdfView.displayMode = .singlePageContinuous
                                      collectionCellImg.pdfView.autoScales = true
                                      collectionCellImg.pdfView.displayDirection = .vertical
                                      collectionCellImg.pdfView.document = pdfDocument
                                  }
                              }
                      return collectionCellImg
                  }
                  // collectionCell.img.kf.setImage(with: URL(string: self.arrForum![indexPath.row].forumVideo!)!)
                  let url : NSString = ((self.copySearchArr?[indexPath.row].forumVideo)!) as NSString
                  let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                  let searchURL : NSURL = NSURL(string: urlStr as String)!
                  _ = searchURL
                  if ((self.copySearchArr?[indexPath.row].forumVideo)!) != "" {
                      collectionCell.setVideo(url:searchURL as URL)
                      videoUrlArr.append(searchURL as URL)
                    forumID.append(self.copySearchArr![indexPath.row].id!)
                  }else {
                    
                  }
            
                  return collectionCell
        }
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForumCollectionCell", for: indexPath) as! ForumCollectionCell
        collectionCell.forumName.text = self.arrForum![indexPath.row].fourmName

        
        if self.arrForum![indexPath.row].fourmImage! == "" {
        }else {
            let collectionCellImg = collectionView.dequeueReusableCell(withReuseIdentifier: "FormImgCell", for: indexPath) as! FormImgCell
            collectionCellImg.imgWidthCons.constant = collVwForum.frame.size.width/2 - 15
            collectionCellImg.imgHeightCons.constant =   collVwForum.frame.size.height/2 - 10
            if !self.arrForum![indexPath.row].fourmImage!.hasSuffix("pdf") {
                collectionCellImg.img.kf.setImage(with: URL(string: self.arrForum![indexPath.row].fourmImage!)!)
            }
            collectionCellImg.forumName.text = self.arrForum![indexPath.row].fourmName
            if self.arrForum![indexPath.row].forumNotes! == "" {
            }else {
                var url : NSString = self.arrForum![indexPath.row].forumNotes as! NSString
                var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                var searchURL : NSURL = NSURL(string: urlStr as String)!
                var pdfUrlll = searchURL
                if let pdfDocument = PDFDocument(url: pdfUrlll as URL) {
                    collectionCellImg.pdfView.displayMode = .singlePageContinuous
                    collectionCellImg.pdfView.autoScales = true
                    collectionCellImg.pdfView.displayDirection = .vertical
                    collectionCellImg.pdfView.document = pdfDocument
                }

            }
            
            return collectionCellImg
        }
        // collectionCell.img.kf.setImage(with: URL(string: self.arrForum![indexPath.row].forumVideo!)!)
        let url : NSString = ((self.arrForum?[indexPath.row].forumVideo)!) as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let searchURL : NSURL = NSURL(string: urlStr as String)!
        _ = searchURL
        if ((self.arrForum?[indexPath.row].forumVideo)!) != "" {
            collectionCell.setVideo(url:searchURL as URL)
            videoUrlArr.append(searchURL as URL)
            forumID.append(self.arrForum![indexPath.row].id!)

        }else {
        }
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 15, height:  collectionView.frame.size.height/2 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isLocalSearch {
            if self.copySearchArr![indexPath.row].user_id! != "" {
                let playerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayerVC") as! PlayerVC
                playerVC.forumID = self.copySearchArr![indexPath.row].user_id
                playerVC.forumDetail = self.copySearchArr![indexPath.row]


                self.navigationController?.pushViewController(playerVC, animated: false)
            }
        }else {
            if self.arrForum![indexPath.row].user_id! != "" {
                let playerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayerVC") as! PlayerVC
                playerVC.forumID = self.arrForum![indexPath.row].user_id
                playerVC.forumDetail = self.arrForum![indexPath.row]
                self.navigationController?.pushViewController(playerVC, animated: false)
            }
        }
        
    }
}

extension ForumVC: UISearchBarDelegate,UITextFieldDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
      isLocalSearch = true
        var textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        textFieldInsideSearchBar!.becomeFirstResponder()

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.arrForum = self.arrOriginalForum
        if searchText == "" {
            isLocalSearch = false
        }
        self.copySearchArr = self.arrForum?.filter({ (data) -> Bool in
            
            data.fourmName?.lowercased() == searchBar.text?.lowercased()
            
        })
        collVwForum.reloadData()

    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       isLocalSearch = false
        searchBar.text = ""
        collVwForum.reloadData()
        self.view.endEditing(true)
    }
}

