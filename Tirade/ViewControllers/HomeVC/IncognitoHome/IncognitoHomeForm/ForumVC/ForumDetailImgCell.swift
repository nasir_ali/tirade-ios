//
//  ForumDetailImgCell.swift
//  Tirade
//
//  Created by admin on 19/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class ForumDetailImgCell: UITableViewCell {

    @IBOutlet weak var imgForum: UIImageView!
      @IBOutlet weak var viewButton: UIButton!
      @IBOutlet weak var shareButton: UIButton!
      @IBOutlet weak var commentsButton: UIButton!
      @IBOutlet weak var likeButton: UIButton!
      
      @IBOutlet weak var likeButtonAction: UIButton!
      @IBOutlet weak var comentButtonAction: UIButton!
      @IBOutlet weak var shareButtonAction : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
