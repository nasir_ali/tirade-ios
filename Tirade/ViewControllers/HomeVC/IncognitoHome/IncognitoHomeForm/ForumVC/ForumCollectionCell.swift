//
//  ForumCollectionCell.swift
//  Tirade
//
//  Created by admin on 05/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import AVKit
class ForumCollectionCell: UICollectionViewCell {
    var videoURL:URL!
    var player:AVPlayer!
    var playerLayer:AVPlayerLayer!
    let controller = AVPlayerViewController()
    @IBOutlet weak var forumName: UILabel!

    @IBOutlet weak var playerView: UIView!
    override func awakeFromNib() {
         super.awakeFromNib()
         // Initialization code
     }
    func setVideo(url:URL){
        player = AVPlayer(url: url)
        controller.player=player
        controller.view.frame = playerView.bounds
        playerView.addSubview(controller.view)
        controller.player?.isMuted = true
        controller.player?.pause()
        //controller.player?.pause()
    }
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgHeightCons: NSLayoutConstraint!
    @IBOutlet weak var imgWidthCons: NSLayoutConstraint!
}
