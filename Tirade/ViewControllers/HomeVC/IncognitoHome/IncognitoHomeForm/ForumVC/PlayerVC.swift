//
//  PlayerVC.swift
//  Tirade
//
//  Created by Lucky on 07/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import AVKit
import PDFKit
import EzPopup

class PlayerVC: TiradeBaseVC,NumberPickerViewControllerDelegate {
    func numberPickerViewController(sender: NumberPickerViewController, didSelectNumber selectedIDs: [String]) {
        
    }
    
   // var videoUrlArr : [URL]?
    var forumID : String?
    var arrOnlyVideo = [IncognitoForum]()
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgName : UILabel!
    @IBOutlet weak var btnDropDown : UIButton!
    @IBOutlet weak var totalVideos : UILabel!

    var arrImg : [ForumDetailImages]!
    var arrVideos : [ForumDetailVideos]!
    var arrNotes : [ForumDetailNotes]!
    var selectionIndex = 0
    var arrComments : [Comments]?
    var arrLikes : [Likes]?

    let pickerVC = NumberPickerViewController.instantiate()
    var forumDetail : IncognitoForum?


    var videoURL:URL = URL(string:"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!
    var data = [Int:[String:Any]]() // array to maintain in which row video play or in which row video not play
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeUrlRequest()
        self.btnDropDown.setTitle("Videos ⌄", for: .normal)
        imgUser.layer.cornerRadius = 10
        btnDropDown.layer.cornerRadius = 5
        btnDropDown.layer.borderWidth = 1
        btnDropDown.layer.borderColor = UIColor.white.cgColor
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

 //       arrOnlyVideo = [IncognitoForum]()
//        for video in arrForum! {
//            if video.forumVideo != "" {
//                arrOnlyVideo.append(video)
//            }
//        }
//        for url in 0..<arrOnlyVideo.count  {
//
//            if arrOnlyVideo[url].forumVideo != nil {
//                var urls : NSString = arrOnlyVideo[url].forumVideo as NSString
//                var urlStr : NSString = urls.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//                var searchURL : NSURL = NSURL(string: urlStr as String)!
//                data[url] = ["url": searchURL,"play":false]
//            }
//        }
    }
    
    func makeUrlRequest()  {
        let url = BASEURL + FORUMDETAILALL
        //TiradeUserDefaults.getUser_ID(key: "user_id")
        let dataDict = ["user_id" : forumID] as [String : Any]
        ServerRequest.doPostDataRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) { (responseData) in
            DispatchQueue.main.async {
                let trashArr : [String : [ForumDetailByUser]]  = try! JSONDecoder().decode([String: [ForumDetailByUser]].self, from: responseData)
                let data = trashArr["data"]
                
                self.arrImg = data![0].images
                self.arrVideos = data![0].videos
                self.arrNotes = data![0].notes
                if data![0].name != nil && data![0].name != "" {
                    self.imgName.text = data![0].name
                }
                if data![0].user_image != nil && data![0].user_image! != "" {
                    self.imgUser.image = UIImage(named: data![0].user_image!)
                    self.imgUser.kf.setImage(with: URL(string: data![0].user_image!)!)
                }

                if self.arrVideos.count > 0 {
                    for url in 0..<self.arrVideos.count  {
                         if self.arrVideos[url].forumVideo != nil {
                             var urls : NSString = self.arrVideos[url].forumVideo as! NSString
                             var urlStr : NSString = urls.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                             var searchURL : NSURL = NSURL(string: urlStr as String)!
                             self.data[url] = ["url": searchURL,"play":false]
                         }
                     }
                }

                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
                
            }
        }
    }
    
    
    func openSelectionFileType()  {
        let alert = UIAlertController(title: "", message: "Select from", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Video", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.selectionIndex = 0
            self.btnDropDown.setTitle("Videos ⌄", for: .normal)
            self.tableView.reloadData()
            self.dismiss(animated: true) {
            }
        }))
        alert.addAction(UIAlertAction(title: "Images", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.selectionIndex = 1
            self.btnDropDown.setTitle("Images ⌄", for: .normal)

            self.tableView.reloadData()
            self.dismiss(animated: true) {
            }
        }))
        alert.addAction(UIAlertAction(title: "Files", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.selectionIndex = 2
            self.btnDropDown.setTitle("Files ⌄", for: .normal)

            self.tableView.reloadData()
            self.dismiss(animated: true) {
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func openSelectionFileType(_ sender: UIButton) {
        openSelectionFileType()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnMyForumDetails(_ sender: UIButton) {
        let myFormVC = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "MyForumDetails") as! MyForumDetails
        myFormVC.forumDetail = forumDetail
        self.navigationController?.pushViewController(myFormVC, animated: false)
    }
}

extension PlayerVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectionIndex == 0 {
            totalVideos.text = "Total Videos \(arrVideos.count)"
            return arrVideos.count
        }else if self.selectionIndex == 1 {
            totalVideos.text = "Total Images \(arrImg.count)"
            return arrImg.count
        }else if self.selectionIndex == 2 {
            totalVideos.text = "Total Files \(arrNotes.count)"
            return arrNotes.count
        }
        return arrOnlyVideo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.selectionIndex == 0 {
            let playerCell = tableView.dequeueReusableCell(withIdentifier:"PlayerTbCell") as! PlayerTbCell
            if data.count > 0 {
                
            }
            playerCell.playVideo(videoURL:data[indexPath.row]!["url"] as! URL,play:(data[indexPath.row]!["play"] as! Bool))
            playerCell.likeButtonAction.tag = indexPath.row
            playerCell.likeButtonAction.addTarget(self, action: #selector(likeOnVideo(sender:)), for: .touchUpInside)
            
            playerCell.comentButtonAction.tag = indexPath.row
            playerCell.comentButtonAction.addTarget(self, action: #selector(commentOnVideo(sender:)), for: .touchUpInside)
            
            playerCell.shareButtonAction.tag = indexPath.row
            playerCell.shareButtonAction.addTarget(self, action: #selector(shareOnVideo(sender:)), for: .touchUpInside)
            
            playerCell.commentsButton.tag = indexPath.row
            playerCell.commentsButton.accessibilityValue = (self.arrVideos[indexPath.row].forum_id)
            playerCell.commentsButton.addTarget(self, action: #selector(commentCountAction(sender:)), for: .touchUpInside)
            
            playerCell.likeButton.tag = indexPath.row
            playerCell.likeButton.accessibilityValue = (self.arrVideos[indexPath.row].forum_id)
            playerCell.likeButton.addTarget(self, action: #selector(likeCountAction(sender:)), for: .touchUpInside)

            if ((self.arrVideos[indexPath.row].forumVideo)!) != "" {
                playerCell.likeButton.setTitle("\(self.arrVideos[indexPath.row].total_agree ?? "0") like", for: .normal)
                playerCell.commentsButton.setTitle("\(self.arrVideos[indexPath.row].total_comment ?? "0") comments", for: .normal)
  
            }
            return playerCell
        }else if self.selectionIndex == 1 {
            let imageCell = tableView.dequeueReusableCell(withIdentifier:"ForumDetailImgCell") as! ForumDetailImgCell
            if (arrImg?[indexPath.row].fourmImage) != "" && (arrImg?[indexPath.row].fourmImage) != nil {
                imageCell.imgForum.kf.setImage(with: URL(string: (arrImg?[indexPath.row].fourmImage)!)!)
            }
            imageCell.likeButtonAction.tag = indexPath.row
            imageCell.likeButtonAction.addTarget(self, action: #selector(likeOnVideo(sender:)), for: .touchUpInside)
            
            imageCell.comentButtonAction.tag = indexPath.row
            imageCell.comentButtonAction.addTarget(self, action: #selector(commentOnVideo(sender:)), for: .touchUpInside)
            
            imageCell.shareButtonAction.tag = indexPath.row
            imageCell.shareButtonAction.addTarget(self, action: #selector(shareOnVideo(sender:)), for: .touchUpInside)
            
            if (arrImg?[indexPath.row].fourmImage) != "" {
              imageCell.likeButton.setTitle("\(self.arrImg[indexPath.row].total_agree ?? "0") like", for: .normal)
              imageCell.commentsButton.setTitle("\(self.arrImg[indexPath.row].total_comment ?? "0") comments", for: .normal)

          }
            imageCell.commentsButton.tag = indexPath.row
            imageCell.commentsButton.accessibilityValue = (self.arrImg[indexPath.row].forum_id)
            imageCell.commentsButton.addTarget(self, action: #selector(commentCountAction(sender:)), for: .touchUpInside)
            
            imageCell.likeButton.tag = indexPath.row
            imageCell.likeButton.accessibilityValue = (self.arrImg[indexPath.row].forum_id)
            imageCell.likeButton.addTarget(self, action: #selector(likeCountAction(sender:)), for: .touchUpInside)

            
            return imageCell
        }
        
        let notesCell = tableView.dequeueReusableCell(withIdentifier:"ForumDetailFileCell") as! ForumDetailFileCell
        if self.arrNotes![indexPath.row].forumNotes == nil || self.arrNotes![indexPath.row].forumNotes == "" {
        }else {
            var url : NSString = self.arrNotes![indexPath.row].forumNotes! as NSString
            var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            var searchURL : NSURL = NSURL(string: urlStr as String)!
            var pdfUrlll = searchURL
            if let pdfDocument = PDFDocument(url: pdfUrlll as URL) {
                notesCell.pdfView.displayMode = .singlePageContinuous
                notesCell.pdfView.autoScales = true
                notesCell.pdfView.displayDirection = .vertical
                notesCell.pdfView.document = pdfDocument
            }
        }
        if self.arrNotes![indexPath.row].forumNotes != "" {
            notesCell.likeButton.setTitle("\(self.arrNotes[indexPath.row].total_agree ?? "0") like", for: .normal)
            notesCell.commentsButton.setTitle("\(self.arrNotes[indexPath.row].total_comment ?? "0") comments", for: .normal)
        }
        notesCell.commentsButton.tag = indexPath.row
        notesCell.commentsButton.accessibilityValue = (self.arrNotes[indexPath.row].forum_id)
        notesCell.commentsButton.addTarget(self, action: #selector(commentCountAction(sender:)), for: .touchUpInside)

        notesCell.likeButton.tag = indexPath.row
        notesCell.likeButton.accessibilityValue = (self.arrNotes[indexPath.row].forum_id)
        notesCell.likeButton.addTarget(self, action: #selector(likeCountAction(sender:)), for: .touchUpInside)
        
        return notesCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func likeOnVideo(sender : UIButton) {
        var likeID = String()
        if selectionIndex == 0 {
            likeID = self.arrVideos[sender.tag].forum_id ?? ""
        }
        if selectionIndex == 1 {
            likeID = self.arrImg[sender.tag].forum_id ?? ""
        }
        if selectionIndex == 2 {
            likeID = self.arrNotes[sender.tag].forum_id ?? ""
        }
        let dataDict = ["forum_id" : likeID ,"value":"agree","user_id" : TiradeUserDefaults.getUser_ID(key: "user_id")] as [String : Any]
        sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
    }
    
    @objc func commentCountAction(sender : UIButton) {
        let forumData = ["forum_id" : sender.accessibilityValue ?? ""]
       getVideoActionDataToServer(dataDict: forumData as [String : AnyObject])

    }
    
    @objc func likeCountAction(sender : UIButton) {
        let forumData = ["forum_id" : sender.accessibilityValue ?? ""]
       getLikeVideoActionDataToServer(dataDict: forumData as [String : AnyObject])
    }
    
    @objc func commentOnVideo(sender : UIButton) {
        openCommentController(selectedRow: sender.tag)
     }
    
    @objc func shareOnVideo(sender : UIButton) {
        shareOnSocial(sender: sender)
     }
    
        func shareOnSocial(sender : UIButton) {
            let imgMember = UIImageView()
            var lblMemberID = String()

            var urlShare = String()
            if selectionIndex == 0 {
                if((self.arrVideos[sender.tag].forumVideo)!) != "" {
                    urlShare = ((self.arrVideos[sender.tag].forumVideo)!)
                }
            }
            if selectionIndex == 1 {
                if((self.arrImg[sender.tag].fourmImage)!) != "" {
                    urlShare = ((self.arrImg[sender.tag].fourmImage)!)
                }
            }
            if selectionIndex == 2 {
                if((self.arrNotes[sender.tag].forumNotes)!) != "" {
                    urlShare = ((self.arrNotes[sender.tag].forumNotes)!)
                }
            }
           
            let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getUser_Image(key: "user_Image"), options: .ignoreUnknownCharacters)!
                  if  dataDecoded.count > 0 {
                      let imageFromData: UIImage = UIImage(data:dataDecoded)!
                      imgMember.image = imageFromData
                  }else {
                      imgMember.image = UIImage(named: "user")
                  }
                lblMemberID = TiradeUserDefaults.getUser_Name(key: "user_Name")
            let shareAll = [urlShare,imgMember,lblMemberID] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.addToReadingList,UIActivity.ActivityType.mail,UIActivity.ActivityType.message,UIActivity.ActivityType.airDrop,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.print,UIActivity.ActivityType.markupAsPDF,UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension"),
                                                            UIActivity.ActivityType(rawValue: "net.whatsapp.WhatsApp.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "pinterest.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.facebook.Messenger.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.fogcreek.trello.trelloshare"),
                                                            UIActivity.ActivityType(rawValue: "com.linkedin.LinkedIn.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.hammerandchisel.discord.Share"),
                                                            UIActivity.ActivityType(rawValue: "com.google.Gmail.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.google.inbox.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.riffsy.RiffsyKeyboard.RiffsyShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.google.hangouts.ShareExtension"),
                                                            UIActivity.ActivityType(rawValue: "com.ifttt.ifttt.share"),    UIActivity.ActivityType(rawValue: "com.burbn.instagram.shareextension")
    ]
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    
    func openCommentController(selectedRow : Int) {
       
        let chatVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatVC.isVideoPLayerComments = true
        chatVC.selectedCommentRow = selectedRow
        if selectionIndex == 0 {
            chatVC.commentsID = self.arrVideos[selectedRow].forum_id
        }
        if selectionIndex == 1 {
            chatVC.commentsID = self.arrImg[selectedRow].forum_id
        }
        if selectionIndex == 2 {
            chatVC.commentsID = self.arrNotes[selectedRow].forum_id
        }
        self.navigationController?.present(chatVC, animated: true, completion: {
        })
    }
    
    func getLikeVideoActionDataToServer(dataDict : [String : AnyObject])  {
        let url = "https://vridhisoftech.in/tirade/incognito/forum/get_likes/"
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: self) { (responseData) in
            DispatchQueue.main.async {
             let likesArr : [String : [Likes]]  = try! JSONDecoder().decode([String: [Likes]].self, from: responseData)
               self.arrLikes = likesArr["agree_data"]
                self.tableView.reloadData()
                guard let pickerVC = self.pickerVC else { return }
                       pickerVC.isSelectFromVideoDetail = true
                       pickerVC.delegate = self
                
                pickerVC.titleHere = "Total Likes"
                pickerVC.arrLikes = self.arrLikes
                       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateArrayData"), object: nil, userInfo: ["selectedTitleArr":pickerVC.arrLikes,"selectedTitle" : pickerVC.titleHere])

                let popupVC = PopupViewController(contentController: pickerVC, position: .center(CGPoint(x: self.btnDropDown.frame.origin.x  , y: self.btnDropDown.frame.origin.y - 120)), popupWidth: 200, popupHeight: 300)
                             popupVC.canTapOutsideToDismiss = true
                             popupVC.cornerRadius = 5
                self.present(popupVC, animated: true, completion: nil)
            }
        }
    }

    func getVideoActionDataToServer(dataDict : [String : AnyObject])  {
        let url = "https://vridhisoftech.in/tirade/incognito/forum/get_comments.php"
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: self) { (responseData) in
            DispatchQueue.main.async {
             let trashArr   = try! JSONDecoder().decode(CommentsDict.self, from: responseData)
                self.arrComments = trashArr.comments
                self.tableView.reloadData()
                guard let pickerVC = self.pickerVC else { return }
                       pickerVC.isSelectFromVideoDetail = true
                       pickerVC.delegate = self
                pickerVC.titleHere = "Total Comments"
                pickerVC.arrComments = self.arrComments
                       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateArrayData"), object: nil, userInfo: ["selectedTitleArr":pickerVC.arrComments,"selectedTitle" : pickerVC.titleHere])

                let popupVC = PopupViewController(contentController: pickerVC, position: .center(CGPoint(x: self.btnDropDown.frame.origin.x  , y: self.btnDropDown.frame.origin.y - 120)), popupWidth: 200, popupHeight: 300)
                             popupVC.canTapOutsideToDismiss = true
                             popupVC.cornerRadius = 5
                self.present(popupVC, animated: true, completion: nil)
            }
        }
    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + FORUMVIDEOACTION
        ServerRequest.doPostDataRequestToServer(url, data: dataDict, onController: self) { (data) in
            DispatchQueue.main.async {

            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height - 120
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        // -----------------------------this func call when user start scroll and scrolling stop by it self
        if selectionIndex == 0 {
            if data.count > 0 {
                for r in 0..<tableView.numberOfRows(inSection:0){
                    print(r)
                    var d = data[r]
                    d!["play"] = false
                    data[r] = d
                }
                if tableView.indexPathsForVisibleRows != nil{
                    var d = data[(tableView.indexPathsForVisibleRows?.first!.row)!]!
                    d["play"] = true
                    data[(tableView.indexPathsForVisibleRows?.first!.row)!] = d
                }
                tableView.reloadData()
            }
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // ----------------------------this func call when user start and stop scrolling by it self
//        for r in 0..<tableView.numberOfRows(inSection:0){
//            print(r)
//            var d = data[r]
//            d!["play"] = false
//            data[r] = d
//        }
//        if tableView.indexPathsForVisibleRows != nil{
//            var d = data[(tableView.indexPathsForVisibleRows?.first!.row)!]!
//            d["play"] = true
//            data[(tableView.indexPathsForVisibleRows?.first!.row)!] = d
//        }
//        tableView.reloadData()
    }


}
