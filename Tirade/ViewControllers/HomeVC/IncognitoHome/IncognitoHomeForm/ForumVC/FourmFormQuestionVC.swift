//
//  FourmFormQuestionVC.swift
//  Tirade
//
//  Created by admin on 15/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class FourmFormQuestionVC: TiradeBaseVC {
    
    @IBOutlet weak var vwSocial: UIView!
    @IBOutlet weak var tblForm: UITableView!
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var imgTopBack: UIImageView!
    @IBOutlet weak var imgTopImg: UIImageView!
    @IBOutlet weak var imgTopTxt: UIImageView!
    @IBOutlet weak var imgTopImgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgTopImgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgTopImgTopConstraint: NSLayoutConstraint!
    var fileInfoType = [FileTypeInfo]()
    var incogForumData : IncognitoForum!
    
    
    
    var selectedPage = INCOGNITOTRASH
    var selectedImage = INCOGNITOTRASHIMG
    
    var arrTrashHeader = ["Category ?","Who is The Audience For Your Forum ?","What is The Over All Goal Of Your Forum ?","Who's Videos Would You Like To Follow?","USE YOUR NAME AND PICTURE OR IMAGE OR CREATE A CUSTOM NAME,BRAND AND PIC OR IMAGE","","TellviewersaboutyourForum(Description)!","","","","My Website","Add URL","Social Links","Add Profile Name"]
    
    //  let v = UIView(frame: window.bounds)
    //            window.addSubview(v);
    //            v.backgroundColor = UIColor.clear
    var categoryVw : UIView!
    var formFieldData = [FourmsQuestion]()
    @IBAction func btnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
    
    @IBAction func btnSeeAllForumsAction(_ sender: UIButton) {
        let forumVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumVC") as! ForumVC
        self.navigationController?.pushViewController(forumVC, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")


        incogForumData = IncognitoForum()
        for item in 0...arrTrashHeader.count {
            let dataTxt = FourmsQuestion()
            formFieldData.append(dataTxt)
        }
        self.navigationController?.isNavigationBarHidden = true
        
        DataUtils.addBorder(view: txtCategory, cornerRadius: 14, borderWidth: 2, borderColor: DataUtils.hexStringToUIColor(hex: "#D4AF37"))
        tblForm.register(UINib(nibName: "CategorySelectionCell", bundle: nil), forCellReuseIdentifier: "CategorySelectionCell")
        vwSocial?.backgroundColor = UIColor(white: 1, alpha: 0.3)
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)
//        let forumVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumVC") as! ForumVC
//        self.navigationController?.pushViewController(forumVC, animated: false)
    }
    
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
    
        
    //    if fileInfoType.count < 2 {
            let fileData = FileTypeInfo()
            fileData.fileData = dictFile["fileData"] as! Data
            fileData.fileName = dictFile["fileName"] as! String
        if dictFile["uploadFileKey"] as! String == "post_video" {
            fileData.uploadFileKey = "forumVideo"

        }else if dictFile["uploadFileKey"] as! String == "post_image" {
            fileData.uploadFileKey = "fourmImage"

        }else {
            fileData.uploadFileKey = "forumNotes"

        }
            fileData.mimeType = dictFile["mimeType"] as! String
            fileInfoType.append(fileData)
//        }else {
//          let fileData = FileTypeInfo()
//            fileData.fileData = dictFile["fileData"] as! Data
//            fileData.fileName = dictFile["fileName"] as! String
//            fileData.uploadFileKey = "forumNotes"
//            fileData.mimeType = dictFile["mimeType"] as! String
//            fileInfoType.append(fileData)
 //       }
        
       
      
    }
    
    
    @IBAction func btnFaceBookAction(_ sender: UIButton) {
        
        
    }
    @IBAction func btnGmailBookAction(_ sender: UIButton) {
        
        
    }
    @IBAction func btnTwitterAction(_ sender: UIButton) {
    }
    @IBAction func btnWatsUpAction(_ sender: UIButton) {
    }
    @IBAction func btnContinueAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if fileInfoType.count > 0 {
            let dataDict = ["user_id" : TiradeUserDefaults.getUser_ID(key: "user_id"),"category" : incogForumData.category!,"fourmAudience":incogForumData.fourmAudience!,"fourmGoal":incogForumData.fourmGoal!,"fourmName":incogForumData.fourmName!,"followLike":incogForumData.followLike!,"fourmsDescription":incogForumData.fourmsDescription!,"linkSite":incogForumData.linkSite!,"customLink":incogForumData.customLink!,"titleLink":incogForumData.titleLink!,"myWebsite":incogForumData.myWebsite!,"addUrl":incogForumData.addUrl!,"socialLink":incogForumData.socialLink!,"profileName":incogForumData.profileName!,"fourmImage":fileInfoType,"forumNotes":fileInfoType,"forumVideo":fileInfoType] as [String : Any]
            sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
        }else {
            DataUtils.showAlert(title: "", msg: "Please attached the files", selfObj: self) {
                
            }
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                let forumVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumVC") as! ForumVC
//                self.navigationController?.pushViewController(forumVC, animated: false)
//
//            }
        }
    }
    
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + INCOGNITOFORUMPOSTDATA
        
        
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: fileInfoType, parameters: dataDict, onCompletion: { (data) in
            do {
                guard let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject] else {
                    return
                }
                DispatchQueue.main.async {
                    DataUtils.removeLoader()
                }
                if let dictFromJSON = jsonResult as? [String:AnyObject] {
                    let locData = dictFromJSON["message"] as? String
                    let status = dictFromJSON["status"] as? Int
                    let userID = dictFromJSON["user"] as? [String:AnyObject]
                    
                    if locData != nil {
                        DataUtils.showAlert(title: "", msg: locData!, selfObj: self) {
                            if status == 0 {
                                
                            }else {
                              let forumVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForumVC") as! ForumVC
                              self.navigationController?.pushViewController(forumVC, animated: false)

                            }
                        }
                    }
                }
            }catch  {
                
                return
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        if categoryVw != nil {
            categoryVw.removeFromSuperview()
            categoryVw = nil
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if categoryVw != nil {
            categoryVw.removeFromSuperview()
            categoryVw = nil
        }
    }
    
    @objc func btnVideoAction() {
        AttachmentHandler.shared.showVideoList(vc: self)
        
    }
    @objc func btnPdfAction() {
        AttachmentHandler.shared.showFileLibraryList(vc: self, fileType: "pdf")
        
    }
    @objc func btnVoiceAction() {
        let voiceVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
        voiceVC.fileNameAudio = randomString(length: 8)
        self.navigationController?.present(voiceVC, animated: true, completion: {
            
        })
        //     AttachmentHandler.shared.showAudioList(vc: self)
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    @objc func btnPhotosAction(sender : UIView) {
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
       // AttachmentHandler.shared.showImageLibraryList(vc: self)
    }
    @objc func btnTextAction() {
        AttachmentHandler.shared.showFileLibraryList(vc: self, fileType: "txt")
    }

}

extension FourmFormQuestionVC : UITableViewDelegate,UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 101 {
            return ARRTRASHCATEGORY.count
        }
        return arrTrashHeader.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 101 {
            let tblCell = tableView.dequeueReusableCell(withIdentifier: "CategoryTblCell", for: indexPath) as! CategoryTblCell
            tblCell.lblTopic.text = ARRTRASHCATEGORY[indexPath.row]
            
            return tblCell
        }else {
            
            if indexPath.row == 4 {
                let tblCell = tableView.dequeueReusableCell(withIdentifier: "FourmQuestionSimpleTXT", for: indexPath) as! FourmQuestionSimpleTXT
                tblCell.lblTxtHeader.text = arrTrashHeader[4]
                return tblCell
                
            }
            
            if indexPath.row == 6 {
                let tblCell = tableView.dequeueReusableCell(withIdentifier: "FourmQuestionFileCell", for: indexPath) as! FourmQuestionFileCell
                tblCell.btnPdf.addTarget(self, action: #selector(btnPdfAction), for: .touchUpInside)
                tblCell.btnPhoto.addTarget(self, action: #selector(btnPhotosAction(sender:)), for: .touchUpInside)
                tblCell.txtFld.delegate = self
                tblCell.txtFld.tag = indexPath.row
                return tblCell
                
            }
            
            let tblCell = tableView.dequeueReusableCell(withIdentifier: "IncognitoFormCell", for: indexPath) as! IncognitoFormCell
            //            if indexPath.row == 0 {
            //                tblCell.txtFldForm.tag = 101
            //            }else {
            //                tblCell.txtFldForm.tag = 102
            //            }
            tblCell.lblHeader.text =  arrTrashHeader[indexPath.row]
            tblCell.txtFldForm.tag = indexPath.row
            tblCell.txtFldForm.delegate = self
            
            if tblCell.lblHeader.text == "" {
                tblCell.isHidden = true
            }else {
                tblCell.isHidden = false
            }
            
            if tblCell.txtFldForm.tag == 0{
                tblCell.txtFldForm.delegate = self
                tblCell.lblHeader.text = ""
                tblCell.txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Category ?",
                                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                tblCell.txtFldForm.setIconOnTextFieldRight(UIImage(named: "dropDownArrow")!)
                tblCell.txtFldForm.inputView = UIView()
                
                tblCell.txtFldForm.text = formFieldData[indexPath.row].textFldStr
                
                //tblCell.txtFldForm.inputView = datePicker
            }
            else {
                tblCell.txtFldForm.attributedPlaceholder = NSAttributedString(string: "  Typehere...",
                                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                tblCell.txtFldForm.inputView = nil
                tblCell.lblHeader.text = arrTrashHeader[indexPath.row]
                //formData.text1 = tblCell.txtFldForm.text
                tblCell.txtFldForm.text = formFieldData[indexPath.row].textFldStr
                tblCell.txtFldForm.setIconOnTextFieldRight(UIImage(named: "textfieldblank")!)
                
            }
            
            return tblCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrTrashHeader[indexPath.row] == "" {
            return 0
        }
        if tableView.tag == 101 {
            return 40
        }
        if indexPath.row == 4 {
            return 50
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 101 {
            
            let path = IndexPath(item: 0, section: 0)
            let cell = self.tblForm.cellForRow(at: path) as! IncognitoFormCell
            cell.txtFldForm.text = ARRTRASHCATEGORY[indexPath.row]
            formFieldData[0].textFldStr = cell.txtFldForm.text
            
            incogForumData.category = ARRTRASHCATEGORY[indexPath.row]
            if categoryVw != nil {
                categoryVw.removeFromSuperview()
                categoryVw = nil
            }
        }
    }
}

extension FourmFormQuestionVC : UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            if categoryVw  == nil {
                self.view.endEditing(true)
                var window = UIApplication.shared.keyWindow!
                //  let v = UIView(frame: window.bounds)
                //            window.addSubview(v);
                //            v.backgroundColor = UIColor.clear
                categoryVw = UIView(frame: CGRect(x: textField.frame.origin.x + textField.frame.size.width/2, y: tblForm.frame.origin.y + textField.frame.origin.y + textField.frame.size.height , width: 150, height: 240))
                categoryVw.backgroundColor = UIColor.clear
                categoryVw.layer.borderWidth = 1
                categoryVw.layer.borderColor = DataUtils.hexStringToUIColor(hex: "#2F6A69").cgColor
                window.addSubview(categoryVw)
                
                let tableView: UITableView = UITableView()
                tableView.register(UINib(nibName: "CategoryTblCell", bundle: nil), forCellReuseIdentifier: "CategoryTblCell")
                
                tableView.frame = CGRect(x: 0, y: 0, width: categoryVw.frame.size.width, height: 240)
                tableView.tag = 101
                tableView.dataSource = self
                tableView.delegate = self
                tableView.backgroundColor = UIColor.clear
                categoryVw.addSubview(tableView)
            }else {
                categoryVw.removeFromSuperview()
                categoryVw = nil
            }
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        //  for item in formFieldData {
        formFieldData[textField.tag].textFldStr = textField.text
        //  }
        switch textField.tag {
        case 0:
            incogForumData?.category = textField.text
            break
        case 1:
            incogForumData?.fourmAudience = textField.text
            break
            
        case 2:
            incogForumData?.fourmGoal = textField.text
            break
            
        case 3:
            incogForumData?.followLike = textField.text
            break
            
        case 4:
            incogForumData?.fourmName = textField.text
            break
            
        case 6:
            incogForumData?.fourmName = textField.text
            break
            
        case 7:
            incogForumData?.linkSite = textField.text
            break
            
        case 8:
            incogForumData?.customLink = textField.text
            break
            
        case 9:
            incogForumData?.titleLink = textField.text
            break
            
        case 10:
            incogForumData?.myWebsite = textField.text
            break
            
        case 11:
            incogForumData?.addUrl = textField.text
            break
        case 12:
            incogForumData?.socialLink = textField.text
            break
        case 13:
                incogForumData?.profileName = textField.text
            break
        case 14:
                incogForumData?.fourmImage = textField.text
            break
        case 15:
                incogForumData?.forumNotes = textField.text
            break
        default:
            incogForumData?.socialLink = textField.text
            break
            
        }
    }
}


