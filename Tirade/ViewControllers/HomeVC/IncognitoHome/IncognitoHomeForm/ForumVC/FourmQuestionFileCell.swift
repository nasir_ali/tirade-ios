//
//  FourmQuestionFileCell.swift
//  Tirade
//
//  Created by admin on 15/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class FourmQuestionFileCell: UITableViewCell {

    @IBOutlet weak var btnPhoto : UIButton!
    @IBOutlet weak var btnPdf : UIButton!
    @IBOutlet weak var lblTxtHeader : UILabel!
    @IBOutlet weak var txtFld : UITextField!


    override func awakeFromNib() {
        super.awakeFromNib()
         DataUtils.addBorder(view: txtFld, cornerRadius: 14, borderWidth: 2, borderColor: DataUtils.hexStringToUIColor(hex: "#D4AF37"))
              txtFld.attributedPlaceholder = NSAttributedString(string: "  Typehere...",
              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
