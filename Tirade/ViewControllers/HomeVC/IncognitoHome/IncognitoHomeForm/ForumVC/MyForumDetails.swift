//
//  MyForumDetails.swift
//  Tirade
//
//  Created by admin on 21/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class MyForumDetails: UIViewController {

    @IBOutlet var showDescriptions: [UILabel]!
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showTitle: UILabel!
    var forumDetail : IncognitoForum?


  override func viewDidLoad() {
        super.viewDidLoad()
    
    showDescriptions[0].text = forumDetail?.fourmAudience
    showDescriptions[1].text = forumDetail?.fourmGoal
    showDescriptions[2].text = forumDetail?.followLike
    showDescriptions[3].text = forumDetail?.fourmName
    showDescriptions[4].text = forumDetail?.myWebsite
    showDescriptions[5].text = forumDetail?.addUrl
    showDescriptions[6].text = forumDetail?.socialLink
    showDescriptions[7].text = forumDetail?.profileName
    }
    @IBAction func btnBackction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
        }
}
