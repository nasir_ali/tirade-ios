//
//  PlayerTbCell.swift
//  Tirade
//
//  Created by Lucky on 08/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import AVKit
class PlayerTbCell: UITableViewCell {
    var player:AVPlayer!
    var playerLayer:AVPlayerLayer!
    let controller = AVPlayerViewController()
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var likeButtonAction: UIButton!
    @IBOutlet weak var comentButtonAction: UIButton!
    @IBOutlet weak var shareButtonAction : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func playVideo(videoURL:URL,play:Bool){
        player = AVPlayer(url: videoURL)
        controller.player=player
        controller.view.frame = playerView.bounds
        playerView.addSubview(controller.view)
        controller.player?.pause()
//        if !play {
//            controller.player?.pause()
//        }
    }
}
