//
//  IncognitoHomeDetailVC.swift
//  Tirade
//
//  Created by admin on 07/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import EzPopup

class IncognitoHomeDetailVC: TiradeBaseVC,NumberPickerViewControllerDelegate {
    
    @IBOutlet var roundViews: [UIView]!
    @IBOutlet var showDescriptions: [UILabel]!
    @IBOutlet var showTitles: [UILabel]!
    @IBOutlet var showCounts: [UILabel]!
    @IBOutlet var showSubText: [UILabel]!
    @IBOutlet var showFeatureImages: [UIImageView]!
    @IBOutlet var dataViews: [UIView]!
    @IBOutlet var dataTitles: [UILabel]!
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showTitle: UILabel!
    @IBOutlet weak var postContent: UITextView!

    @IBOutlet var vwAllBase: [UIView]!
    @IBOutlet weak var txtPdf: UITextView!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var vwPlayer: UIView!
    let pickerVC = NumberPickerViewController.instantiate()

    
    var tiradeDetails = TrashHome()
    let lblDecArray = ["Like","Dislike","Boo","Comment","Agree","Cheer","Disagree"]
    let lblIconArray = ["agreeThumb","disagreeThumb","boo","comment.png","agree.png","Cheer.png","dis_agree"]
    var titleCate = ["Category?","Who?","What?"]
    var dataTitle = [String]()
    var showTitlesWho = [String]()
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        if tiradeDetails.video_path != "" {
           // playVideoUrl(url:(trashData?[sender.tag].video_path)!)
            
        }else  if tiradeDetails.audio_path != "" {
           // playAudioUrl(url:(trashData?[sender.tag].audio_path)!)
            
        }else  if tiradeDetails.image_path != "" {
          //  openPhotos(url:(trashData?[sender.tag].image_path)!)
                imgPost.kf.setImage(with: URL(string: (tiradeDetails.image_path)!)!)
        }else  if tiradeDetails.pdf_path != "" {
          //  openPDF(url:(trashData?[sender.tag].pdf_path)!)
            
        }else  if tiradeDetails.txtfile_path != "" {
            
            var url : NSString = (tiradeDetails.txtfile_path) as! NSString
            var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            var searchURL : NSURL = NSURL(string: urlStr as String)!
            var pdfUrlll = searchURL as? NSURL
            
            
            var arrayClients = [[String:String]]() // do not use NSMutableArray in Swift
            var dictClients = [String:String]()
            
            if let url = pdfUrlll {
                do {
                    let data = try Data(contentsOf:url as URL)
                    let attibutedString = try NSAttributedString(data: data, documentAttributes: nil)
                    let fullText = attibutedString.string
                    // let readings = fullText.components(separatedBy: CharacterSet.newlines)
                    txtPdf.text = fullText
                } catch {
                    print(error)
                }
            }
        }else {
            print("nothing selected")
        }
        dataTitle.append("Post")
        dataTitle.append("Description")
        showTitlesWho.append(tiradeDetails.category ?? "")
        showTitlesWho.append(tiradeDetails.who ?? "")
        showTitlesWho.append(tiradeDetails.what ?? "")

        if tiradeDetails.post_type?.uppercased() == "TRASH" {
            showTitle.text = "TRASH TALK"
        }else {
            showTitle.text = tiradeDetails.post_type?.uppercased()
        }
        
        
        
        if tiradeDetails.post_content != nil {
            postContent.text = tiradeDetails.post_content
        }
        if tiradeDetails.post_type?.lowercased() == "trash".lowercased() {
            showImage.image = UIImage(named: "trashform")
           titleCate = ["Post Category?","Who are you Trashing ?","What?"]
        }else if tiradeDetails.post_type?.lowercased() == "debate".lowercased() {
            showImage.image = UIImage(named: "debateform")
            titleCate = ["Post Category?","Who are you Debating ?","What?"]

        }else if tiradeDetails.post_type?.lowercased() == "Whistle Blower".lowercased() {
            showImage.image = UIImage(named: INCOGNITOCALLOUTIMGFORM)
            titleCate = ["Post Category?","Who are you blowing the whistle on ?","What?"]

        }else if tiradeDetails.post_type?.lowercased() == "jokes".lowercased() {
            showImage.image = UIImage(named: "jokesform")
            titleCate = ["Category?","How are you Joking ?","What?"]

        }
        
        for i in 0..<showTitlesWho.count {
                 showDescriptions[i].text = showTitlesWho[i]
               }
        for i in 0..<dataTitle.count {
          dataTitles[i].text = dataTitle[i]
        }
        
        for i in 0..<titleCate.count {
            showTitles[i].text = titleCate[i]
        }
        for i in 0..<lblDecArray.count {
            showSubText[i].text = lblDecArray[i]
        }
        for i in 0..<showFeatureImages.count {
            showFeatureImages[i].image = UIImage(named: lblIconArray[i])
        }
        
        for i in 0..<lblDecArray.count {
            switch i {
            case 0:
                showCounts[0].text  = "\(tiradeDetails.s_agree?.count ?? 0)"
            case 1:
                showCounts[1].text = "\(tiradeDetails.s_disagree?.count ?? 0)"
            case 2:
                showCounts[2].text = "\(tiradeDetails.boo?.count ?? 0)"
            case 3:
                showCounts[3].text = "\(tiradeDetails.comments?.count ?? 0)"
            case 4:
                showCounts[4].text = "\(tiradeDetails.agree?.count ?? 0)"
            case 5:
                showCounts[5].text = "\(tiradeDetails.cheers?.count ?? 0)"
            case 6:
                showCounts[6].text = "\(tiradeDetails.disagree?.count ?? 0)"
                
            default:
                showCounts[0].text = ""
            }
        }

        
        
        for v in roundViews{
            v.layer.cornerRadius = 10
            v.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        }
        for v in showTitles{
            v.layer.cornerRadius = 10
            v.layer.masksToBounds = true
           // v.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        }
        for v in showDescriptions{
            v.layer.cornerRadius = 10
            v.layer.masksToBounds = true
           // v.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        }
        for v in dataTitles{
            v.layer.cornerRadius = 10
            v.layer.masksToBounds = true
           // v.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        }
        
        for v in vwAllBase{
            v.layer.cornerRadius = 10
            v.layer.masksToBounds = true
           // v.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        }
    }
    func numberPickerViewController(sender: NumberPickerViewController, didSelectNumber selectedIDs: [String]) {
              dismiss(animated: true) {
               
               }
          }
    
    @IBAction func detailsLikeCommentActions(_ sender: UIButton) {

        guard let pickerVC = pickerVC else { return }
        pickerVC.isSelectFromDetail = true
        pickerVC.delegate = self

        switch sender.tag {
        case 0:
            pickerVC.titleHere = "Total Likes"
            pickerVC.arrDetailsLike = tiradeDetails.s_agree
        case 1:
            pickerVC.titleHere = "Total DisLikes"
            pickerVC.arrDetailsLike = tiradeDetails.disagree
        case 2:
            pickerVC.titleHere = "Total Boo"
            pickerVC.arrDetailsLike = tiradeDetails.boo
        case 3:
            pickerVC.titleHere = ""
        // pickerVC.arrDetailsLike = tiradeDetails.comments
        case 4:
            pickerVC.titleHere = "Total Agree"
            pickerVC.arrDetailsLike = tiradeDetails.agree
        case 5:
            pickerVC.titleHere = "Total Cheer"
            pickerVC.arrDetailsLike = tiradeDetails.cheers
        case 6:
            pickerVC.titleHere = "Total Disagree"
            pickerVC.arrDetailsLike = tiradeDetails.s_disagree
        default:
            pickerVC.titleHere = "Total Disagree"
            pickerVC.arrDetailsLike = tiradeDetails.s_disagree
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateArrayData"), object: nil, userInfo: ["selectedTitleArr":pickerVC.arrDetailsLike,"selectedTitle" : pickerVC.titleHere])

              let popupVC = PopupViewController(contentController: pickerVC, position: .center(CGPoint(x: showTitle.frame.origin.x  , y: sender.frame.origin.y - 120)), popupWidth: 200, popupHeight: 300)
              popupVC.canTapOutsideToDismiss = true
              popupVC.cornerRadius = 5
              present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnBackction(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
}
