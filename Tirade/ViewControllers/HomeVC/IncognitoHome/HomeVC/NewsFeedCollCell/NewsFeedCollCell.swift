//
//  NewsFeedCollCell.swift
//  Tirade
//
//  Created by admin on 01/06/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class NewsFeedCollCell: UICollectionViewCell {
   
      @IBOutlet weak var lblUserTitle: UILabel!
      @IBOutlet weak var imgUser: UIImageView!
      @IBOutlet weak var postImg: UIImageView!

      @IBOutlet weak var lblDate: UILabel!
      @IBOutlet weak var lblContent: UILabel!
      @IBOutlet weak var vwBaseMedia: UIView!
      @IBOutlet weak var imgVideoFile: UIImageView!

    override func awakeFromNib() {
        imgUser.layer.cornerRadius = imgUser.frame.size.height/2
        vwBaseMedia.layer.cornerRadius = 10
        vwBaseMedia.layer.masksToBounds = true
        vwBaseMedia.layer.borderWidth = 0
        vwBaseMedia.layer.borderColor = UIColor.white.cgColor
    }
}
