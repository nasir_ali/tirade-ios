//
//  PublicChoosePlatformVC.swift
//  Tirade
//
//  Created by admin on 17/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class PublicChoosePlatformVC: TiradeBaseVC {
    
    
    @IBOutlet weak var btnMad: UIButton!
    @IBOutlet weak var btnTirade: UIButton!
    @IBOutlet weak var btnBlack: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnMad(_ sender: UIButton) {
        let loginStoryBoard = UIStoryboard(name: "MadSkills", bundle: nil).instantiateViewController(withIdentifier: "MadskillsIntroVC") as! MadskillsIntroVC
        self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        
    }
    @IBAction func btnTrashTalk(_ sender: UIButton) {
        let value = TiradeUserDefaults.getIncognitoCredential(key: "incognitoLoggedIn")
        if value {
            let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IncognitoHomeVC") as! IncognitoHomeVC
            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        }else {
            let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewVC") as! SignUpViewVC
            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        }
        
    }
    @IBAction func btnTirade(_ sender: UIButton) {
        let loginStoryBoard = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeHomeVC") as! TiradeHomeVC
        self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        
    }
    @IBAction func btnBlack(_ sender: UIButton) {
        let loginStoryBoard = UIStoryboard(name: "BlackMarket", bundle: nil).instantiateViewController(withIdentifier: "BlackMarketIntroVC") as! BlackMarketIntroVC
        self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
