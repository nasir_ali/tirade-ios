//
//  SignUpViewVC.swift
//  Tirade
//
//  Created by RISHIKET on 16/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit



class SignUpViewVC: TiradeBaseVC, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var tblZip: UITableView!
    @IBOutlet weak var vwBaseSelectionZip: UIView!
    
    @IBOutlet weak var goOnlineBtn: UIButton!
    @IBOutlet weak var txtFldName: UITextField!
    @IBOutlet weak var txtFldAge: UITextField!
    @IBOutlet weak var txtFldZipcode: UITextField!
    @IBOutlet weak var btnEditProfilePic: UIButton!
    var txtFldGender: String!
    
    var imagePicker: ImagePicker!
    var photosData : String?
    @IBOutlet weak var vwChoosUpload: UIView!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    
    @IBOutlet weak var imgAgeSelection: UIImageView!
    @IBOutlet weak var btnAgeSelection: UIButton!
    @IBOutlet weak var btnZipcodeSelection: UIButton!
    
    @IBOutlet weak var stackviewAge: UIStackView!
    
   
    @IBOutlet weak var lblRefName: UILabel!
    @IBOutlet weak var lblRefAge: UILabel!
    @IBOutlet weak var lblRefZipcode: UILabel!
    @IBOutlet weak var lblRefZender: UILabel!
    
    @IBOutlet weak var collRadonPic: UICollectionView!
    @IBOutlet weak var vwMain: UIView!
    var randomSelectedImg = String()
    
    
    var collProfileImg = ["img1","img2","img3","img4","img5","img6","img7","img8","img9","img10","img11","img12","img13","img14","img15","img16","img17","img18","img19","img20"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        vwMain.isHidden = true
        self.navigationController?.isNavigationBarHidden = true

        txtFldGender = "Male"
        btnMale.setImage(UIImage(named: "checkTik"), for: .normal)
        btnEditProfilePic.layer.cornerRadius = btnEditProfilePic.frame.size.height/2
        btnEditProfilePic.layer.masksToBounds = true
        //        txtFldName.rightViewMode = UITextFieldViewMode.always
        //        txtFldName.rightView = UIImageView(image: UIImage(named: "dropDownArrow"))
        goOnlineBtn.layer.cornerRadius = goOnlineBtn.frame.size.height/2
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    func setUpFonts() {
        txtFldZipcode.font = UIFont(name: "adrip", size: 20)
        txtFldName.font = UIFont(name: "adrip", size: 20)
        txtFldAge.font = UIFont(name: "adrip", size: 20)
        lblRefAge.font = UIFont(name: "adrip", size: 20)
        lblRefName.font = UIFont(name: "adrip", size: 20)
        lblRefZender.font = UIFont(name: "ebrima", size: 20)
        lblRefZipcode.font = UIFont(name: "adrip", size: 20)
        btnMale.titleLabel?.font =  UIFont(name: "adrip", size: 20)
        btnFemale.titleLabel?.font =  UIFont(name: "adrip", size: 20)
        btnOthers.titleLabel?.font =  UIFont(name: "adrip", size: 20)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        hideOthers()
        self.vwMain.isHidden = true
    }
    
    func hideOthers()  {
        imgAgeSelection.isHidden = true
        stackviewAge.isHidden = true
        vwChoosUpload.isHidden = true
        btnAgeSelection.isSelected = false
        btnZipcodeSelection.isSelected = false
        btnEditProfilePic.isSelected = false
        vwBaseSelectionZip.isHidden = true
    }
    
    @IBAction func btnSelectProfileImgAction(_ sender: UIButton) {
        hideOthers()
        sender.isSelected = !sender.isSelected
        //        imagePicker.delegate = self
        //        imagePicker.sourceType = .savedPhotosAlbum
        //        imagePicker.allowsEditing = false
        //
        //        present(imagePicker, animated: true, completion: nil)
        vwChoosUpload.isHidden = sender.isSelected ? false : true
        
        
    }
    
    @IBAction func btnSelectAgeAction(_ sender: Any) {
        hideOthers()
    }
    
    //    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    //
    //
    //
    //    }
    
    
    //    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
    //         self.dismiss(animated: true, completion: { () -> Void in
    //            self.btnEditProfilePic.setImage(image, for: .normal)
    //         })
    //
    //
    //     }
    
    @IBAction func goOnlineBtnAction(_ sender: UIButton) {
        hideOthers()
        if photosData == nil {
            let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getUser_Image(key: "user_Image"), options: .ignoreUnknownCharacters)!
            if  dataDecoded.count > 0 {
                let imageFromData: UIImage = UIImage(data:dataDecoded)!
                photosData = (imageFromData.pngData()?.base64EncodedString())
            }
        }


        let url = BASEURL + INCOGNITOSIGNUPURL
        var dataDict = [String:Any]()
        if photosData != nil {
            TiradeUserDefaults.setUser_Name(value: txtFldName.text!) 

             dataDict = ["name" : txtFldName.text!,"age" : txtFldAge.text!,"zip" : txtFldZipcode.text!,"gender":txtFldGender!,"user_profile":photosData!] as [String : Any]

        }else {
            TiradeUserDefaults.setUser_Name(value: txtFldName.text!)

             dataDict = ["name" : txtFldName.text!,"age" : txtFldAge.text!,"zip" : txtFldZipcode.text!,"gender":txtFldGender!,"user_profile":""] as [String : Any]

        }
        
        ServerRequest.doPostRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) {
            TiradeUserDefaults.setIncognitCredential(key: "incognitoLoggedIn")
            TiradeSingleTon.shared.isLoggedINUserIncognito = true
                let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GoIncognitoVC") as! GoIncognitoVC
                           self.navigationController?.pushViewController(loginStoryBoard, animated: true)
           
        }
    }
    
    @IBAction func chooseProfilePicAction(_ sender: UIButton) {
        hideOthers()
        self.imagePicker.present(from: sender)
        vwChoosUpload.isHidden = true
    }
    
    @IBAction func chooseRandonPicAction(_ sender: UIButton) {
       vwMain.isHidden = false
        
        hideOthers()
    }
    @IBAction func btnSelectZipcode(_ sender: UIButton) {
        hideOthers()
        sender.isSelected = !sender.isSelected
        vwBaseSelectionZip.isHidden = sender.isSelected ? false : true
    }
    
    @IBAction func btnSelectMaleAction(_ sender: UIButton) {
        hideOthers()
        btnMale.setImage(UIImage(named: "checkTik"), for: .normal)
        btnFemale.setImage(UIImage(named: "questionBack"), for: .normal)
        btnOthers.setImage(UIImage(named: "questionBack"), for: .normal)
        txtFldGender = "Male"
    }
    @IBAction func btnSelectFemaleAction(_ sender: UIButton) {
        hideOthers()
        btnFemale.setImage(UIImage(named: "checkTik"), for: .normal)
        btnMale.setImage(UIImage(named: "questionBack"), for: .normal)
        btnOthers.setImage(UIImage(named: "questionBack"), for: .normal)
        txtFldGender = "Female"
        
    }
    @IBAction func btnSelectOthersAction(_ sender: UIButton) {
        hideOthers()
        btnOthers.setImage(UIImage(named: "checkTik"), for: .normal)
        btnMale.setImage(UIImage(named: "questionBack"), for: .normal)
        btnFemale.setImage(UIImage(named: "questionBack"), for: .normal)
        txtFldGender = "other"
        
    }
    
    @IBAction func btnAgeSelection18to25(_ sender: UIButton) {
        hideOthers()
        txtFldAge.text = "18 - 28"
    }
    
    @IBAction func btnAgeSelection26to38(_ sender: UIButton) {
        hideOthers()
        txtFldAge.text = "29 - 38"
    }
    
    @IBAction func btnAgeSelection39(_ sender: UIButton) {
        hideOthers()
        txtFldAge.text = "39 - 49"
    }

       @IBAction func btnAgeSelection50to60(_ sender: UIButton) {
             hideOthers()
             txtFldAge.text = "50 - 60"
         }
       @IBAction func btnAgeSelection61(_ sender: UIButton) {
             hideOthers()
             txtFldAge.text = "61+"
         }
    
    @IBAction func btnAgeClicked(_ sender: UIButton) {
        hideOthers()
        sender.isSelected = !sender.isSelected
        if imgAgeSelection.isHidden {
            imgAgeSelection.isHidden = false
            stackviewAge.isHidden = false
        }else {
            imgAgeSelection.isHidden = true
            stackviewAge.isHidden = true
        }
        
    }
    
    @IBAction func btnZipcodeClicked(_ sender: UIButton) {
        hideOthers()
        sender.isSelected = !sender.isSelected
        vwBaseSelectionZip.isHidden = sender.isSelected ? false : true
    }
    @IBAction func btnBack(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
}

extension SignUpViewVC: ImagePickerDelegate {
    func uploadVideoToServer(videoUrl: String, videoName: String) {
        
    }
    
    func selectedImgFormat(imageFormat: String) {
        
    }
    
    
    func didSelect(image: UIImage?) {
        if image != nil {
            self.btnEditProfilePic.setImage(image, for: .normal)

            TiradeUserDefaults.setUser_Image(value: (image!.pngData()?.base64EncodedString())!)

                  let imageData:Data = image!.pngData()!
                  let imageStr = imageData.base64EncodedString()
                  photosData = imageStr
        }else {

        }
       }
  }

extension SignUpViewVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ZIPCODEARR.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tblCell = tblZip.dequeueReusableCell(withIdentifier: "SelectZipcodeCell", for: indexPath) as! SelectZipcodeCell
        tblCell.lblSelectZip.text = ZIPCODEARR[indexPath.row]
        return tblCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideOthers()
        txtFldZipcode.text = ZIPCODEARR[indexPath.row]
    }
}

extension SignUpViewVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 10
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          return CGSize(width: 40, height: 40)
      }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RandomPicCell", for: indexPath) as! RandomPicCell
        collectionCell.imgPic.image = UIImage(named: collProfileImg[indexPath.row])
        return collectionCell
    }
    
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let image = UIImage(named: collProfileImg[indexPath.row])
        randomSelectedImg = collProfileImg[indexPath.row]
        self.btnEditProfilePic.setImage(UIImage(named: collProfileImg[indexPath.row]), for: .normal)
        self.vwMain.isHidden = true
        TiradeUserDefaults.setUser_Image(value: ((image!.pngData()?.base64EncodedString())!))
    }
}
