//
//  AgreeDisagreeVC.swift
//  Tirade
//
//  Created by videotap ios on 13/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class AgreeDisagreeVC: TiradeBaseVC {

    @IBOutlet weak var btnIagree: UIButton!
    @IBOutlet weak var btnDisagree: UIButton!
    @IBOutlet weak var descriptionV: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        descriptionV.text = AGREEDISAGREEDEC
        self.navigationController?.isNavigationBarHidden = true

        btnIagree.layer.cornerRadius = btnIagree.frame.size.height/2
        btnDisagree.layer.cornerRadius = btnDisagree.frame.size.height/2

        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func btnIAgreeAction(_ sender: UIButton) {
        let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgreeDisagreeIncognito") as! AgreeDisagreeIncognito

        self.navigationController?.pushViewController(loginStoryBoard, animated: true)

    }
    
    @IBAction func btnIDisagreeAction(_ sender: UIButton) {
    }
    
}
