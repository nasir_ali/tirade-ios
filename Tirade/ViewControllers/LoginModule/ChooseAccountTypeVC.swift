//
//  ChooseAccountTypeVC.swift
//  Tirade
//
//  Created by RISHIKET on 15/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import AVKit

class ChooseAccountTypeVC: TiradeBaseVC {
    @IBOutlet weak var vwPlayer: UIView!
    var player : AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.navigationController?.isNavigationBarHidden = true
        let bundle = Bundle.main
        let moviePath = bundle.path(forResource: "chooseAccount", ofType: "mp4")
        // let width = vwPlayer.frame.size.width
        let height: CGFloat = vwPlayer.frame.size.height
        player = AVPlayer(url: URL(fileURLWithPath: moviePath ?? ""))
        let playerLayer = AVPlayerLayer()
        playerLayer.player = player
        playerLayer.frame = CGRect(x: 0.0, y: 0, width: UIScreen.main.bounds.size.width, height: height + 30)
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.videoGravity = .resizeAspectFill
        vwPlayer.layer.addSublayer(playerLayer)
        player!.play()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            self.player!.seek(to: CMTime.zero)
            self.player!.play()
        }
        // DataUtils.addPlayer(view: vwPlayer, url: "chooseAccount")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if player != nil {
            player?.play()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if player != nil {
            NotificationCenter.default.removeObserver(self)
            player?.pause()
            player?.replaceCurrentItem(with: nil)
        }
    }
    
    @IBAction func chooseAccountIncognito(_ sender: UIButton) {
        TiradeSingleTon.shared.isIncognitoUserSelection = true
        let value = TiradeUserDefaults.getIncognitoCredential(key: "incognitoLoggedIn")
        if value {
            let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IncognitoHomeVC") as! IncognitoHomeVC
            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        }else {
            let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewVC") as! SignUpViewVC
            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        }
    }
    
    @IBAction func chooseAccountPublic(_ sender: UIButton) {
        TiradeSingleTon.shared.isIncognitoUserSelection = false
        let value = TiradeUserDefaults.getPublicCredential(key: "PublicLoggedInUser")
        if value {
            let loginStoryBoard = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeHomeVC") as! TiradeHomeVC
            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        }else {
            let loginStoryBoard = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "PublicLoginVC") as! PublicLoginVC
            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        }
    }
}
