//
//  GoSelectionVC.swift
//  Tirade
//
//  Created by videotap ios on 13/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import AVKit

class GoSelectionVC: TiradeBaseVC {
    
    @IBOutlet weak var btnGoOutlet: UIButton!
    @IBOutlet weak var vwPlayer: UIView!
    var player : AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.navigationController?.isNavigationBarHidden = true
        btnGoOutlet.layer.cornerRadius = btnGoOutlet.frame.size.height/2
        self.navigationController?.isNavigationBarHidden = true
        let bundle = Bundle.main
        let moviePath = bundle.path(forResource: "goSelection", ofType: "mp4")
        // let width = vwPlayer.frame.size.width
        let height: CGFloat = vwPlayer.frame.size.height
        player = AVPlayer(url: URL(fileURLWithPath: moviePath ?? ""))
        let playerLayer = AVPlayerLayer()
        playerLayer.player = player
        playerLayer.frame = CGRect(x: 0.0, y: 0, width: UIScreen.main.bounds.size.width, height: height + 30)
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.videoGravity = .resizeAspectFill
        vwPlayer.layer.addSublayer(playerLayer)
        player!.play()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            
            self.player!.seek(to: CMTime.zero)
            self.player!.play()
           }
        // DataUtils.addPlayer(view: vwPlayer, url: "chooseAccount")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if player != nil {
            player?.play()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if player != nil {
            NotificationCenter.default.removeObserver(self)
            player?.pause()
            player?.replaceCurrentItem(with: nil)
        }
    }
    
    @IBAction func goSelectionAction(_ sender: UIButton) {
        
        let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgreeDisagreeVC") as! AgreeDisagreeVC
        self.navigationController?.pushViewController(loginStoryBoard, animated: false)
    }
}
