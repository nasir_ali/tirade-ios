//
//  AgreeDisagreeIncognito.swift
//  Tirade
//
//  Created by RISHIKET on 16/02/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class AgreeDisagreeIncognito: TiradeBaseVC {

    @IBOutlet weak var btnIagree: UIButton!
    @IBOutlet weak var btnDisagree: UIButton!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        btnIagree.layer.cornerRadius = btnIagree.frame.size.height/2
        btnDisagree.layer.cornerRadius = btnDisagree.frame.size.height/2

    }
 
    @IBAction func btnIAgreeAction(_ sender: UIButton) {
        let loginStoryBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
          self.navigationController?.pushViewController(loginStoryBoard, animated: true)

      }
      
      @IBAction func btnIDisagreeAction(_ sender: UIButton) {
      }
}
