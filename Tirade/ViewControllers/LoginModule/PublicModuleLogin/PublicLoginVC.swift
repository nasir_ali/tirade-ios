//
//  PublicLoginVC.swift
//  Tirade
//
//  Created by admin on 12/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class PublicLoginVC: TiradeBaseVC{
    var imagePicker: ImagePicker!
    
    @IBOutlet weak var goOnlineBtn: UIButton!
    @IBOutlet weak var btnEditProfilePic: UIButton!
    @IBOutlet weak var tblPublicSignUp: UITableView!
    @IBOutlet weak var zipCodeSelectionPicker: UIPickerView!
    @IBOutlet weak var zipCodeVwHeightCons: NSLayoutConstraint!
    
    let datePicker = UIDatePicker()
    //let zipCodeSelectionPicker = UIPickerView()
    
    let toolbar = UIToolbar()
    var txtDatePicker: UITextField!
    var txtZipCode: UITextField!
    
    let signUpData = PublicSignUp()
    
    var tblCellTxtFldIcons = ["userName","userName","userPhone","userEmail","userPassword","userBirthday","userZipCode","userGender"]
    var tblCellTxtFldPlaceholders = ["firstName","lastName","userPhone","Email Address","Password","Birthday","Zip Code","Gender"]
    var fileInfoType = [FileTypeInfo]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        self.navigationController?.isNavigationBarHidden = true
        signUpData.gender = "Male"
        zipCodeVwHeightCons.constant = 0
        goOnlineBtn.layer.cornerRadius = goOnlineBtn.frame.size.height/2
        btnEditProfilePic.layer.cornerRadius = btnEditProfilePic.frame.size.height/2
        btnEditProfilePic.layer.masksToBounds = true
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)
        
    }
    
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
        
        let fileData = FileTypeInfo()
        fileData.fileData = dictFile["fileData"] as! Data
        fileData.fileName = dictFile["fileName"] as! String
        fileData.uploadFileKey = "user_profile"//dictFile["uploadFileKey"] as! String
        fileData.mimeType = dictFile["mimeType"] as! String
        fileInfoType.append(fileData)
        //        var arr = [FileTypeInfo]()
        //        arr.append(fileData)
        //
        //        formData.fileInfoType.append(fileData)
        //        print(formData.fileInfoType)
        //        formData.fileInfoType[1] = fileData
        //        formData.fileInfoType[2] = fileData
        //        formData.fileInfoType[3] = fileData
        if fileData.fileData != nil {
            let imageFromData: UIImage = UIImage(data:fileData.fileData)!
            self.btnEditProfilePic.setImage(imageFromData, for: .normal) //image = imageFromData
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectProfileImgAction(_ sender: UIButton) {
        //self.imagePicker.present(from: sender)
        AttachmentHandler.shared.showImageLibraryList(vc: self)
    }
    
    @IBAction func goOnlineBtnAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let dataDict = ["name" : signUpData.firstName!,"phone" : signUpData.phone!,"email" : signUpData.email!,"password":signUpData.password!,"birthday":signUpData.birthday!,"zip":signUpData.zipCode!,"gender" : signUpData.gender] as [String : Any]
        print(dataDict)
        let url = BASEURL + PUBlICSIGNUPURL
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: fileInfoType, parameters: dataDict, onCompletion: { (data) in
            do {
                guard let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject] else {
                    return
                }
                DispatchQueue.main.async {
                    DataUtils.removeLoader()
                }
                if let dictFromJSON = jsonResult as? [String:AnyObject] {
                    let locData = dictFromJSON["message"] as? String
                    let status = dictFromJSON["status"] as? Int
                    let userID = dictFromJSON["user"] as? [String:AnyObject]
                    
                    if locData != nil{
                        DispatchQueue.main.async {
                            if self.fileInfoType.count > 0 {
                                if self.fileInfoType[0].fileData != nil {
                                    TiradeUserDefaults.setTiradeUser_Image(value: (self.fileInfoType[0].fileData.base64EncodedString()))
                                }
                            }

                            TiradeUserDefaults.setTiradeUser_Name(value: self.signUpData.firstName!)
                            
                            DataUtils.showAlert(title: "", msg: locData!, selfObj: self) {
                                if status == 0 {
                                    
                                }else {
                                    if TiradeSingleTon.shared.isIncognitoUserSelection {
                                        if TiradeUserDefaults.getUser_ID(key: "user_id")  == "noid" {
                                            print("useriduseriduserid\(userID?["user_id"] as! String)")
                                            TiradeUserDefaults.setUser_ID(value: userID?["user_id"] as! String)
                                        }
                                    }
                                    
                                    if TiradeUserDefaults.getTiradeUser_ID(key: "userTirade_id")  == "noid" {
                                        TiradeUserDefaults.setTiradeUser_ID(value: userID?["user_id"] as! String)
                                    }
                                    
                                    TiradeUserDefaults.setPublicCredential(key : "PublicLoggedInUser")
                                    let loginStoryBoard = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "TiradeHomeVC") as! TiradeHomeVC
                                    self.navigationController?.pushViewController(loginStoryBoard, animated: true)
                                }
                            }
                        }
                    }
                }
            }catch  {
                
                return
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
        
        
        //        ServerRequest.doPostRequestToServer(url, data: dataDict as Dictionary<String, AnyObject>, onController: self) {
        //            TiradeUserDefaults.setPublicCredential(key : "PublicLoggedInUser")
        //
        //            let loginStoryBoard = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "PublicChoosePlatformVC") as! PublicChoosePlatformVC
        //            self.navigationController?.pushViewController(loginStoryBoard, animated: true)
        //        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.4) {
            self.zipCodeVwHeightCons.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}

extension PublicLoginVC: ImagePickerDelegate {
    func uploadVideoToServer(videoUrl: String, videoName: String) {
        
    }
    
    func selectedImgFormat(imageFormat: String) {
        
    }
    
    
    func didSelect(image: UIImage?) {
        self.btnEditProfilePic.setImage(image, for: .normal)
    }
}

extension PublicLoginVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblCellTxtFldIcons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == (tblCellTxtFldIcons.count - 1) {
            let tblGenderCell = tblPublicSignUp.dequeueReusableCell(withIdentifier: "GenderSelectionCell", for: indexPath) as! GenderSelectionCell
            tblGenderCell.btnMaleSelect.tag = 0
            tblGenderCell.btnFemaleSelect.tag = 1
            tblGenderCell.btnOthersSelect.tag = 2
            tblGenderCell.btnMaleSelect.addTarget(self, action: #selector(selectionGender), for: .touchUpInside)
            tblGenderCell.btnFemaleSelect.addTarget(self, action: #selector(selectionGender), for: .touchUpInside)
            tblGenderCell.btnOthersSelect.addTarget(self, action: #selector(selectionGender), for: .touchUpInside)
            switch signUpData.gender {
            case "Male":
                tblGenderCell.btnMaleSelect.setImage(UIImage(named: "checkTik"), for: .normal)
                tblGenderCell.btnFemaleSelect.setImage(UIImage(named: "questionBack"), for: .normal)
                tblGenderCell.btnOthersSelect.setImage(UIImage(named: "questionBack"), for: .normal)
            case "Female":
                tblGenderCell.btnMaleSelect.setImage(UIImage(named: "questionBack"), for: .normal)
                tblGenderCell.btnFemaleSelect.setImage(UIImage(named: "checkTik"), for: .normal)
                tblGenderCell.btnOthersSelect.setImage(UIImage(named: "questionBack"), for: .normal)
            case "other":
                tblGenderCell.btnMaleSelect.setImage(UIImage(named: "questionBack"), for: .normal)
                tblGenderCell.btnFemaleSelect.setImage(UIImage(named: "questionBack"), for: .normal)
                tblGenderCell.btnOthersSelect.setImage(UIImage(named: "checkTik"), for: .normal)
            default:
                tblGenderCell.btnMaleSelect.setImage(UIImage(named: "checkTik"), for: .normal)
                tblGenderCell.btnFemaleSelect.setImage(UIImage(named: "questionBack"), for: .normal)
                tblGenderCell.btnOthersSelect.setImage(UIImage(named: "questionBack"), for: .normal)
            }
            
            return tblGenderCell
        }
        let tblCell = tblPublicSignUp.dequeueReusableCell(withIdentifier: "PublicSignUpCell", for: indexPath) as! PublicSignUpCell
        
        if indexPath.row == 5 {
            tblCell.userTextField.inputAccessoryView = toolbar
            tblCell.userTextField.inputView = datePicker
        }else {
            tblCell.userTextField.inputAccessoryView = nil
            tblCell.userTextField.inputView = nil
        }
        if indexPath.row == 6 {
            //            tblCell.userTextField.inputAccessoryView = toolbar
            //            tblCell.userTextField.inputView = zipCodeSelectionPicker
           // txtZipCode = signUpData.zipCode
            tblCell.btnClick.isHidden = false
            tblCell.btnClick.isUserInteractionEnabled = true
            tblCell.btnClick.tag = 6
            // tblCell.btnClick.addTarget(self, action: #selector(selectZipCode(sender:)), for: .touchUpInside)
            
        }
        //tblCell.userTextField.text = signUpData[indexPath.row]
        tblCell.userTextField.tag = indexPath.row
        tblCell.btnClick.tag = indexPath.row
        tblCell.userTextField.placeholder = tblCellTxtFldPlaceholders[indexPath.row]
        tblCell.userTextField.setIconOnTextFieldLeft(UIImage(named: tblCellTxtFldIcons[indexPath.row])!)
        if indexPath.row ==  5  {
            tblCell.userTextField.setIconOnTextFieldRight(UIImage(named: "dropDownArrow")!)
            
        }else {
            tblCell.userTextField.setIconOnTextFieldRight(UIImage(named: "signuptxt")!)

        }
        switch tblCell.userTextField.tag {
        case 0:
            tblCell.userTextField.text = signUpData.firstName
        case 1:
           tblCell.userTextField.text = signUpData.lastName
        case 2:
           tblCell.userTextField.text = signUpData.phone
        case 3:
           tblCell.userTextField.text = signUpData.email
        case 4:
           tblCell.userTextField.text = signUpData.password
        case 5:
           tblCell.userTextField.text = signUpData.birthday
        case 6:
           tblCell.userTextField.text = signUpData.zipCode 
        default:
           tblCell.userTextField.text = ""
        }
        
        
        
        tblCell.userTextField.delegate = self
        
        return tblCell
    }
    
    @IBAction func doneZipCode(sender : UIButton)  {
        UIView.animate(withDuration: 0.0) {
            self.zipCodeVwHeightCons.constant = 0
            self.view.layoutIfNeeded()
        }
        doneZipPicker()
    }
    
    @IBAction func cancelZipCode(sender : UIButton)  {
        UIView.animate(withDuration: 0.0) {
            self.zipCodeVwHeightCons.constant = 0
            self.view.layoutIfNeeded()
        }
        //cancelZipPicker()
    }
    
    
    @objc func selectZipCode(sender : UIButton)  {
        UIView.animate(withDuration: 0.4) {
            self.zipCodeVwHeightCons.constant = 180
        }
        //        txtZipCode.inputAccessoryView = toolbar
        //        txtZipCode.inputView = zipCodeSelectionPicker
        //        txtZipCode.becomeFirstResponder()
        if sender.tag == 6 {
            self.view.endEditing(true)
            showZipCodeSelection()
        }
    }
    
    @objc func selectionGender(sender : UIButton)  {
        switch sender.tag {
        case 0:
            signUpData.gender = "Male"
        case 1:
            signUpData.gender = "Female"
        case 2:
            signUpData.gender = "other"
        default:
            signUpData.gender = "Male"
        }
        
        let indexPath = IndexPath(item: tblCellTxtFldIcons.count - 1, section: 0)
        tblPublicSignUp.reloadRows(at: [indexPath], with: .none)
        
        // tblPublicSignUp.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == (tblCellTxtFldIcons.count - 1) {
            return 100
        }
        
        return 70
    }
    
    func showZipCodeSelection()  {
        //Formate Date
        //zipCodeSelectionPicker.mo = .date
        //ToolBar
        zipCodeSelectionPicker.delegate = self
        zipCodeSelectionPicker.dataSource = self
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(doneZipPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(cancelZipPicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    }
    
    @objc func doneZipPicker()  {
        let selectedValue = ZIPCODEARR[zipCodeSelectionPicker.selectedRow(inComponent: 0)]
        txtZipCode.text = selectedValue
        signUpData.zipCode = selectedValue
        self.view.endEditing(true)
    }
    
    @objc func cancelZipPicker()  {
        self.view.endEditing(true)
        
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        txtDatePicker.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func setIconInTableUser(cell : PublicSignUpCell ,icon : UIImage)  {
        cell.userTextField.tintColor = UIColor.white
        cell.userTextField.setIconOnTextFieldLeft(icon)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension PublicLoginVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        UIView.animate(withDuration: 0.0) {
            self.zipCodeVwHeightCons.constant = 0
            self.view.layoutIfNeeded()
        }
        if textField.tag == 2 {
            textField.keyboardType = .phonePad
        }
        if textField.tag == 5 {
            txtDatePicker = textField
            showDatePicker()
        }
        if textField.tag == 6 {
            textField.keyboardType = .phonePad
            // cancelZipPicker()
            //  txtZipCode = textField
            //  showZipCodeSelection()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        switch textField.tag {
        case 0:
            signUpData.firstName = textField.text
        case 1:
            signUpData.lastName = textField.text
        case 2:
            signUpData.phone = textField.text
        case 3:
            signUpData.email = textField.text
        case 4:
            signUpData.password = textField.text
        case 5:
            signUpData.birthday = textField.text
        case 6:
            signUpData.zipCode = textField.text
        default:
            signUpData.firstName = ""
        }
    }
}

extension PublicLoginVC : UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ZIPCODEARR.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ZIPCODEARR[row]
    }
}





