//
//  GenderSelectionCell.swift
//  Tirade
//
//  Created by admin on 18/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class GenderSelectionCell: UITableViewCell {

    @IBOutlet weak var vwBaseGender : UIView!
    @IBOutlet weak var txtFldGender : UITextField!
    
    @IBOutlet weak var btnMaleSelect : UIButton!
    @IBOutlet weak var btnFemaleSelect : UIButton!
    @IBOutlet weak var btnOthersSelect : UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        vwBaseGender.layer.cornerRadius = 10
        txtFldGender.setIconOnTextFieldLeft(UIImage(named: "userName")!)
    }
}
