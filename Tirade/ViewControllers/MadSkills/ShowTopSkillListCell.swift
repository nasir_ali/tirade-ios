//
//  ShowTopSkillListCell.swift
//  Tirade
//
//  Created by Lucky on 23/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class ShowTopSkillListCell: UITableViewCell {

    @IBOutlet weak var showText: UILabel!
    @IBOutlet weak var showImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
