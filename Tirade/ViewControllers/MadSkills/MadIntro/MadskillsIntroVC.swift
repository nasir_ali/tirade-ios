//
//  MadskillsIntroVC.swift
//  Tirade
//
//  Created by admin on 13/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class MadskillsIntroVC: UIViewController {

    @IBOutlet weak var btnGoOutlet: UIButton!
    @IBOutlet weak var vwPlayer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        btnGoOutlet.layer.cornerRadius = btnGoOutlet.frame.size.height/2
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DataUtils.player(view:vwPlayer, url:Bundle.main.path(forResource:"madSkills", ofType: "mp4")!, playContinue:true)
    }
      
      override func viewDidDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
        globalPlayer.player?.pause()
      }
      
      @IBAction func goSelectionAction(_ sender: UIButton) {
          
          let loginStoryBoard = UIStoryboard(name: "MadSkills", bundle: nil).instantiateViewController(withIdentifier: "MadskillsPostVC") as! MadskillsPostVC
          self.navigationController?.pushViewController(loginStoryBoard, animated: false)
      }
}
