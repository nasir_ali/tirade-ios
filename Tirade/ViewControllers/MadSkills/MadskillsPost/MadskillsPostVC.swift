//
//  MadskillsPostVC.swift
//  Tirade
//
//  Created by admin on 13/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import MobileCoreServices
class MadskillsPostVC: UIViewController,GetChooseSkill,UINavigationControllerDelegate {
    @IBOutlet weak var vwSocial: UIView!
    @IBOutlet weak var tableView: UITableView!
    var selectedSkill = "Choose your skill"
    
    var formData = MadSkillsPostCategory()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwSocial?.backgroundColor = UIColor(white: 1, alpha: 0.3)
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)
        
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if formData.business_category != nil {
            if formData.post == "" {
                DataUtils.showAlert(title: "", msg: "please write a post", selfObj: self) {
                }
            }else {
                let dataDict = ["user_id" :  TiradeUserDefaults.getUser_ID(key: "user_id"),
                                "business_category":formData.business_category,"business_name":formData.business_name,"experience":formData.experience,"youRepresent":formData.youRepresent,"height":formData.height,"weight":formData.weight,"otherSkills":formData.otherSkills as Any] as [String : Any]
                sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
            }
        }
        else {
        }
//        let madVC = UIStoryboard(name: "MadSkills", bundle: nil).instantiateViewController(withIdentifier: "MadSkillsHomeVC") as! MadSkillsHomeVC
//        self.navigationController?.pushViewController(madVC, animated: true)

    }
    @objc func chooseSkills(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"ChooseSkillsVC") as! ChooseSkillsVC
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(vc, animated:true, completion: {
            
        })
    }
    func skill(value: String) {
        selectedSkill = value
        formData.business_category = selectedSkill
        tableView.reloadData()
        
    }
    @objc func openProfileCamera(){
        AttachmentHandler.shared.showImageLibraryList(vc: self)
    }
    @objc func openCamera(){
        AttachmentHandler.shared.showImageLibraryList(vc: self)
        
    }
    @objc func openVideo(){
        AttachmentHandler.shared.showVideoList(vc: self)
        
    }
    @objc func openMusic(){
        let voiceVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
        voiceVC.fileNameAudio = randomString(length: 8)
        self.navigationController?.present(voiceVC, animated: true, completion: {
            
        })
    }
    
    @objc func openFile(){
        AttachmentHandler.shared.showFileLibraryList(vc: self, fileType: "pdf")
        
    }
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
        
        let fileData = FileTypeInfo()
        fileData.fileData = dictFile["fileData"] as! Data
        fileData.fileName = dictFile["fileName"] as! String
        fileData.uploadFileKey = dictFile["uploadFileKey"] as! String
        fileData.mimeType = dictFile["mimeType"] as! String
        
        var arr = [FileTypeInfo]()
        arr.append(fileData)
        
        formData.fileInfoType.append(fileData)
        print(formData.fileInfoType)
    }
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + MADSKILLPOSTDATA
        
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: formData.fileInfoType, parameters: dataDict, onCompletion: { (data) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    DataUtils.showAlert(title: "", msg: "Post Successfully Uploaded.", selfObj: self) {
                        let madVC = UIStoryboard(name: "MadSkills", bundle: nil).instantiateViewController(withIdentifier: "MadSkillsHomeVC") as! MadSkillsHomeVC
                        self.navigationController?.pushViewController(madVC, animated: true)
                    }
                }
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
    }
}


extension MadskillsPostVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let buttonCell = tableView.dequeueReusableCell(withIdentifier:"ButtonCell") as! ButtonCell
        let textfieldCell = tableView.dequeueReusableCell(withIdentifier:"TextFieldCell") as! TextFieldCell
        let textViewCell = tableView.dequeueReusableCell(withIdentifier:"TextViewCell") as! TextViewCell
        buttonCell.t1.delegate = self
        buttonCell.t2.delegate = self
        buttonCell.t3.delegate = self
        textfieldCell.textField.delegate = self
        if indexPath.row == 0 {
            textfieldCell.chooseSkills.setTitle(selectedSkill, for:.normal)
            textfieldCell.chooseSkills.addTarget(self, action: #selector(chooseSkills), for:.touchUpInside)
            textfieldCell.openCamera.addTarget(self, action:#selector(openCamera), for:.touchUpInside)
            textfieldCell.openProfileCam.addTarget(self, action:#selector(openProfileCamera), for:.touchUpInside)
            textfieldCell.openMusic.addTarget(self, action:#selector(openMusic), for:.touchUpInside)
            textfieldCell.openVideo.addTarget(self, action:#selector(openVideo), for:.touchUpInside)
            textfieldCell.openFile.addTarget(self, action:#selector(openFile), for:.touchUpInside)
            
            return textfieldCell
        }else if indexPath.row == 5 {
            return textViewCell
        }else{
            if indexPath.row == 1 {
                buttonCell.showText.text = "Experience:"
                buttonCell.t1.placeholder = "Barely"
                buttonCell.t2.placeholder = "I'mOK"
                buttonCell.t3.placeholder = "Lots"
                //                buttonCell.b1.setTitle("Barely", for:.normal)
                //                buttonCell.b2.setTitle("I'mOK", for:.normal)
                //                buttonCell.b3.setTitle("Lots", for:.normal)
                return buttonCell
            }
            if indexPath.row == 2 {
                buttonCell.showText.text = "YouRepresent:"
                buttonCell.t1.placeholder = "City"
                buttonCell.t2.placeholder = "State"
                buttonCell.t3.placeholder = "Country"
                //                buttonCell.b1.setTitle("City", for:.normal)
                //                buttonCell.b2.setTitle("State", for:.normal)
                //                buttonCell.b3.setTitle("Country", for:.normal)
                return buttonCell
            }
            if indexPath.row == 3 {
                buttonCell.showText.text = "Height(choose_one)"
                buttonCell.t1.placeholder = "ft_Inch"
                buttonCell.t2.placeholder = "StillGroing"
                buttonCell.t3.placeholder = "other"
                //                buttonCell.b1.setTitle("ft_Inch", for:.normal)
                //                buttonCell.b2.setTitle("StillGroing", for:.normal)
                //                buttonCell.b3.setTitle("other", for:.normal)
                return buttonCell
            }
            if indexPath.row == 4 {
                buttonCell.showText.text = "Weight(choose_one)"
                buttonCell.t1.placeholder = "Ibs/kilos"
                buttonCell.t2.placeholder = "NotEnough"
                buttonCell.t3.placeholder = "tooMuch"
                //                buttonCell.b1.setTitle("Ibs/kilos", for:.normal)
                //                buttonCell.b2.setTitle("NotEnough", for:.normal)
                //                buttonCell.b3.setTitle("tooMuch", for:.normal)
                return buttonCell
                
            }
            return buttonCell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 {
            return 130
        }
        return UITableView.automaticDimension
    }
}

protocol GetChooseSkill {
    func skill(value: String)
}

extension MadskillsPostVC : UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.placeholder == "Barely" || textField.placeholder == "Barely" || textField.placeholder == "Barely" {
            formData.experience = textField.text
        }
        if textField.placeholder == "City" || textField.placeholder == "State" || textField.placeholder == "Country" {
            formData.youRepresent = textField.text
        }
        if textField.placeholder == "ft_Inch" || textField.placeholder == "StillGroing" || textField.placeholder == "other" {
            formData.height = textField.text
        }
        if textField.placeholder == "Ibs/kilos" || textField.placeholder == "NotEnough" || textField.placeholder == "tooMuch" {
            formData.weight = textField.text
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func textViewDidEndEditing(_ textView: UITextView) {
      
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        formData.post = textView.text
        return true
    }
}
