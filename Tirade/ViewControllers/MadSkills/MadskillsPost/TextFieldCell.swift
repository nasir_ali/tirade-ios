//
//  TextFieldCell.swift
//  vridhi
//
//  Created by Lucky on 30/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var openCamera: UIButton!
    @IBOutlet weak var openVideo: UIButton!
    @IBOutlet weak var openFile: UIButton!
    @IBOutlet weak var openMusic: UIButton!
    @IBOutlet weak var openProfileCam: UIButton!

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var showText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        DataUtils.addBorder(view: textField, cornerRadius: 14, borderWidth: 2, borderColor: DataUtils.hexStringToUIColor(hex: "#D4AF37"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet weak var chooseSkills: UIButton!
}
