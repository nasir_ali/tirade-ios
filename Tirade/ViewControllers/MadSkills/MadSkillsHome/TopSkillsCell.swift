//
//  TopSkillsCell.swift
//  vridhi
//
//  Created by Lucky on 26/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class TopSkillsCell: UICollectionViewCell {
    
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var showRightText: UILabel!
    @IBOutlet weak var showRightTitle: UILabel!
    @IBOutlet weak var showRightImage: UIImageView!
    @IBOutlet weak var showMiddleImage: UIImageView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var showLeftText: UILabel!
    @IBOutlet weak var showLeftTitle: UILabel!
    @IBOutlet weak var showLeftImage: UIImageView!
    @IBOutlet weak var showTitle: UILabel!
    @IBOutlet weak var backGroundColor: UIView!
    @IBOutlet weak var backGroundImage: UIImageView!
    
    @IBOutlet weak var left2LikeText: UILabel!
    @IBOutlet weak var left1DislikeText: UILabel!
}
