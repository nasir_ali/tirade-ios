//
//  SkillsTableCell.swift
//  vridhi
//
//  Created by Lucky on 26/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class SkillsTableCell: UITableViewCell {
    @IBOutlet weak var backGroundImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backGroundColor: UIView!
    var topSkills = [MadTopSkill]()

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension SkillsTableCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:UIScreen.main.bounds.width/2, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topSkills.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let topSkill = collectionView.dequeueReusableCell(withReuseIdentifier:"TopSkillsCell", for: indexPath) as! TopSkillsCell
        topSkill.showLeftText.text = topSkills[indexPath.row].id
        return topSkill
    }
    
    
}
