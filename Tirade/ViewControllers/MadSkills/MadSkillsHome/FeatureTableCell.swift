//
//  FeatureTableCell.swift
//  vridhi
//
//  Created by Lucky on 26/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class FeatureTableCell: UITableViewCell {

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var underLine: UIView!
    @IBOutlet weak var showFeatureImage: UIImageView!
    @IBOutlet weak var showdate: UILabel!
    @IBOutlet weak var showTitle: UILabel!
    @IBOutlet weak var showSubText: UILabel!
    @IBOutlet weak var showImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.clipsToBounds = true
        roundView.layer.cornerRadius = 40
        roundView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        menuView.isHidden = true

    }
    @IBOutlet weak var menuView: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func addAction(_ sender: UIButton) {
        addButton.isSelected = !addButton.isSelected
        print(addButton.isSelected)
        if addButton.isSelected{
            menuView.isHidden = false
        }
        else {
            menuView.isHidden = true
        }
    }
    @IBAction func menuAction(_ sender: UIButton) {
   
    
    }
    
    @IBAction func likeAction(_ sender: UIButton) {
  
    }
    
    @IBAction func dislikeAction(_ sender: Any) {
    }
    @IBAction func messageAction(_ sender: UIButton) {
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
    }
    func displayData(madSkill:MadSkillsData){
        DispatchQueue.main.async {
            if let url = URL(string: madSkill.business_image!){
                if let data = try? Data(contentsOf: url){
                    self.showImage.image = UIImage(data: data)
                }
            }
        }
        showTitle.text = madSkill.business_name
        showdate.text = madSkill.created_date      
    }
}
