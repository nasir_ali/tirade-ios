//
//  FindSkillTableCell.swift
//  vridhi
//
//  Created by Lucky on 27/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class FindSkillTableCell: UITableViewCell {

    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var backGroundColor: UIView!
    @IBOutlet weak var backGroundImage: UIImageView!
    var topSkills = [MadTopSkill]()
    var viewController = UIViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        CollectionView.delegate = self
        CollectionView.dataSource = self
        startTimer()

    }
    
    func startTimer() {

        let timer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }


    @objc func scrollAutomatically(_ timer1: Timer) {

        if let coll  = CollectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)! < topSkills.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)

                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }

            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
extension FindSkillTableCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:UIScreen.main.bounds.width/5, height:100)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topSkills.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let findSkill = collectionView.dequeueReusableCell(withReuseIdentifier:"FindSkillsCell", for: indexPath) as! FindSkillsCell
        findSkill.showText.setTitle(topSkills[indexPath.row].name, for: .normal)
        return findSkill
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let VC =  viewController.storyboard?.instantiateViewController(withIdentifier:"ShowTopSkillVC") as! ShowTopSkillVC
        VC.id = topSkills[indexPath.row].id  
        viewController.navigationController?.pushViewController(VC, animated: false)
    }
    
}
