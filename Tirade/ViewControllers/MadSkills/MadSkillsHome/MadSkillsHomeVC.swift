//
//  MadSkillsHomeVC.swift
//  Tirade
//
//  Created by admin on 02/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import JJFloatingActionButton

class MadSkillsHomeVC: TiradeBaseVC {

    @IBOutlet weak var btnFloating: UIButton!
    @IBOutlet weak var btnBackVW: UIView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    
    @IBOutlet weak var imgMember: UIImageView!
    @IBOutlet weak var lblMemberID: UILabel!
    

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnSearch: UILabel!
    var topSkills = [MadTopSkill]()
    var madSkillsData = [MadSkillsData]()
    var rateTopSkills = [MadTopSkill]()

    
    var actionButton = JJFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

          NotificationCenter.default.addObserver(self,selector: #selector(tiradecirclebtnClicked(notification:)),name: NSNotification.Name(rawValue: "tiradecirclebtnClicked"), object: nil)
         
    }
    override func viewDidAppear(_ animated: Bool) {
        //MARK:- Hit API to get data
      getRateTopSkills()
    }
    
    @IBAction func drawerIconTapped(_ sender: UIButton) {
        let viewController22 = LeftSideViewController(nibName: "LeftSideViewController", bundle: nil)
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: viewController22)
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    @IBAction func btnFloatingAction(_ sender: UIButton) {
          sender.isSelected = !sender.isSelected
          if sender.isSelected {
              actionButton.tintColor = UIColor.white
              setFloatingMenu(actionButton: actionButton)
              actionButton.isHidden = false
              
              
          }else {
              actionButton.items.removeAll()
              actionButton.removeFromSuperview()
              actionButton.isHidden = true
              
          }
      }
      
      func setFloatingMenu(actionButton : JJFloatingActionButton)  {
          
          
          actionButton.addItem(title: "", image: UIImage(named: "picture")?.withRenderingMode(.alwaysOriginal)) { item in
              // do something
          }
          
          actionButton.addItem(title: "", image: UIImage(named: "themeIcon")?.withRenderingMode(.alwaysOriginal)) { item in
              // do something
          }
          
          actionButton.addItem(title: "", image: UIImage(named: "voiceIcon")?.withRenderingMode(.alwaysOriginal)) { item in
              // do something
          }
          actionButton.addItem(title: "", image: UIImage(named: "fileIcon")?.withRenderingMode(.alwaysOriginal)) { item in
              // do something
          }
          actionButton.addItem(title: "", image: UIImage(named: "postIcon")?.withRenderingMode(.alwaysOriginal)) { item in
              // do something
          }
          actionButton.addItem(title: "", image: UIImage(named: "goLiveIcon")?.withRenderingMode(.alwaysOriginal)) { item in
              let commingSoonVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PublisherViewController") as! PublisherViewController
              
              self.navigationController?.present(commingSoonVC, animated: true, completion: nil)
          }
          
          view.addSubview(actionButton)
          actionButton.translatesAutoresizingMaskIntoConstraints = false
          
          
          
          
          
          actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
          actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
          //  actionButton.bottomAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
          //actionButton.trailingAnchor.constraint(equalTo: btnFloating.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
          
          actionButton.handleSingleActionDirectly = true
          actionButton.buttonDiameter = btnFloating.frame.size.width
          
          actionButton.overlayView.backgroundColor = UIColor(white: 1, alpha: 0.1)
          actionButton.buttonImageSize = CGSize(width: 30, height: 30)
          actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 180)
          
          //        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "X")!)
          //       actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
          
          //       actionButton.layer.shadowColor = UIColor.black.cgColor
          //       actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
          //       actionButton.layer.shadowOpacity = Float(0.4)
          //       actionButton.layer.shadowRadius = CGFloat(2)
          
      }
      
      
      @IBAction func btnHomeAction(_ sender: UIButton) {
          
      }
      
      @IBAction func btnLogOutAction(_ sender: UIButton) {
          let domain = Bundle.main.bundleIdentifier!
          UserDefaults.standard.removePersistentDomain(forName: domain)
          UserDefaults.standard.synchronize()
          print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
          TiradeUserDefaults.setUser_ID(value: "noid")
          
          let chooseAcc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
          self.navigationController?.pushViewController(chooseAcc, animated: false)
          
          
      }
      
      @IBAction func btnSettingAction(_ sender: UIButton) {
          let settingVC = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
          self.navigationController?.pushViewController(settingVC, animated: false)
          
          
      }
      @IBAction func btnNotificationAction(_ sender: UIButton) {
          let notification = UIStoryboard(name: "PublicModule", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
          self.navigationController?.pushViewController(notification, animated: false)
      }
      @IBAction func btnMoreAction(_ sender: UIButton) {
          commingSoon()
      }
      func commingSoon()  {
          let commingSoonVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CommingSoonVC") as! CommingSoonVC
          self.navigationController?.present(commingSoonVC, animated: true, completion: {
          })
      }
      
      @IBAction func btnSearchAction(_ sender: UIButton) {
          commingSoon()
      }
    @objc private func tiradecirclebtnClicked(notification: NSNotification){
           let goSelection = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseAccountTypeVC") as! ChooseAccountTypeVC
           self.navigationController?.pushViewController(goSelection, animated: false)
       }
    func getMadskillsData()  {
            ServerRequest.doGetRequestToServer(BASEURL + MADSKILLGETDATA, onController: self) { (responseData) in
                let arr : [String : [MadSkillsData]]  = try! JSONDecoder().decode([String: [MadSkillsData]].self, from: responseData)
                               print(arr)
                self.madSkillsData = arr["data"]!
                DispatchQueue.main.async {
                    DataUtils.removeLoader()
                     self.tableView.reloadData()
                }
            }
    }
    func getTopSkills(){
        ServerRequest.doGetRequestToServer(BASEURL + MADTOPSKILL, onController: self) { (responseData) in
            
            let arr : [String : [MadTopSkill]]  = try! JSONDecoder().decode([String: [MadTopSkill]].self, from: responseData)
            print(arr)
            self.topSkills = (arr["data"])!
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                self.tableView.reloadData()
                self.getMadskillsData()
            }
        }
    }
    func getRateTopSkills(){
          ServerRequest.doGetRequestToServer(BASEURL + RATETOPSKILL, onController: self) { (responseData) in
            print("-----------------------------------------------------------")
            print("respones",responseData)
            print("-----------------------------------------------------------")
            let arr : [String : [MadRateTopSkill]]  = try! JSONDecoder().decode([String: [MadRateTopSkill]].self, from: responseData)
            print(arr)

            DispatchQueue.main.async {
                DataUtils.removeLoader()
                self.tableView.reloadData()
                self.getTopSkills()
            }
            
//
//              let arr : [String : [MadTopSkill]]  = try! JSONDecoder().decode([String: [MadTopSkill]].self, from: responseData)
//              print(arr)
//              self.topSkills = (arr["data"])!
//

          }

      }
}
extension MadSkillsHomeVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return madSkillsData.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let skillCell = tableView.dequeueReusableCell(withIdentifier:"SkillsTableCell") as! SkillsTableCell
        let findSkill = tableView.dequeueReusableCell(withIdentifier:"FindSkillTableCell") as! FindSkillTableCell
        let featureCell = tableView.dequeueReusableCell(withIdentifier:"FeatureTableCell") as! FeatureTableCell
        if indexPath.row == 0 {
            
            return skillCell
        }
        if indexPath.row == 1 {
            findSkill.topSkills = topSkills
            findSkill.viewController = self
            DispatchQueue.main.async {
                findSkill.CollectionView.reloadData()
            }
            return findSkill
        }
        else {
           // if let _ = madSkillsData[exist:indexPath.row - 2] {
             featureCell.displayData(madSkill:madSkillsData[indexPath.row - 2])
          //  }
             
            return featureCell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1 {
            return 150
        }
        return UITableView.automaticDimension
    }
}

