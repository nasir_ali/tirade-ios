//
//  ChooseSkillsVC.swift
//  Tirade
//
//  Created by Lucky on 23/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class ChooseSkillsVC: UIViewController {
    var delegate: GetChooseSkill?
    @IBOutlet weak var tableView: UITableView!
    var topSkills = [MadTopSkill]()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        getTopskill()
    }
        @IBAction func closeButton(_ sender: UIButton) {
            self.dismiss(animated: true, completion: {
                
            })
        }
               func getTopskill()  {
                        ServerRequest.doGetRequestToServer(BASEURL + MADTOPSKILL, onController: self) { (responseData) in
                            
                            let arr : [String : [MadTopSkill]]  = try! JSONDecoder().decode([String: [MadTopSkill]].self, from: responseData)
                                           print(arr)
                            self.topSkills = (arr["data"])!
                            DispatchQueue.main.async {
                                DataUtils.removeLoader()
                                self.tableView.reloadData()
                            }
                             
                        }
                }
    
}
extension ChooseSkillsVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topSkills.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"ChooseSkills") as! ChooseSkills
        cell.showText.text = topSkills[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = self.delegate {
            delegate.skill(value: topSkills[indexPath.row].name!)
        }
        self.dismiss(animated: true, completion: {
                       
                   })
    }
}
