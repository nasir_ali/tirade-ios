//
//  ShowTopSkillVC.swift
//  Tirade
//
//  Created by Lucky on 22/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class ShowTopSkillVC: UIViewController {

    var id:String!
    @IBOutlet weak var tablView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getTopskill()
    }
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated:true)
    }
    
    func getTopskill()  {
        print(id)
        //        https://vridhisoftech.in/tirade/madeskills/postdata/get_skills_detail/
        ServerRequest.doPostDataRequestToServer(BASEURL + MADSKILLDETAIL, data:["business_category":id] as [String:AnyObject], onController:self) {(respones) in
            
//            let arr : [String : [MadTopSkill]]  = try! JSONDecoder().decode([String: [MadTopSkill]].self, from: responseData)
            print(respones, try? JSONSerialization.jsonObject(with: respones, options: []) as! [String:AnyObject])
            DispatchQueue.main.async {
                DataUtils.removeLoader()
//                self.tablView.reloadData()
            }
        }
    }
}
extension ShowTopSkillVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"ShowTopSkillListCell") as! ShowTopSkillListCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
