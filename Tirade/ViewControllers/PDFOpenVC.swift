//
//  PDFOpenVC.swift
//  Tirade
//
//  Created by admin on 14/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import PDFKit

class PDFOpenVC: TiradeBaseVC {

    @IBOutlet var pdfView: PDFView!
    var pdfURL = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")
        var url : NSString = pdfURL as NSString
        var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        var searchURL : NSURL = NSURL(string: urlStr as String)!
        var pdfUrlll = searchURL
        if let pdfDocument = PDFDocument(url: pdfUrlll as URL) {
                pdfView.displayMode = .singlePageContinuous
                pdfView.autoScales = true
                pdfView.displayDirection = .vertical
                pdfView.document = pdfDocument
        }
        
    }
    
    @IBAction func btnDownPressed(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
       }
}
