//
//  TiradeBaseVC.swift
//  Tirade
//
//  Created by admin on 12/05/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import EFInternetIndicator

class TiradeBaseVC: UIViewController,InternetStatusIndicable {
        
        var internetConnectionIndicator:InternetViewIndicator?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.startMonitoringInternet()
        }
    }
