//
//  OpenRTFfileVC.swift
//  Tirade
//
//  Created by admin on 14/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class OpenRTFfileVC: TiradeBaseVC {

  @IBOutlet var textView: UITextView!
    var rtfURL = String()

    @IBAction func btnDownPressed(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
       }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        var url : NSString = rtfURL as NSString
             var urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
             var searchURL : NSURL = NSURL(string: urlStr as String)!
            var pdfUrlll = searchURL as? NSURL
        
        
        var arrayClients = [[String:String]]() // do not use NSMutableArray in Swift
        var dictClients = [String:String]()

        if let url = pdfUrlll {
            do {
                let data = try Data(contentsOf:url as URL)
                let attibutedString = try NSAttributedString(data: data, documentAttributes: nil)
                let fullText = attibutedString.string
               // let readings = fullText.components(separatedBy: CharacterSet.newlines)
                textView.text = fullText
            } catch {
                print(error)
            }
        }
        
    }
}
