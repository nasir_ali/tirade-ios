//
//  NotificationVC.swift
//  Tirade
//
//  Created by admin on 11/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class NotificationVC: TiradeBaseVC {
    
    @IBOutlet var tblNotification : UITableView!
    @IBOutlet var btnSearch : UIButton!
    @IBOutlet var searchBar : UISearchBar!
    
    var debateNoti : [DebateNotification]?
    var dictNotification : [String:Any]?
    var newCopyDebateArr = [DebateNotification]()
    var isSearchActive = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet(backgroundColor:UIColor.gray, style: .cardView, textColor:UIColor.white, message:"please check your internet connection", remoteHostName: "")

        dictNotification = [String:Any]()
        dictNotification = ["Debate Invitation's":debateNoti]
       
       // debateNoti = [DebateNotification]()
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        }
    }
    @IBAction func btnBackction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        btnSearch.isSelected = !btnSearch.isSelected
        if btnSearch.isSelected {
            isSearchActive = true
            btnSearch.setTitle("Cancel", for: .normal)
            btnSearch.titleLabel?.tintColor = UIColor.red
            
        }else {
            isSearchActive = false
            btnSearch.setTitle("Search", for: .normal)
            btnSearch.titleLabel?.tintColor = UIColor.red
        }
        tblNotification.reloadData()
    }
}

extension NotificationVC : UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        filterContentForSearchText(searchText: searchBar.text!)
    }
    
    func filterContentForSearchText(searchText: String) {
        // Filter the array using the filter method
        if self.debateNoti == nil {
            // self.speciesSearchResults = nil
            return
        }
        
        newCopyDebateArr = self.debateNoti!.filter({( debateTxt: DebateNotification) -> Bool in
            // to start, let's just search by name
            return debateTxt.sender_username!.lowercased().range(of: searchText.lowercased()) != nil
        })
        
        print(newCopyDebateArr)
    }
    
}

extension NotificationVC : UITableViewDelegate,UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return dictNotification?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "NotificationTblHeader") as! NotificationTblHeader
        headerCell.lblLeftHeader.text = "Debate Invitation's"
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return isSearchActive ? newCopyDebateArr.count : debateNoti?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tblCell = tblNotification.dequeueReusableCell(withIdentifier: "NotificationTblCell", for: indexPath) as! NotificationTblCell
        let debateArr  = isSearchActive ? newCopyDebateArr : debateNoti!
        if (debateArr[indexPath.row].sender_user_profile) == "" || (debateArr[indexPath.row].sender_user_profile) == nil {
                   tblCell.imgNotification.image = UIImage(named: "user_profile_img")
               }else {
                   tblCell.imgNotification.kf.setImage(with: URL(string: debateArr[indexPath.row].sender_user_profile!)!)
               }
        tblCell.lblNotificationType.text = debateArr[indexPath.row].sender_username
        tblCell.lblTime.text = debateArr[indexPath.row].created_date
        tblCell.lblDescription.text = debateArr[indexPath.row].debate_reason
        return tblCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showAlert(alertMsg: debateNoti![indexPath.row].debate_reason!)
    }
    
    func showAlert(alertMsg : String) {
        var alertController = UIAlertController(title: "Debate Invitation", message: alertMsg, preferredStyle: .alert)
        
        
        alertController.addAction(UIAlertAction(title: "Accept", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.dismiss(animated: true) {
                self.showAlertWithDistructiveButton(msg: "Debate Accepted")
                
            }           }))
        
        alertController.addAction(UIAlertAction(title: "Reject", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.dismiss(animated: true) {
                self.showAlertWithDistructiveButton(msg: "Debate Rejected")
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Canel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.dismiss(animated: true) {
                
            }           }))
        
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertWithDistructiveButton(msg : String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.dismiss(animated: true) {
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
   
}



extension IncognitoHomeVC: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("The notification is about to be presented")
        
        completionHandler([.badge, .sound, .alert])
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let identifier = response.actionIdentifier
        
        switch identifier {
            
        case UNNotificationDismissActionIdentifier:
            print("Notification was dismissed")
            completionHandler()
            
        case UNNotificationDefaultActionIdentifier:
            print("The user opened the app from the notification")
            completionHandler()
            
        default:
            print("The default case was called")
            completionHandler()
        }
    }
}
