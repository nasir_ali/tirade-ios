//
//  NotificationTblCell.swift
//  Tirade
//
//  Created by admin on 12/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class NotificationTblCell: UITableViewCell {

    @IBOutlet var imgBack : UIImageView!
    @IBOutlet var imgNotification : UIImageView!

    @IBOutlet var lblNotificationType : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var lblDescription : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgBack.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
