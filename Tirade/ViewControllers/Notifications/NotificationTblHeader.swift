//
//  NotificationTblHeader.swift
//  Tirade
//
//  Created by admin on 12/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class NotificationTblHeader: UITableViewCell {

   @IBOutlet weak var lblLeftHeader : UILabel!
   @IBOutlet weak var btnClose : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
