//
//  BlackMarketVC.swift
//  vridhi
//
//  Created by Lucky on 23/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class BlackMarketVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

}
//MARK:- Tableview Delegate and DataSource
extension BlackMarketVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let blackMarketCell = tableView.dequeueReusableCell(withIdentifier:"BlackMarketTableCell") as! BlackMarketTableCell
        return blackMarketCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
extension BlackMarketVC:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let blackMarketCell = collectionView.dequeueReusableCell(withReuseIdentifier:"BlackMarketCollectionCell", for: indexPath) as! BlackMarketCollectionCell
        return blackMarketCell
    }
    
}
