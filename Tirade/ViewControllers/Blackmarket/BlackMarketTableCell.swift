//
//  BlackMarketTableCell.swift
//  vridhi
//
//  Created by Lucky on 23/04/20.
//  Copyright © 2020 Lucky. All rights reserved.
//

import UIKit

class BlackMarketTableCell: UITableViewCell {

    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var eventSubImage2: UIButton!
    @IBOutlet weak var eventSubImage1: UIButton!
    @IBOutlet weak var eventDescription: UILabel!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var mainEventImage: UIImageView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var showDate: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var backGroundColor: UIView!
    @IBOutlet weak var backGroundImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBAction func menuAction(_ sender: UIButton) {
    }
    @IBAction func flagAction(_ sender: UIButton) {
    }
    @IBAction func hideAction(_ sender: UIButton) {
    }
    @IBAction func favoriteAction(_ sender: UIButton) {
    }
    @IBAction func replayAction(_ sender: UIButton) {
    }
    @IBAction func notIntrestedAction(_ sender: UIButton) {
    }
    @IBAction func donotRecomAction(_ sender: UIButton) {
    }
    @IBAction func watchLaterAction(_ sender: UIButton) {
    }
    @IBAction func saveToPlaylistAction(_ sender: UIButton) {
    }
    @IBAction func downloadAction(_ sender: UIButton) {
    }
}
