//
//  PublisherViewController.swift
//  Tirade
//
//  Created by admin on 06/04/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import R5Streaming

class PublisherViewController: R5VideoViewController,R5StreamDelegate {
    
    @IBOutlet weak var liveView: UIView!

//    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {
//        let s =  String(format: "Status: %s (%@)",  r5_string_for_status(statusCode), msg)
//        NSLog(s)
//
//        if (Int(statusCode) == Int(r5_status_disconnected.rawValue)) {
//            self.cleanup()
//        }
//        else if (Int(statusCode) == Int(r5_status_video_render_start.rawValue)) {
//            NSLog("SUPPORT-482 %@", msg);
//        }
//    }
    

   @IBAction func btnCancel(_ sender: UIButton)  {
    self.dismiss(animated: true, completion: nil)
    }
    
     func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {
        
        if( Int(statusCode) == Int(r5_status_start_streaming.rawValue) ){
            let session : AVAudioSession = AVAudioSession.sharedInstance()
            let cat = session.category
            let opt = session.categoryOptions
        
            let s =  String(format: "AV: %@ (%d)",  cat.rawValue, opt.rawValue)
            print("ssssssssssssssssssssssssss\(s)")
        }
        else if (Int(statusCode) == Int(r5_status_buffer_flush_start.rawValue)) {
            NotificationCenter.default.post(Notification(name: Notification.Name("BufferFlushStart")))
        }
        else if (Int(statusCode) == Int(r5_status_buffer_flush_empty.rawValue)) {
            NotificationCenter.default.post(Notification(name: Notification.Name("BufferFlushComplete")))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {

           super.viewDidAppear(animated)
           
           AVAudioSession.sharedInstance().requestRecordPermission { (gotPerm: Bool) -> Void in
              
           };
           
           
           let vc = setupDefaultR5VideoViewController()
           let button = UIButton(frame: CGRect(x: 30, y: self.view.frame.size.height - 100, width: 100, height: 100))
               button.setTitle("Cancel", for: .normal)
               button.backgroundColor = UIColor.red
            vc.view.addSubview(button)
           
           // Set up the configuration
           let config = getConfig()
           // Set up the connection and stream
           let connection = R5Connection(config: config)
           
           setupPublisher(connection: connection!)
           // show preview and debug info
          // self.publishStream?.getVideoSource().fps = 2;
           self.currentView?.attach(publishStream!)
           
           self.publishStream?.publish("stream1", type: getPublishRecordType ())

       }
    
    var shouldClose : Bool = true
    var currentView : R5VideoViewController? = nil
    var publishStream : R5Stream? = nil
    var subscribeStream : R5Stream? = nil
    
    required init () {
       
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func cleanup () {
        if( self.publishStream != nil ) {
            self.publishStream!.client = nil
            self.publishStream?.delegate = nil
            self.publishStream = nil
        }
        
        if( self.subscribeStream != nil ) {
            self.subscribeStream!.client = nil
            self.subscribeStream?.delegate = nil
            self.subscribeStream = nil
        }
        self.removeFromParent()
    }
    
    func closeTest(){
        
        NSLog("closing view")

        if( self.publishStream != nil ){
            self.publishStream!.stop()
        }
        
        if( self.subscribeStream != nil ){
            self.subscribeStream!.stop()
        }
        
        // Moved to status disconnect, due to publisher emptying queue buffer on bad connections.
//        self.removeFromParentViewController()
    }
    
    func getConfig()->R5Configuration{
        // Set up the configuration
        let config = R5Configuration()
        config.host = "http://172.20.10.1:5080/"
        config.port = 49152
        config.contextName = "live"
        config.`protocol` = Int32(r5_rtsp.rawValue);
        config.buffer_time = 3333
        config.licenseKey = "33XE-8ESE-4TEJ-B0HG"
        return config
    }
    
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        if(currentView != nil){
            
            currentView?.setFrame(view.frame);
        }
        
    }
    
    func setupPublisher(connection: R5Connection){
        
        self.publishStream = R5Stream(connection: connection)
        self.publishStream!.delegate = self
        
    //    if(Testbed.getParameter(param: "video_on") as! Bool){
            // Attach the video from camera to stream
            let videoDevice = AVCaptureDevice.devices(for: AVMediaType.video).last as? AVCaptureDevice
            
            let camera = R5Camera(device: videoDevice, andBitRate: 750)
           
            camera?.width = 300
            camera?.height = 500
            camera?.fps = 15
            camera?.orientation = 90
            self.publishStream!.attachVideo(camera)
   //     }
   //     if(Testbed.getParameter(param: "audio_on") as! Bool){
            // Attach the audio from microphone to stream
            let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
            let microphone = R5Microphone(device: audioDevice)
            microphone?.bitrate = 32
            NSLog("Got device %@", String(describing: audioDevice?.localizedName))
            self.publishStream!.attachAudio(microphone)
   //     }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        AVAudioSession.sharedInstance().requestRecordPermission { (gotPerm: Bool) -> Void in
            
        };
        
        r5_set_log_level((Int32)(r5_log_level_debug.rawValue))
        
        self.view.autoresizesSubviews = false
        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//
//        super.viewDidAppear(animated)
//
//        //this is just to have a white background to the example
//        let backView : UIView = UIView(frame: self.view.frame);
//        backView.backgroundColor = UIColor.white;
//        self.view.addSubview(backView);
//
//    }
    
    func getPublishRecordType () -> R5RecordType {
        var type = R5RecordTypeLive
      //  if Testbed.getParameter(param: "record_on") as! Bool {
            type = R5RecordTypeRecord
//            if Testbed.getParameter(param: "append_on") as! Bool {
//                type = R5RecordTypeAppend
//            }
     //   }
        return type
    }
    
    func setupDefaultR5VideoViewController() -> R5VideoViewController{
        
        currentView = getNewR5VideoViewController(rect: self.view.frame)
        
        self.addChild(currentView!)
        self.liveView.addSubview(currentView!.view)
        
        currentView?.setFrame(self.view.bounds)
        
        currentView?.showPreview(true)

        currentView?.showDebugInfo(false)
       
        
        return currentView!
    }
    
    func getNewR5VideoViewController(rect : CGRect) -> R5VideoViewController {
        
        let view : UIView = UIView(frame: rect)
        
        var r5View : R5VideoViewController
        r5View = R5VideoViewController.init()
        r5View.view = view;
        
        return r5View;
            
    }
    
    open override var shouldAutorotate:Bool {
        get {
            return true
        }
    }
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return [UIInterfaceOrientationMask.all]
        }
    }

}
