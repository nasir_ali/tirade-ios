//
//  LeftSideViewController.swift
//  MilkuSingh
//
//  Created by Ajay Yadav on 15/10/18.
//  Copyright © 2018 MilkuSingh. All rights reserved.
//

import UIKit

protocol LeftDrawerClicked {
    func selectedCell(indexPath :  Int,selectedTitle : String)
}

class LeftSideViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var profileImgCenter: UIImageView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var profileImgInCenter: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileID: UILabel!    
    @IBOutlet weak var leftTable: UITableView!
    var selectedPage = String()
    var incogNitoTrash = ["All Posts","Politics","Race","Government","LBGT","Business","Legal","Religeon","Sex","Drugs","Music","Sports","Education", "Law enforecment","Medical","Love","Culture","Language","Behavior","Psyche","Beauty","Community","Exercise"  ,"Cooking","Cosmology","Dancing","Gender","Dream Interpretation","Eschatology","Courtship","Ethics","World Relations","Etiquette","Faith ","Food","Taboos","Funeral Rites","Games","Gift Giving","Healing","Hairstyle","Hospitality","Housing","Hygiene","Incest","Inheritance","Rules","Joking","Kinship","Relatives","Systems","Categorizing","sexual restrictions","Soul Concept","Luck","Superstition","Magic", "Marriage","Meal times","Medicine","Obstetrics","Pregnancy","Childbirth","Rituals","Penal Sanctions","Punishment of Crimes","Personal Names", "Population","Policy","Post Natal Care","Property Rights","Propitiation of Supernatural Beings" ,"Puberty","Customs","Politics","Other"]

    var tiradeHome = ["Profile","Golive","Music","Competitions","Event Calender","Sports","Topvideos","Games"]
    var delegate : LeftDrawerClicked?
    
    //var iconArr = ["home.png","profile.png","orderHis.png","aboutUs.png","contact-us.png","termCon.png","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch selectedPage {
        case "TiradeHome":
            self.profileImg.image = UIImage(named: "profileImgBack")
            self.profileImgCenter.image = UIImage(named: "tiradeProfileBack")
            self.profileImgInCenter.image = UIImage(named: "tiradeLogo")
            self.view.backgroundColor = DataUtils.hexStringToUIColor(hex: "#31544F").withAlphaComponent(0.9)
        default:
            self.profileImg.image = UIImage(named: "trash-talk-logo")
            self.profileImgCenter.image = UIImage(named: "backgroundTirade")
            self.profileImgInCenter.image = UIImage(named: "")
            self.view.backgroundColor = DataUtils.hexStringToUIColor(hex: "#5B6572").withAlphaComponent(0.9)
            
        }
        //  profileImgCenter.layer.cornerRadius = profileImgCenter.frame.size.height/2
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //  profileImg.layer.cornerRadius = profileImg.frame.height/2
        // profileImg.layer.masksToBounds = true
        self.navigationController?.isNavigationBarHidden = true
        leftTable.register(UINib(nibName: "LeftVCCell", bundle: nil), forCellReuseIdentifier: "LeftVCCell")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedPage {
        case "TiradeHome":
            return tiradeHome.count
        default:
            break
        }
        return incogNitoTrash.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 40
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftVCCell", for: indexPath ) as!  LeftVCCell
        // cell.leftIcon.image =  UIImage(named: iconArr[indexPath.row])
        if selectedPage == "TiradeHome" {
            cell.leftNameLbl.text = tiradeHome[indexPath.row] as! String
        }else {
            cell.leftNameLbl.text = incogNitoTrash[indexPath.row] as! String
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedPage == "TiradeHome" {
            delegate?.selectedCell(indexPath: indexPath.row, selectedTitle: tiradeHome[indexPath.row])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leftCellClicked"), object: nil, userInfo: ["selectedTitle":tiradeHome[indexPath.row]])
        }else {
            delegate?.selectedCell(indexPath: indexPath.row, selectedTitle: incogNitoTrash[indexPath.row])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "leftCellClicked"), object: nil, userInfo: ["selectedTitle":incogNitoTrash[indexPath.row]])

        }
            dismiss(animated: true, completion: nil)
    }
}
