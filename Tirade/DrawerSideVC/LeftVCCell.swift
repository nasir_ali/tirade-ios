//
//  LeftVCCell.swift
//  MilkuSingh
//
//  Created by Ajay Yadav on 16/10/18.
//  Copyright © 2018 MilkuSingh. All rights reserved.
//

import UIKit

class LeftVCCell: UITableViewCell {

    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var leftNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
