//
//  PostPageVC.swift
//  Tirade
//
//  Created by admin on 01/06/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit
import IQKeyboardManager

class PostPageVC: UIViewController {
    
    @IBOutlet weak var txtVwPost : IQTextView!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var lblUser : UILabel!
    @IBOutlet weak var vwPostHeight : NSLayoutConstraint!
    @IBOutlet weak var vwBaseIcons : UIView!
    @IBOutlet weak var btnPost : UIButton!
    var formData = TrashCategory()

    var selectedIndexPath = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,selector: #selector(getFileData(notification:)),name: NSNotification.Name(rawValue: "fileSelectionclicked"), object: nil)

        vwBaseIcons.layer.cornerRadius = 10
        txtVwPost.becomeFirstResponder()
        let dataDecoded : Data = Data(base64Encoded: TiradeUserDefaults.getUser_Image(key: "user_Image"), options: .ignoreUnknownCharacters)!
        if  dataDecoded.count > 0 {
            let imageFromData: UIImage = UIImage(data:dataDecoded)!
            self.imgUser.image = imageFromData
        }else {
            self.imgUser.image = UIImage(named: "user")
        }
        if selectedIndexPath == 0 {
            self.lblUser.text = "Who are you giving a shout out to?" //What are you representing?
            self.btnPost.setTitle("shout out", for: .normal)
            formData.category = "shout out"
        }else {
            self.btnPost.setTitle("representing", for: .normal)
            self.lblUser.text = "What are you representing?"
            formData.category = "representing"
        }
    }
    
    @objc func getFileData(notification : NSNotification)  {
        guard let dictFile  = notification.userInfo
            else {
                return
        }
        
        let fileData = FileTypeInfo()
        fileData.fileData = dictFile["fileData"] as! Data
        fileData.fileName = dictFile["fileName"] as! String
        fileData.uploadFileKey = dictFile["uploadFileKey"] as! String
        fileData.mimeType = dictFile["mimeType"] as! String
        
        var arr = [FileTypeInfo]()
        arr.append(fileData)
        
        formData.fileInfoType.append(fileData)
        print(formData.fileInfoType)        
    }
    
    func sendActionDataToServer(dataDict : [String : AnyObject])  {
        let url = BASEURL + POSTCATEGORYNEWSFEEDDATA
        
        
        ServerRequest.requestWithFormData(endUrl: url, fileTypeInfo: formData.fileInfoType, parameters: dataDict, onCompletion: { (data) in
            DispatchQueue.main.async {
                DataUtils.removeLoader()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    DataUtils.showAlert(title: "", msg: "Post Successfully Uploaded.", selfObj: self) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }, onError: { (error) in
            print("errorerrorerrorerrorerrorerrorerror\(error)")
        }, onController: self)
    }
    
    @IBAction func videoAction(_ sender: Any) {
        AttachmentHandler.shared.showVideoList(vc: self)

    }
    
    @IBAction func audioAction(_ sender: Any) {
        let voiceVC = UIStoryboard(name: "Tirade", bundle: nil).instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
        voiceVC.fileNameAudio = randomString(length: 8)
//        self.navigationController?.present(voiceVC, animated: true, completion: {
//
//        })
        
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController  {
                 while let presentedViewController = topController.presentedViewController {
                       topController = presentedViewController
                      }
           topController.present(voiceVC, animated: true, completion: nil)
        }
        
        
        
        
    }
    
    @IBAction func photosAction(_ sender: Any) {
        AttachmentHandler.shared.showImageLibraryList(vc: self)
    }
    
    @IBAction func fileAction(_ sender: Any) {
        AttachmentHandler.shared.showFileLibraryList(vc: self, fileType: "pdf")

    }
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    @IBAction func cancelAction(_ sender: Any) {
     dismiss(animated: true, completion: nil)
    }
    
    @IBAction func postAction(_ sender: Any) {
        formData.post = txtVwPost.text
        if formData.category != nil {
            if formData.post == "" {
                DataUtils.showAlert(title: "", msg: "please write a post", selfObj: self) {
                }
            }else {
                    let dataDict = ["user_id" :  TiradeUserDefaults.getUser_ID(key: "user_id"),
                                    "who":formData.text1,"what":formData.text2,"category":formData.selectedCategory,"post_content":formData.post,"post_type":formData.category] as [String : Any]
                    sendActionDataToServer(dataDict: dataDict as [String : AnyObject])
                }
            }
        else {
            self.navigationController?.popViewController(animated: false)
        }

    }
}
