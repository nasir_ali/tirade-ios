//
//  CategorySelectionVW.swift
//  Tirade
//
//  Created by admin on 19/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class CategorySelectionVW: UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tblCell = tableView.dequeueReusableCell(withIdentifier: "CategoryTblCell") as! CategoryTblCell
        return tblCell
    }
    

  @IBOutlet weak var tblCategory : UITableView!
    
}
