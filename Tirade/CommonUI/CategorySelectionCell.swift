//
//  CategorySelectionCell.swift
//  Tirade
//
//  Created by admin on 19/03/20.
//  Copyright © 2020 ajayyadav. All rights reserved.
//

import UIKit

class CategorySelectionCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var txtFldCategory : UITextField!
    @IBOutlet weak var tblCategory : UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tblCategory.register(UINib(nibName: "CategoryTblCell", bundle: nil), forCellReuseIdentifier: "CategoryTblCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tblCell = tblCategory.dequeueReusableCell(withIdentifier: "CategoryTblCell") as! CategoryTblCell
        tblCell.textLabel?.text = "ajayyyyy"
        return tblCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
